scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=45600, X1=48500, dX=700, Y0=3100, Y1=16700, passes=10, sample='s#4 61: Cu', linedelay=0)

scan.spot(detector.eiger(), X=58000, Y=15200, exposure=4, images=100, sample='s#5 Zn foil, Kb')
scan.spot(detector.eiger(), X=58000, Y=13000, exposure=3, images=200, sample='s#5 Cu foil, Kb')
scan.spot(detector.eiger(), X=58000, Y=6700, exposure=4, images=100, sample='s#5 Ho foil, Kb')

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3100, Y1=16700, passes=10, sample='s#6 62: CuPdCu', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3100, Y1=16700, passes=10, sample='s#6 62: CuPdCu', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3100, Y1=16700, passes=10, sample='s#6 62: CuPdCu', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=45600, X1=48500, dX=700, Y0=3200, Y1=16650, passes=10, sample='s#4 61: Cu', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3200, Y1=16650, passes=10, sample='s#6 62: CuPdCu', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3200, Y1=16650, passes=10, sample='s#6 62: CuPdCu', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3200, Y1=16650, passes=10, sample='s#6 62: CuPdCu', linedelay=0)

scan.spot(detector.eiger(), X=58000, Y=15200, exposure=4, images=100, sample='s#5 Zn foil, Kb')
scan.spot(detector.eiger(), X=58000, Y=13000, exposure=3, images=200, sample='s#5 Cu foil, Kb')
scan.spot(detector.eiger(), X=58000, Y=6700, exposure=4, images=100, sample='s#5 Ho foil, Kb')

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=45600, X1=48500, dX=700, Y0=3300, Y1=16600, passes=10, sample='s#4 61: Cu', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3300, Y1=16600, passes=10, sample='s#6 62: CuPdCu', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=69000, X1=72000, dX=700, Y0=3300, Y1=16600, passes=10, sample='s#6 62: CuPdCu', linedelay=0)

caput("PINK:ANC01:ACT0:CMD:TARGET",10000)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)