## DCM functions

class DCMFUNC():
    def ID_on(self):
        caput("u171dcm1:SetIdOn", "Id On")
        caput("u171pgm1:SetIdOn", "Id On")
        caput("U17IT6R:BaseCmdLswitch", "remote")
        sleep(1)
        #caput("u171pgm1:idMbboIndex","u17_15Apr21pgm")
        #caput("u171dcm1:idMbboIndex","U17_DCM_20210415")
        #caput("u171dcm1:idMbboIndex","U17_07-12-21dcm")
        caput("u171dcm1:idMbboIndex","U17DCM_7-12-21")
        #sleep(1)
        #caput("u171pgm1:Harmonic","1st Harmonic")
        #caput("u171dcm1:Harmonic","Max Flux")
        sleep(1)
        caput("u171dcm1:Harmonic","Max Flux")
        print("ID set to ON")

    def ID_off(self):
        caput("u171dcm1:SetIdOn","Id Off")
        caput("u171pgm1:SetIdOn","Id Off")
        caput("U17IT6R:BaseCmdLswitch","local")
        print("ID set to OFF")

    def fbp_off(self):
        caput("u171dcm1:piezoPitchfbStatus", 0)
        print("Feedback loop for pitch set to OFF")

    def fbr_off(self):
        caput("u171dcm1:piezoRollfbStatus", 0)
        print("Feedback loop for roll set to OFF")

    def set_energy(self, energy):
        caput("u171dcm1:monoSetEnergy", energy)
