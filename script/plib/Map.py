if get_exec_pars().args:
    filename=get_exec_pars().args[0]
else:  
    err = "Invalid argument"
    print err
    raise Exception(err)

def posconv(n):
    v = 10*round(n/10)
    return v

def posuniq(n):
    vv = []
    for i in n:
        v = posconv(i)
        if v not in vv:
            vv.append(v)
    return vv

def getindex(v, a):
    res = -1
    for i in range(len(a)):
        if v==a[i]:
            return i 

def getintensity(d):
    dd = []
    for i in d:
        dd.append(sum(i))
    return dd

def mzeros(sx, sy):
    m = []
    for j in range(sy):
        m.append([0.0 for i in range(sx)])
    return m

def getimg(x, y , dat):
    xx = posuniq(x)
    yy = posuniq(y)
    dd = getintensity(dat)
    img = mzeros(len(xx), len(yy))
    for i in range(len(dd)):
        xpos = posconv(x[i])
        ypos = posconv(y[i])
        xi = getindex(xpos, xx)
        yi = getindex(ypos, yy)
        img[yi][xi]=dd[i]
    return img

def pink_map(filename):
    pname = filename.split('_')
    pname = pname[2]
    try:
        ## check type
        if get_data_info("scan/type", root = filename).containsKey("Data Type"):
            ptype = load_data("scan/type", root=filename)
            if ptype == "line":
                plottype=1
            elif ptype == "continuous":
                plottype=2
            else:
                print("Map not available for this type of scan")
                return

        ## zigzag map                
        if plottype==1:
            x = load_data("passes/pass01/positioners/sec_el_x", root=filename)
            y = load_data("passes/pass01/positioners/sec_el_y", root=filename)
            dat = load_data("passes/pass01/detector/eiger/processed/spectrum", root=filename)
            xx = posuniq(x)
            yy = posuniq(y)
            dd = getintensity(dat)
        ## continoues map
        else:
            print("plottype 2 cont")
            return        

        ## create image matrix
        img = getimg(x, y, dat)
        plot(img)

    except Exception, errvalue:
        print(errvalue)

    return        

pink_map(filename)
