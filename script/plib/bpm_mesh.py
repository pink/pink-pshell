class MESH:
    def __init__(self):
        pass

    def calibrate(self, Xrange, Xstep, Yrange, Ystep, exposure):
        print("### BPM Quadrant diodes calibration ###")
        print(time.asctime())

        xdrange = float(Xrange)
        xstep = float(Xstep)
        ydrange = float(Yrange)
        ystep = float(Ystep)
        exposure = float(exposure)
    
        ## channels
        print("Creating channels...")
        xmotor = create_channel_device("PINK:SMA01:m10a.VAL", type='d')
        xmotor_rbv = create_channel_device("PINK:SMA01:m10a.RBV", type='d')
        xmotor_rbv.setMonitored(True)
        xmotor_dmov = create_channel_device("PINK:SMA01:m10a.DMOV", type='d')
        xmotor_dmov.setMonitored(True)
        ymotor = create_channel_device("PINK:SMA01:m9a.VAL", type='d')
        ymotor_rbv = create_channel_device("PINK:SMA01:m9a.RBV", type='d')
        ymotor_rbv.setMonitored(True)
        ymotor_dmov = create_channel_device("PINK:SMA01:m9a.DMOV", type='d')
        ymotor_dmov.setMonitored(True)
        xbeam = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV", type='d')
        xbeam.setMonitored(True)
        ybeam = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV", type='d')
        ybeam.setMonitored(True)
        cae2_acq = create_channel_device("PINK:CAE2:Acquire", type='i')
        cae2_acq.setMonitored(True)
        diode_d1_pv = create_channel_device("PINK:CAE2:Current1:MeanValue_RBV", type='d')
        diode_d1_pv.setMonitored(True)
        diode_d2_pv = create_channel_device("PINK:CAE2:Current2:MeanValue_RBV", type='d')
        diode_d2_pv.setMonitored(True)
        diode_d3_pv = create_channel_device("PINK:CAE2:Current3:MeanValue_RBV", type='d')
        diode_d3_pv.setMonitored(True)
        diode_d4_pv = create_channel_device("PINK:CAE2:Current4:MeanValue_RBV", type='d')
        diode_d4_pv.setMonitored(True)
        sleep(2)        

        ## positions
        xpos = linspace(-xdrange, xdrange, xstep)
        ypos = linspace(-ydrange, ydrange, ystep)
        xdim = len(xpos)
        ydim = len(ypos)
        N = xdim*ydim
        loop_id=0
        set_status("{:d}/{:d}".format(loop_id, N))
    
        ## initial positions
        xini = xmotor_rbv.read()
        yini = ymotor_rbv.read()

        ## setup filename
        set_exec_pars(open=True, name="bpm_calibration", reset=True)
    
        ## Plot
        xdata = [range(xdim)]*ydim
        ydata = [[j]*xdim for j in range(ydim)]
        zxdata=[]
        zydata=[]
        for j in range(ydim):
            zex=[]
            zey=[]
            for i in range(xdim):
                zex.append(0)
                zey.append(0)
            zxdata.append(zex)
            zydata.append(zey)

        mplot = plot([zxdata, zydata],["Grid X","Grid Y"])
        zxplot = mplot[0].getSeries(0)
        zyplot = mplot[1].getSeries(0) 
                
        ## arrays
        minpx = []
        minpy = []
        motorposx = []
        motorposy = []
        diode_d1 = []
        diode_d2 = []
        diode_d3 = []
        diode_d4 = []

        print("running...")    
        ## main loop
        for i in range(xdim):
            posx = xpos[i]
            xmotor.write(posx)
            xmotor_rbv.waitValueInRange(posx, 1.0, 10000)
            xmotor_dmov.waitValueInRange(1, 0.5, 10000)
            for j in range(ydim):
                posy = ypos[j]       
                ymotor.write(posy)
                ymotor_rbv.waitValueInRange(posy, 1.0, 10000)
                ymotor_dmov.waitValueInRange(1, 0.5, 10000)
                while(cae2_acq.read()>0): sleep(0.2)
                #cae2_acq.waitValueInRange(0, 0.5, 10000) 
                sleep(0.3)
                cae2_acq.write(1)
                xbeam.waitCacheChange(1000*int(exposure+20))
                motorposx.append(xmotor_rbv.read())
                motorposy.append(ymotor_rbv.read())
                minpx.append(xbeam.read())
                minpy.append(ybeam.read())
                diode_d1.append(diode_d1_pv.value)
                diode_d2.append(diode_d2_pv.value)
                diode_d3.append(diode_d3_pv.value)
                diode_d4.append(diode_d4_pv.value)
                loop_id += 1
                zxdata[j][i]=xbeam.value
                zydata[j][i]=ybeam.value
                zxplot.setData(zxdata, xdata, ydata)
                zyplot.setData(zydata, xdata, ydata)            
                set_status("Status: {:.1f}% ({:d}/{:d})".format(100*(float(loop_id)/N),loop_id, N))
                
        ## end of loop
        print("Saving data...")
        save_dataset("data/xpos", xpos)
        save_dataset("data/ypos", ypos)
        save_dataset("data/xdim", xdim)
        save_dataset("data/ydim", ydim)
        save_dataset("data/xdata", minpx)
        save_dataset("data/ydata", minpy)
        save_dataset("data/zxdata", motorposx)
        save_dataset("data/zydata", motorposy)
        save_dataset("data/diode_d1", diode_d1)
        save_dataset("data/diode_d2", diode_d2)
        save_dataset("data/diode_d3", diode_d3)
        save_dataset("data/diode_d4", diode_d4)
        
        ## push results
        print("Pushing data to epics...")
        caput("PINK:QBPM:xdata", array('d', minpx))
        caput("PINK:QBPM:ydata", array('d', minpy))
        caput("PINK:QBPM:zxdata", array('d', motorposx))
        caput("PINK:QBPM:zydata", array('d', motorposy))
        caput("PINK:QBPM:xdim", float(xdim))
        caput("PINK:QBPM:ydim", float(ydim))
        caput("PINK:QBPM:calibrate.PROC", 1)
        
        ## moving back to initial position
        xmotor.write(xini)
        ymotor.write(yini)

        print("### Complete ###")

    def load(self, filename, smoothing):
        fname=filename.split('.')[0]+".h5"
        #print("Loading data from file: {}".format(fname))
        print("## Loading BPM calibration data ##")

        xdata = load_data("data/xdata", root=fname)
        ydata = load_data("data/ydata", root=fname)
        zxdata = load_data("data/zxdata", root=fname)
        zydata = load_data("data/zydata", root=fname)
        xdim = load_data("data/xdim", root=fname)
        ydim = load_data("data/ydim", root=fname)

        if smoothing>0:
            print("Smoothing mesh ...")
            ## index functions
            def gyi(n):
                return n%ydim
            def gxi(n):
                return math.floor(float(n)/ydim)
            def get_neighbours(n):
                np = [n]
                if gyi(n-1) < gyi(n):
                    if (n-1)>=0:
                        np.append(n-1)
                if gyi(n+1) > gyi(n):
                    if (n+1)<N:
                        np.append(n+1)
                if gxi(n-ydim) < gxi(n):
                    if (n-ydim)>=0:
                        np.append(n-ydim)
                if gxi(n+ydim) > gxi(n):
                    if (n+ydim)<N:
                        np.append(n+ydim)
                return np

            #p1h = plot(None, "0")
            #p1 = p1h.get(0)
            #p1.addSeries(LinePlotSeries("00"))
            #p1.getSeries(0).setData(xdata)
            #p1.setLegendVisible(True)
                
            ## smoothing
            SN = int(smoothing)
            N = xdim*ydim
            for k in range(SN):
                #print("Smooth pass {}".format(k))
                xdata2 = []
                ydata2 = []
                for i in range(N):
                    ilist = get_neighbours(i)
                    xval = []
                    yval = []
                    for j in ilist:
                        xval.append(xdata[j])
                        yval.append(ydata[j])
                    xdata2.append(mean(xval))
                    ydata2.append(mean(yval))
                xdata = array('d', xdata2)
                ydata = array('d', ydata2)
                #p1.addSeries(LinePlotSeries("{}".format(k+1)))
                #p1.getSeries(k+1).setData(xdata)

        #p1.addSeries(LinePlotSeries("smoothed"))
        #p1.getSeries(1).setData(xdata)


        ## push results
        print("Pushing data to epics...")
        caput("PINK:QBPM:xdata", xdata)
        caput("PINK:QBPM:ydata", ydata)
        caput("PINK:QBPM:zxdata", zxdata)
        caput("PINK:QBPM:zydata", zydata)
        caput("PINK:QBPM:xdim", float(xdim))
        caput("PINK:QBPM:ydim", float(ydim))
        caput("PINK:QBPM:calibrate.PROC", 1)

        print("### Complete ###")

        
            


        