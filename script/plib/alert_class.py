class ALERT():
    def enable(self):
        #print("enable")
        caput("PINK:ALERT:enable",1)

    def disable(self):
        #print("disable")
        caput("PINK:ALERT:enable",0)

    def message(self, msg):
        if type(msg)==type("str"):
            #print(msg)
            msgout = self.__addts(msg)
            log("[Alert msg] {}".format(msgout))
            caput("PINK:ALERT:message",msgout)
       
    def __addts(self, msg):
        tn = time.localtime()
        ts = "[{}.{}|{:02d}:{:02d}:{:02d}] {}".format(tn.tm_mday,tn.tm_mon,tn.tm_hour,tn.tm_min,tn.tm_sec,msg)
        return ts