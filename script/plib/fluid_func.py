## fluid functions

class FLUID():
    def scan_continuous(self, detector, det_exposure=1, sample_exposure=1, X0=0, X1=0, dX=0.0, Y0=0, Y1=0, passes=1, sample="", linedelay=0, offsets=[]):
        if isinstance(detector, DETEC):
            print("!! Please use detector. to see list of detectors !!")
            return
        if detector=="eiger":
            run("plib/scan/fluid_continuous_eiger.py")
            myscan = CONTEIGER()
            myscan.scan(det_exposure, sample_exposure, X0, X1, dX, Y0, Y1, passes, sample, linedelay, offsets)
            del myscan
            print("OK")
            return
        else:
            print("Detector not available")
        return
