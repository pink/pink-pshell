class DCMSCAN():
    def __init__(self):
        pass

    ########################################
    ### scan
    ########################################

    def scan(self, *args, **kwargs):
        ## arguments
        detector=kwargs["detector"]
        start=kwargs["start"]
        end=kwargs["end"]
        step=kwargs["step"]
        exposure=kwargs["exposure"]
        point_delay=kwargs["point_delay"]
        
        DEBUG=False
        data_compression = {"compression":"True", "shuffle":"True"}

        print("DCM Scan")
        
        if DEBUG: log("Creating channels...", data_file = False)
        ## Creating channels
        #if detector=="bpm3":
        #    SENSOR = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
        #    SENSOR.setMonitored(True)
        #    ACQ = create_channel_device("PINK:DG01:TriggerDelayBO", type='d')
        if detector=="eiger":
            Eiger_ROI_X = caget("PINK:EIGER:image3:ArraySize0_RBV")
            Eiger_ROI_Y = caget("PINK:EIGER:image3:ArraySize1_RBV")        
            SENSOR = create_channel_device("PINK:EIGER:specint", type='d')
            SENSOR.setMonitored(True)
            ACQ = create_channel_device("PINK:EIGER:cam1:Trigger", type='d')
            Eiger_frameID = create_channel_device("PINK:EIGER:cam1:ArrayCounter_RBV", type='d')
            Eiger_frameID.setMonitored(True)
            Eiger_roi_array = create_channel_device("PINK:EIGER:image3:ArrayData", type='[d', size=int(Eiger_ROI_X*Eiger_ROI_Y))
            Eiger_roi_array.setMonitored(True)
            Eiger_Spectra = create_channel_device("PINK:EIGER:spectrum_RBV", type='[d', size=Eiger_ROI_X)
            Eiger_Spectra.setMonitored(True)

        #elif detector=="ge":
        #    SENSOR = create_channel_device("PINK:GEYES:specint", type='d')
        #    SENSOR.setMonitored(True)
        #    ACQ = create_channel_device("PINK:GEYES:cam1:Acquire", type='i')
        else:
            raise Exception("Detector not available")

        ## DCM
        ENERGY = create_channel_device("u171dcm1:monoSetEnergy")
        ENERGY_RBV = create_channel_device("u171dcm1:monoGetEnergy")
        ENERGY_RBV.setMonitored(True)
        ENERGY_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
        ENERGY_DMOV.setMonitored(True)

        ##DCM TEST
        #ENERGY = create_channel_device("PINK:MSIM2:m1.VAL")
        #ENERGY_RBV = create_channel_device("PINK:MSIM2:m1.RBV")
        #ENERGY_RBV.setMonitored(True)
        #ENERGY_DMOV = create_channel_device("PINK:MSIM2:m1.DMOV", type='i')
        #ENERGY_DMOV.setMonitored(True)
        
        ## GAP
        GAP = create_channel_device("U17IT6R:BasePmGap.A")
        GAP.setMonitored(True)
        ## IZero
        IZERO = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
        IZERO.setMonitored(True)
        IZero_profile = create_channel_device("PINK:CAE2:image1:ArrayData", type='[d', size=100)
        IZero_profile.setMonitored(True)        
        ## CAE
        #CAE1_ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
        #CAE2_ACQ = create_channel_device("PINK:CAE2:Acquire", type='i')
        ## dcm CR1/2/CT
        DCM_CR1 = create_channel_device("u171dcm1:dcm:CR1", type='d')
        DCM_CR2 = create_channel_device("u171dcm1:dcm:CR2", type='d')
        DCM_CT = create_channel_device("u171dcm1:dcm:CT", type='d')
        DCM_TRANSLATION = create_channel_device("u171dcm1:CT", type='d')
        DCM_ROTATION = create_channel_device("u171dcm1:CR", type='d')
        DCM_THETA = create_channel_device("u171dcm1:Theta", type='d')
        ## piezo voltages
        PZHEIGHTV = create_channel_device("MONOY01U112L:Piezo1U1", type='d')
        PZPITCHV = create_channel_device("MONOY01U112L:Piezo2U1", type='d')
        PZROLLV = create_channel_device("MONOY01U112L:Piezo3U1", type='d')
        ## BPM2 profiles
        BPM2Ver = create_channel_device("PINK:PG04:Stats2:ProfileAverageY_RBV", type='[d')
        BPM2Hor = create_channel_device("PINK:PG04:Stats2:ProfileAverageX_RBV", type='[d')

        ##Channel cut
        ch_cut_status = caget("u171dcm1:disableCT", type='i')

        ## plot vectors
        energy = []
        izero = []
        sensor = []
        absvec = []

        ## Setup
        #print("Scanning ...")
        set_exec_pars(open=False, name="dcm", reset=True)
        save_dataset("scan/scantype", "DCM scan")   
        save_dataset("scan/detector", detector)   
        save_dataset("scan/start_time", time.ctime())
        save_dataset("dcm/channel_cut", ch_cut_status)

        ## Saving detectors settings
        save_dataset("detector/eiger/exposure", exposure)
        save_dataset("detector/eiger/roi_line", caget("PINK:EIGER:ROI1:MinY_RBV"))
        save_dataset("detector/eiger/roi_sizex", Eiger_ROI_X)
        save_dataset("detector/eiger/roi_sizey", Eiger_ROI_Y)
        save_dataset("detector/eiger/roi_h_start", caget("PINK:EIGER:roie0"))
        save_dataset("detector/eiger/roi_h_end", caget("PINK:EIGER:roie1"))
        save_dataset("detector/eiger/energy", caget("PINK:EIGER:cam1:PhotonEnergy_RBV"))
        save_dataset("detector/eiger/threshold", caget("PINK:EIGER:cam1:ThresholdEnergy_RBV"))        

        ## create datasets
        create_dataset("detector/eiger/processed/image", 'd', False, (0, Eiger_ROI_Y, Eiger_ROI_X), features=data_compression)
        create_dataset("detector/eiger/processed/spectrum", 'd', False, (0, Eiger_ROI_X))
        create_dataset("detector/eiger/raw/frame_id", 'd', False)
        
        create_dataset("readings/energy", 'd', False)
        create_dataset("readings/detector", 'd', False)
        create_dataset("readings/izero", 'd', False)
        create_dataset("readings/izero_profile", 'd', False, (0, 100))
        create_dataset("readings/gap", 'd', False)
        create_dataset("processed/detector_norm", 'd', False)
        create_dataset("dcm/CR1", 'd', False)
        create_dataset("dcm/CR2", 'd', False)
        create_dataset("dcm/CT", 'd', False)
        create_dataset("bpm2/X", 'd', False)
        create_dataset("bpm2/Y", 'd', False)
        create_dataset("dcm/translation", 'd', False)
        create_dataset("dcm/rotation", 'd', False)
        create_dataset("dcm/theta", 'd', False)        
        create_dataset("dcm/height_V", 'd', False)
        create_dataset("dcm/pitch_V", 'd', False)
        create_dataset("dcm/roll_V", 'd', False)

        if DEBUG: log("Setting up Caenels...", data_file = False)
        ## Setup caenels
        caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:TriggerMode", 1)
        caput("PINK:CAE2:AcquireMode", 0)
        caputq("PINK:CAE2:Acquire", 1)

        caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:TriggerMode", 1)
        caput("PINK:CAE1:AcquireMode", 0)
        caputq("PINK:CAE1:Acquire", 1)

        ## configure scan positions
        positionarray = linspace(start, end, step)        
        
        if DEBUG: log("Setup plot...", data_file = False)
        ## Setup plot
        plottitle="IZero"
        plottitle2="Detector"
        plottitle3="Detector_norm"
        [p1, p2, p3]=plot([None, None, None], [plottitle, plottitle2, plottitle3], title="DCM Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p2.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p3.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p1.getAxis(p1.AxisId.X).setLabel("Energy")
        p2.getAxis(p1.AxisId.X).setLabel("Energy")
        p3.getAxis(p1.AxisId.X).setLabel("Energy")
        set_setting("last_plot_title", "DCM Scan") 

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(1, [0, exposure-0.02], [0, exposure+0.001], [0, 0], [0, 0.001])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 3)
        caput("PINK:RPISW:select_B", 2)    

        ## setup eiger
        if DEBUG: log("Stop eiger", data_file = False)
        caput("PINK:EIGER:cam1:Acquire", 0)
        sleep(1)
        caput("PINK:EIGER:cam1:AcquireTime", exposure)
        sleep(1)
        caput("PINK:EIGER:cam1:AcquirePeriod", exposure+0.002)
        caput("PINK:EIGER:cam1:NumImages", 1)
        caput("PINK:EIGER:cam1:NumTriggers", len(positionarray))
        # manual trigger enable
        caput("PINK:EIGER:cam1:ManualTrigger", 1)
        sleep(0.5)
        ## arm detector
        caputq("PINK:EIGER:cam1:Acquire", 1)

        if DEBUG: log("Moving to first pos", data_file = False)
        ## Moving to first position
        print("Moving DCM to start position...")
        pos = positionarray[0]
        ENERGY.write(pos)
        sleep(1)
        self.__waitdcm(ENERGY_DMOV, point_delay)

        if DEBUG: log("Scanning...", data_file = False)        
        print("Scanning...")
        print("Filename: {}".format(self.__get_filename()))
        
        ## Scan loop
        try:
            ## Main loop
            for pos in positionarray:
                ## move and wait for dcm
                if DEBUG: log("moving", data_file = False)
                ENERGY.write(pos)
                sleep(0.5)
                self.__waitdcm(ENERGY_DMOV, point_delay)

                ##trigger acq
                if DEBUG: log("trigger", data_file = False)
                ACQ.write(1)
                resp = SENSOR.waitCacheChange(int(1000*(exposure+2)))
                sleep(0.2)

                ## append to vecto
                if DEBUG: log("appending to vectors", data_file = False)
                energy.append(ENERGY_RBV.read())
                sensor.append(SENSOR.read())
                izero.append(IZERO.read())
                absvec.append(SENSOR.take()/IZERO.take())

                ## update plot
                if DEBUG: log("updating plots", data_file = False)
                p1.getSeries(0).setData(energy, izero)
                p2.getSeries(0).setData(energy, sensor)
                p3.getSeries(0).setData(energy, absvec)

                ## calculate BPM2 pos from profiles
                if DEBUG: log("bpm calc", data_file = False)
                posv = self.beampos_bpm2_v(BPM2Ver)
                posh = self.beampos_bpm2_h(BPM2Hor)

                ## append data
                if DEBUG: log("appending data", data_file = False)
                append_dataset("readings/energy", ENERGY_RBV.take())
                append_dataset("readings/detector", SENSOR.take())
                append_dataset("readings/izero", IZERO.take())
                append_dataset("readings/izero_profile", IZero_profile.take())
                append_dataset("readings/gap", GAP.read())
                append_dataset("processed/detector_norm", SENSOR.take()/IZERO.take())
                append_dataset("dcm/CR1", DCM_CR1.read())
                append_dataset("dcm/CR2", DCM_CR2.read())
                append_dataset("dcm/CT", DCM_CT.read())
                append_dataset("bpm2/X", posh)
                append_dataset("bpm2/Y", posv)
                append_dataset("dcm/translation", DCM_TRANSLATION.read())
                append_dataset("dcm/rotation", DCM_ROTATION.read())
                append_dataset("dcm/theta", DCM_THETA.read())        
                append_dataset("dcm/height_V", PZHEIGHTV.read())
                append_dataset("dcm/pitch_V", PZPITCHV.read())
                append_dataset("dcm/roll_V", PZROLLV.read())          
                append_dataset("detector/eiger/processed/image", Convert.reshape(Eiger_roi_array.take(), Eiger_ROI_Y, Eiger_ROI_X))
                append_dataset("detector/eiger/processed/spectrum", Eiger_Spectra.take())
                append_dataset("detector/eiger/raw/frame_id", Eiger_frameID.take())
                
        except:
            print("DCM scan aborted.")
            #print(value)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        ## save data
        save_dataset("scan/end_time", time.ctime())
        save_dataset("plot/title", plottitle)
        save_dataset("plot/title2", plottitle2)
        save_dataset("plot/title3", plottitle3)
        save_dataset("plot/x", positionarray)
        create_dataset("plot/y", 'd', False, (0, len(izero)))
        append_dataset("plot/y", izero)
        create_dataset("plot/y2", 'd', False, (0, len(sensor)))
        append_dataset("plot/y2", sensor)
        create_dataset("plot/y3", 'd', False, (0, len(absvec)))
        append_dataset("plot/y3", absvec)
        save_dataset("plot/y_desc", "")
        save_dataset("plot/xlabel", "Energy")
        save_dataset("plot/ylabel", "")

        ## setup eiger
        if DEBUG: log("Stop eiger", data_file = False)
        caput("PINK:EIGER:cam1:Acquire", 0)
        sleep(1)
        # manual trigger enable
        caput("PINK:EIGER:cam1:ManualTrigger", 0)
        sleep(0.5)

        print("Scan finished")


    ## extra functions
    def __get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname  

    def __waitdcm(self, dmov, delay):
        ## sim
        #while(dmov.read()<1):
        while(dmov.read()):
            sleep(0.5)
        sleep(delay)
        
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])            
            
    def bpmanalysis(self, vec, pos0, pos1, N, myCF):
        vecb = vec[0:N]
        xvecabs = range(pos0,pos1)
        xvec = range(0,N)
        weights = [ 1.0] * len(xvec)
        try:
            (a, b, amp, com, sigma) = fit_gaussian_linear(vecb, xvec, None, weights)
            abspos=com+pos0
        except:
            log("Error on bpm fitting", data_file = False)
            abspos=0
        return abspos

    def beampos_bpm2_v(self, PV):
        bpm2_vert_CF=13
        bpm2_hor_CF=13*0.707
        sig2fwmh=2.355
        roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
        roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
        roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
        roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
        N = int(roisizey)
        pos0 = roiposy
        pos1 = roiposy+roisizey
        myCF = bpm2_vert_CF
        #vec = caget("PINK:PG04:Stats2:ProfileAverageY_RBV")
        vec = PV.read()
        beampos = self.bpmanalysis(vec, pos0, pos1, N, myCF)
        return beampos

    def beampos_bpm2_h(self, PV):
        bpm2_vert_CF=13
        bpm2_hor_CF=13*0.707
        sig2fwmh=2.355
        roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
        roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
        roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
        roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
        N = int(roisizex)
        pos0 = roiposx
        pos1 = roiposx+roisizex
        myCF = bpm2_hor_CF
        #vec = caget("PINK:PG04:Stats2:ProfileAverageX_RBV")
        vec = PV.read()
        beampos = self.bpmanalysis(vec, pos0, pos1, N, myCF)
        return beampos            