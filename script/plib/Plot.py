if get_exec_pars().args:
    filename=get_exec_pars().args[0]
else:  
    err = "Invalid argument"
    print err
    raise Exception(err)

def pink_plot(filename):
    pname = filename.split('_')
    pname = pname[2]
    plotdata = {}
    plottype=0
     
    ## load data from file
    try:
        ## check type
        if get_data_info("plot/type", root = filename).containsKey("Data Type"):
            ptype = load_data("plot/type", root=filename)
            if ptype == "bpm":
                plottype=4
                
        ## BPM profile plot
        if plottype==4:
            title = load_data("plot/title", root=filename)
            title2 = load_data("plot/title2", root=filename)
            xlabel = load_data("plot/xlabel", root=filename)
            ylabel = load_data("plot/ylabel", root=filename)
            x = load_data("plot/x", root=filename)
            x2 = load_data("plot/x2", root=filename)
            y = load_data("plot/y", root=filename)
            y2 = load_data("plot/y2", root=filename)
            [p1, p2]=plot([None, None], [title, title2], title=pname)  
            p1.getSeries(0).setData(x, y[0])
            p2.getSeries(0).setData(x2, y2[0])
            ss_h = LinePlotSeries("Gaussian Fit")
            ss_v = LinePlotSeries("Gaussian Fit")
            p1.addSeries(ss_h)
            p2.addSeries(ss_v)
            ss_h.setData(x, y[1])
            ss_v.setData(x2, y2[1])
            return
    
        ## single plot
        plottype=1
        title = load_data("plot/title", root=filename)
        xlabel = load_data("plot/xlabel", root=filename)
        ylabel = load_data("plot/ylabel", root=filename)
        x = load_data("plot/x", root=filename)
        y = load_data("plot/y", root=filename)
        y_desc = load_data("plot/y_desc", root=filename)
        
        ## dual plot
        if get_data_info("plot/title2", root = filename).containsKey("Data Type"):
            title2 = load_data("plot/title2", root=filename)
            y2 = load_data("plot/y2", root=filename)
            if get_data_info("plot/x2", root = filename).containsKey("Data Type"):
                plotdata["x2"] = load_data("plot/x2", root=filename)
            plottype=2

        ## tripple plot
        if get_data_info("plot/title3", root = filename).containsKey("Data Type"):
            title3 = load_data("plot/title3", root=filename)
            y3 = load_data("plot/y3", root=filename)
            plottype=3
            
    except:
        err = "Invalid plot structure"
        print(err)
        raise Exception(err)
        return

    if plottype==1:
        p1h=plot(None, y_desc[0], title=pname)
        p1 = p1h.get(0)
        p1.setTitle(title)
        p1.getAxis(p1.AxisId.X).setLabel(xlabel)
        p1.getAxis(p1.AxisId.Y).setLabel(ylabel)
        for i in range(len(y)):
            p1.addSeries(LinePlotSeries(y_desc[i]))
            p1.getSeries(i).setData(x, y[i])
        p1.setLegendVisible(True)
    elif plottype==2:
        [p1, p2]=plot([None, None], [title, title2], title=pname)    
        p1.setTitle(title)
        p1.getAxis(p1.AxisId.X).setLabel(xlabel)
        p1.getAxis(p1.AxisId.Y).setLabel(ylabel)
        p2.getAxis(p2.AxisId.X).setLabel(xlabel)
        p2.getAxis(p2.AxisId.Y).setLabel(ylabel)
        p1.getSeries(0).setData(x, y[0])
        if plotdata.has_key("x2"):
            p2.getSeries(0).setData(plotdata["x2"], y2[0])
        else:
            p2.getSeries(0).setData(x, y2[0])
    elif plottype==3:
        [p1, p2, p3]=plot([None, None, None], [title, title2, title3], title=pname)    
        p1.setTitle(title)
        p1.getAxis(p1.AxisId.X).setLabel(xlabel)
        p1.getAxis(p1.AxisId.Y).setLabel(ylabel)
        p2.getAxis(p2.AxisId.X).setLabel(xlabel)
        p2.getAxis(p2.AxisId.Y).setLabel(ylabel)
        p3.getAxis(p3.AxisId.X).setLabel(xlabel)
        p3.getAxis(p3.AxisId.Y).setLabel(ylabel)        
        p1.getSeries(0).setData(x, y[0])
        p2.getSeries(0).setData(x, y2[0])
        p3.getSeries(0).setData(x, y3[0])           
    else:
        err = "Invalid plot structure"
        print err
        raise Exception(err)
        
    set_setting("last_plot_title", pname)

pink_plot(filename)        
