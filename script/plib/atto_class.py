class ATTO():
    def __init__(self):
        self.poschan = ""
        self.poslist=[]
        self.enable=False
        self.acq=False
        self.flist = []
        self.status = "idle"
        
    def __worker(self):
        self.status = "running"
        while(self.enable):
            if self.acq:
                self.poschan.waitCacheChange(1000)
                self.poslist.append(abs(self.poschan.value))
            else:
                sleep(0.25)
        self.status = "idle"
                
    def clean(self):
        self.poslist=[]

    def acq_start(self):
        self.acq=True

    def acq_stop(self):
        self.acq=False

    def get_data(self):
        return self.poslist

    def job_start(self):
        self.enable=True
        self.flist = fork(self.__worker)

    def job_stop(self):
        self.enable=False

    def set_channel(self, poschan):
        self.poschan=poschan

    def get_status(self):
        return self.status

    def reset(self):
        self.acq=False
        self.enable=False
        sleep(1)
        self.clean()