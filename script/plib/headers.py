class HEADERS:
    def zigzag(self, headdict):
        #Str.removeMultipleSpaces(a)
        run("plib/headers.py")
        HH = HZZ()
        HH.hexec(headdict)

    def spot(self, headdict):
        run("plib/headers.py")
        HH = HSPOT()
        HH.hexec(headdict)
    
    def __send(self, header):
        horline = 80*'*'
        print(horline)
        for hline in header:
            print(hline)
        print(horline)

class HZZ:
    def hexec(self, headdict):
        print("class HZZ")
        header=[]
        if headdict.has_key("sample"): 
            header.append(" Sample:  {}".format(headdict["sample"]))
        headers.__send(header)

class HSPOT:
    def hexec(self, headdict):
        print("class HSPOT")
        header=[]
        if headdict.has_key("sample"): 
            header.append(" Sample:  {}".format(headdict["sample"]))
        headers.__send(header)