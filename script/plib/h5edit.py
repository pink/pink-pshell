#!/usr/bin/python3

import sys
import os.path
import h5py

argnum = len(sys.argv)
#print("argnum: "+str(argnum))
status = True

if len(sys.argv)<2:
    print('Error: not enough arguments')
    status=False

if status:
    f=open(sys.argv[1], 'r')
    lines=f.readlines()
    #print(lines)
    fname = lines[0].splitlines()[0]
    newdata = lines[1]
    dataset = "/scan/sample"
    dtype = "S"+str(int(len(newdata)))
    if(os.path.exists(fname)==False):
        print("Error: File does not exist")
        status=False

if status:
    try:
        f1 = h5py.File(fname, 'r+')
        del f1[dataset]
        data = f1.create_dataset(dataset, (1,), dtype=dtype)
        data[...]=newdata.encode()
        f1.close()
    except:
        print("Error: Could not write new data")
        status=0

if status:
    print("OK")