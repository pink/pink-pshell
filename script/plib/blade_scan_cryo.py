## blade scan
class BLADESCAN():
    def __init__(self):
        pass

    def scan(self, source, axis, start=0, end=0, steps=0, exposure=0):
        if (exposure == 0):
            print("Abort: exposure = 0 ")
            return
        
        if source=="bpm3":
            ## check for bpm3 diode status
            diode_out = int(caget("PINK:PLCGAS:vdiode_FA_LS",'i'))==1
            if diode_out:
                print("[Warning] The diode on BPM3 is not in the beam path. Aborting.")
                return
            ## check for filters attenuation
            filter_att = float(caget("PINK:FILTER:TxNow"))
            if filter_att > 0.5:
                print("[Warning] Please check the attenuation filters to avoid diode damage.")
                print("[Warning] The total attenuation is now approximately: 1/{:.0f}".format(1/filter_att))
        
        ## Debug option
        verbose = True
        
        ## channels
        if verbose: print("Creating channels")            
    
        if source=="bpm3":
            SENSOR = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
            SENSOR.setMonitored(True)
            SENSOR_ID = create_channel_device("PINK:CAE1:NumAcquired")
            SENSOR_ID.setMonitored(True)
            ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
            ACQ.setMonitored(True)            
        elif source=="tfy":
            SENSOR = create_channel_device("PINK:CAE1:Current2:MeanValue_RBV")
            SENSOR.setMonitored(True)
            SENSOR_ID = create_channel_device("PINK:CAE1:NumAcquired")
            SENSOR_ID.setMonitored(True)
            ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
            ACQ.setMonitored(True) 
        else:
            print("Blade scan is not not available for this source.")
            return

        ## sample motor
        if axis=="x":
            MOTOR = create_channel_device("PINK:PHY:AxisJ.VAL", type='d')
            MOTOR_RBV = create_channel_device("PINK:PHY:AxisJ.RBV", type='d')
            MOTOR_RBV.setMonitored(True)
            MOTOR_DMOV = create_channel_device("PINK:PHY:AxisJ.DMOV", type='d')
            MOTOR_DMOV.setMonitored(True)
        elif axis=="y":
            MOTOR = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
            MOTOR_RBV = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
            MOTOR_RBV.setMonitored(True)
            MOTOR_DMOV = create_channel_device("PINK:ANC01:ACT0:IN_TARGET", type='d')
            MOTOR_DMOV.setMonitored(True)                
        else:
            print("Erro on axis definition. Aborting.")
            return

        ## variables
        sensor = []
        motor = []
        prescan_pos = 0

        ## Setup
        set_exec_pars(open=False, name="blade", reset=True)

        if verbose: print("Setup ampmeter")
        ## Configure ampmeter
        if source == "bpm3":
            exposure=float(exposure)
            ACQ.write(0)
            sleep(1)
            caput("PINK:CAE1:AcquireMode", 2) ## single
            caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
            caput("PINK:CAE1:AveragingTime", exposure)
            caput("PINK:CAE1:TriggerMode", 0)
            caput("PINK:CAE1:Range", 0)
            sleep(1)
        elif source == "tfy":
            exposure=float(exposure)
            ACQ.write(0)
            sleep(1)
            caput("PINK:CAE1:AcquireMode", 2) ## single
            caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
            caput("PINK:CAE1:AveragingTime", exposure)
            caput("PINK:CAE1:TriggerMode", 0)
            caput("PINK:CAE1:Range", 1)
            sleep(1)


        ## configure scan positions
        positionarray = linspace(start, end, steps)   
                 
        if axis=="x":
            xinfo = "{}, {}, {}".format(start, end, steps)
            yinfo = str(caget("PINK:ANC01:ACT0:POSITION"))
        else:
            xinfo = str(caget("PINK:PHY:AxisJ.RBV"))        
            yinfo = "{}, {}, {}".format(start, end, steps)

        ##set file name
        set_exec_pars(open=False, name="blade", reset=True)

        ## Save some data to initialize file
        save_dataset("scan/scantype", "blade scan")

        ## print some info
        print("******************************************************")
        print("   Filename: " + self.get_filename())
        print("  Scan type: blade scan")
        print("     Sensor: " + source)
        print("          X: " + xinfo)
        print("          Y: " + yinfo)
        print("   Exposure: {}".format(exposure))
        print("******************************************************")

        elab.put("******************************************************")
        elab.put("   Filename: " + self.get_filename())
        elab.put("  Scan type: blade scan")
        elab.put("     Sensor: " + source)
        elab.put("          X: " + xinfo)
        elab.put("          Y: " + yinfo)
        elab.put("   Exposure: {}".format(exposure))
        elab.put("******************************************************")

        ## plot setup
        if verbose: print("Setup plot")
        [p1, p2]=plot([None, None], ["Scan","Derivative"], title="Blade Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p2.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        #p1.addSeries(LinePlotSeries("Interpolation"))
        p2.addSeries(LinePlotSeries("Fit"))

        if verbose: print("Scanning...")

        prescan_pos = MOTOR_RBV.read()

        # Open fast shutter
        caput("PINK:PLCGAS:ei_B01", 1)

        ## Main loop
        for pos in positionarray:
            MOTOR.write(pos)
            sleep(0.25)
            MOTOR_DMOV.waitValueInRange(1, 0.5, 60000)

            while(ACQ.take()==1):
                sleep(0.25)
            ACQ.write(1)
            resp = SENSOR.waitCacheChange(1000*int(exposure+2))
            if resp==False:
                print("Abort: No data from ampmeter")
                return

            sensor.append(SENSOR.read())
            motor.append(MOTOR_RBV.take())
            p1.getSeries(0).setData(motor, sensor)
            if len(motor)>2:
                dt = deriv(sensor, xdata=motor)
                p2.getSeries(0).setData(motor, dt)

        # close fast shutter
        caput("PINK:PLCGAS:ei_B01", 0)

        ## Analysing data
        if verbose: print("Analysing data")
        dt = deriv(sensor, xdata=motor)
        p2.getSeries(1).setData(motor, dt)
        if mean(dt)<0:
            dt = [-i for i in dt]
        p2.getSeries(0).setData(motor, dt)
        (off, amp, com, sigma, gauss, fwhm, xgauss)=self.__gauss_fit(dt, motor)
        p2.getSeries(1).setData(xgauss, gauss)
        p2.setLegendVisible(True)
        self.__print_info(off, amp, com, sigma, fwhm)

        save_dataset("gaussian/offset", off)
        save_dataset("gaussian/amplitude", amp)
        save_dataset("gaussian/mean", com)
        save_dataset("gaussian/sigma", sigma)
        save_dataset("gaussian/FWHM", fwhm)
        save_dataset("plot/gaussY_fit", gauss)
        save_dataset("plot/gaussX_fit", xgauss)
        save_dataset("processed/derivative", dt)
        save_dataset("raw/sensor", sensor)
        save_dataset("raw/blade", motor)

        ## Move motor back to pre scan position
        #MOTOR.write(prescan_pos)
        #MOTOR_RBV.waitValueInRange(prescan_pos, 1.0, 60000)

        pink_save_bl_snapshot()
        print("OK")

    ##----------------------------------------------------------------------------------------------
    ######################################
    ### Support Functions
    ######################################

    def __gauss_fit(self, ydata, xdata):
        (off, amp, com, sigma) = fit_gaussian_offset(ydata, xdata)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 100)
        gauss = [f.value(i)+off for i in xgauss]
        fwhm = 2.355*sigma
        return (off, amp, com, sigma, gauss, fwhm, xgauss)

    def __print_info(self, off, amp, com, sigma, fwhm):
        print("*** Gaussian fit ***")
        print("--------------------")
        print("   Offset: " + '{:.3e}'.format(off))
        print("Amplitude: " + '{:.3e}'.format(amp))
        print("     Mean: " + '{:.3f}'.format(com))
        print("    Sigma: " + '{:.3f}'.format(sigma))
        print("     FWHM: " + '{:.3f}'.format(fwhm))
        print("--------------------")
    
    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname        
                        
        
