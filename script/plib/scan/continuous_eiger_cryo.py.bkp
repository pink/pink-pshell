## continuous scan for eiger using cryo chamber
class CONTEIGER():

    def scan(self, det_exposure, sample_exposure, X0, X1, dX, Y0, Y1, passes, sample, linedelay):
        print("Continuous scan for eiger using cryo chamber...")
        
        #clean up epics connections
        Epics.destroy()
        Epics.create()

        ## variables
        DEBUG=True
        initial_frame = 0
        pass_id = 1
        Eiger_ROI_X = caget("PINK:EIGER:image3:ArraySize0_RBV")
        Eiger_ROI_Y = caget("PINK:EIGER:image3:ArraySize1_RBV")
        data_compression = {"compression":"True", "shuffle":"True"}
        profile_size = 100
        scan_abort = False
        tnow = 0
        scan_dir=1

        ## convertions
        X0=float(X0)
        X1=float(X1)
        Y0=float(Y0)
        Y1=float(Y1)

        ## parameter calculations
        ## assuming a beam of vertical size = 50um
        sample_speed = 50.0/sample_exposure
        x_positions = linspace(X0, X1, float(dX))
        num_lines = len(x_positions)
        if det_exposure == "auto" or det_exposure=="AUTO":
            images_per_line = 1
            det_exposure = ((abs(Y1-Y0))/sample_speed)-0.1
        else:
            images_per_line = math.floor((abs(Y1-Y0)/sample_speed)/(det_exposure+0.001))
        sample_l = images_per_line*(det_exposure+0.001)*sample_speed
        effic = sample_l/(abs(Y1-Y0))

        Xpoints = num_lines
        Ypoints = images_per_line
        exposure = det_exposure

        ## setup filename
        set_exec_pars(open=False, name="eiger", reset=True)

        scantimestr = self.scantime_calc(exposure=exposure, Ypoints=Ypoints, Xpoints=Xpoints, passes=passes, linedelay=linedelay)

        # close fast shutter
        caput("PINK:PLCGAS:ei_B01", 0)
        #FSHT.write(0)

        ## Move to first X position
        # move Y to middle
        if DEBUG: log("Moving Y to middle position...")
        caput("PINK:ANC01:ACT0:CMD:TARGET", 9000.0)
        self.wait_piezo(9000.0, 60)
        if DEBUG: log("Moving to first X position...")
        print("Moving to sample position...")
        caput("PINK:PHY:AxisJ", x_positions[0])
        sleep(2)
        while(caget("PINK:PHY:AxisJ.DMOV",type='d') < 1.0):
            sleep(1)
        if DEBUG: log("In position")    

        print("Estimating piezo frequencies...")
        atto_freqs = self.speed2freq(sample_speed)
        #atto_freqs = [0,0]

        tse_string = self.timeformater(det_exposure*num_lines*images_per_line*passes)

        print("******************************************* ")
        print("               Filename:  " + self.get_filename())
        print("                 Sample:  " + sample)
        print("              Scan type:  continuous")
        print("               Detector:  Eiger")
        print("           Sample speed:  " + '{:.1f}'.format(sample_speed) + " um/s")
        print("             Piezo freq:  {} Hz".format(atto_freqs))
        print("                      X: N={}, delta={}, {}".format(num_lines, dX, str(x_positions)))
        print("                      Y: N={}, [{:.0f} ... {:.0f}]".format(int(images_per_line), Y0, Y1))        
        print("         Images p/ line:  " + '{:d}'.format(int(images_per_line)))
        print("       Number of passes:  " + '{:d}'.format(int(passes)))
        print("      Detector exposure:  " + '{:.1f}'.format(det_exposure) + " s")
        print("          Dose per pass:  " + '{:.2f}'.format(sample_exposure) + " s")
        print("                   Dose:  " + '{:.2f}'.format(sample_exposure*passes) + " s")
        print("  Total sample exposure:  " + tse_string)
        print("        Total scan time:  {}".format(scantimestr))
        print("******************************************* ")
        print(" ")

        elab.put("***********************************************************************")
        elab.put(" Sample:  " + sample)
        elab.put(" Filename:  " + self.get_filename())
        elab.put(" Scan type:  continuous")
        elab.put(" Sample speed:  " + '{:.1f}'.format(sample_speed) + " um/s")
        elab.put(" Piezo freq:  {} Hz".format(atto_freqs))        
        elab.put(" X: N={}, delta={}, {}".format(num_lines, dX, str(x_positions)))
        elab.put(" Y: N={}, [{:.0f} ... {:.0f}]".format(int(images_per_line), Y0, Y1))
        elab.put(" Number of passes:  " + '{:d}'.format(int(passes)))
        elab.put(" Detector exposure: {:.1f} s".format(det_exposure))
        elab.put(" Dose per pass:  " + '{:.2f}'.format(sample_exposure) + " s")
        elab.put(" Dose:  {:.2f} s".format(sample_exposure*passes))
        elab.put(" Total sample exposure: " + tse_string)
        elab.put("***********************************************************************")
        elab.put(" ")

        #return

        if DEBUG: log("Set channels")
        ## create channels
        Eiger_acquire = create_channel_device("PINK:EIGER:cam1:Acquire", type='i')
        Eiger_status = create_channel_device("PINK:EIGER:cam1:Acquire_RBV", type='i')
        Eiger_status.setMonitored(True)
        Eiger_frameID = create_channel_device("PINK:EIGER:cam1:ArrayCounter_RBV", type='d')
        Eiger_frameID.setMonitored(True)
        Eiger_roi_array = create_channel_device("PINK:EIGER:image3:ArrayData", type='[d', size=int(Eiger_ROI_X*Eiger_ROI_Y))
        Eiger_roi_array.setMonitored(True)
        Eiger_Spectra = create_channel_device("PINK:EIGER:spectrum_RBV", type='[d', size=Eiger_ROI_X)
        Eiger_Spectra.setMonitored(True)
        Eiger_Spectra_sum = create_channel_device("PINK:EIGER:specsum_RBV", type='[d', size=Eiger_ROI_X)
        Eiger_trigger = create_channel_device("PINK:EIGER:cam1:Trigger", type='d')

        TFY = create_channel_device("PINK:CAE1:Current2:MeanValue_RBV", type='d')
        TFY.setMonitored(True)
        TFY_profile = create_channel_device("PINK:CAE1:image2:ArrayData", type='[d', size=100)
        TFY_profile.setMonitored(True)
        IZero = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV", type='d')
        IZero.setMonitored(True)
        IZero_profile = create_channel_device("PINK:CAE2:image1:ArrayData", type='[d', size=100)
        IZero_profile.setMonitored(True)
        Frame_countdown = create_channel_device("PINK:AUX:countdown.VAL")
        Display_status = create_channel_device("PINK:AUX:ps_status")
        Progress = create_channel_device("PINK:GEYES:Scan:progress")
        Ring_current = create_channel_device("MDIZ3T5G:current", type='d')
        Ring_current.setMonitored(True)
        ### positioners
#        ## sec el x
#        Sec_el_x = create_channel_device("PINK:SMA01:m10.VAL", type='d')
#        Sec_el_x_RBV = create_channel_device("PINK:SMA01:m10.RBV", type='d')
#        Sec_el_x_RBV.setMonitored(True)
#        Sec_el_x_DMOV = create_channel_device("PINK:SMA01:m10.DMOV", type='d')
#        Sec_el_x_DMOV.setMonitored(True)
#        ## sec el y
#        Sec_el_y = create_channel_device("PINK:SMA01:m9.VAL", type='d')
#        Sec_el_y_RBV = create_channel_device("PINK:SMA01:m9.RBV", type='d')
#        Sec_el_y_RBV.setMonitored(True)
#        Sec_el_y_DMOV = create_channel_device("PINK:SMA01:m9.DMOV", type='d')
#        Sec_el_y_DMOV.setMonitored(True)
#        Sec_el_y_VELO = create_channel_device("PINK:SMA01:m9.VELO", type='d')
#        Sec_el_y_STOP = create_channel_device("PINK:SMA01:m9.STOP", type='d')
        ## cryo x
        cryo_x = create_channel_device("PINK:PHY:AxisJ.VAL", type='d')
        cryo_x_RBV = create_channel_device("PINK:PHY:AxisJ.RBV", type='d')
        cryo_x_RBV.setMonitored(True)
        cryo_x_DMOV = create_channel_device("PINK:PHY:AxisJ.DMOV", type='d')
        cryo_x_DMOV.setMonitored(True)
        ## cryo y
        cryo_y = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
        cryo_y_RBV = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
        cryo_y_RBV.setMonitored(True)
        cryo_y_DMOV = create_channel_device("PINK:ANC01:ACT0:IN_TARGET", type='d')
        cryo_y_DMOV.setMonitored(True)
        cryo_y_SPEED = create_channel_device("PINK:ANC01:ACT0:speedavg", type='d')
        #cryo_y_SPEED = create_channel_device("PINK:ANC01:ACT0:calcspeed", type='d')
        cryo_y_SPEED.setMonitored(True) 
        cryo_y_FREQ = create_channel_device("PINK:ANC01:ACT0:CMD:FREQ", type='d') 
        cryo_y_BASEFREQ = create_channel_device("PINK:ANC01:ACT0:AuxFreq1", type='d') 
        cryo_y_BASEFREQ.setMonitored(True)

        ## watchdog
        caput("PINK:ALERT:WD:enable", 0)
        WD_keep_alive = create_channel_device("PINK:ALERT:WD:keep_alive", type='d')        

        # fast shutter 
        FSHT = create_channel_device("PINK:PLCGAS:ei_B01", type='i')

        #if DEBUG: log("Set atto")
        ## config asynchronous data gatherer
        #atto.reset()
        #atto.set_channel(cryo_y_SPEED)
        #atto.job_start()

        if DEBUG: log("Set pressure channels")
        ## create pressure channels
        run("config/pressure_devices.py")
        pdev = []
        for pd in pressure_pvlist:
            pvdesc = pd[1]+".DESC"
            pdev.append([pd[0], create_channel_device(pd[1], type='d'), caget(pvdesc)])
        for pd in pdev:
            pd[1].setMonitored(True)

        ## Clean status message
        Display_status.write(" ")

        if DEBUG: log("Stop eiger")
        ## Stop eiger
        if Eiger_status.read():
            Eiger_acquire.write(0)
            if DEBUG: log("Eiger Stop", data_file = False)
            while(Eiger_status.read()):
                sleep(1)
            if DEBUG: log("Eiger Idle", data_file = False)

        if DEBUG: log("Saving pre data")
        ## save initial scan data
        save_dataset("scan/sample", sample)
        save_dataset("scan/start_time", time.ctime())
        save_dataset("scan/type", "continuous")
        save_dataset("scan/num_passes", passes)
        save_dataset("scan/num_images_pass", Xpoints*Ypoints)
        save_dataset("scan/sample_speed", sample_speed)
        save_dataset("scan/sample_exposure", sample_exposure)
        save_dataset("scan/vert_lines", Xpoints)
        save_dataset("scan/images_per_line", Ypoints)
        save_dataset("scan/chamber", "Cryo")

        ## save plot data
        save_dataset("plot/title", sample)
        save_dataset("plot/xlabel", "channel")
        save_dataset("plot/ylabel", "counts")
        plotx = linspace(1,Eiger_ROI_X,1.0)
        save_dataset("plot/x", plotx)

        ## create plot dataset
        create_dataset("plot/y", 'd', False, (0, Eiger_ROI_X))
        create_dataset("plot/y_desc", 's', False)

        ## Saving detectors settings
        save_dataset("detector/eiger/exposure", exposure)
        save_dataset("detector/eiger/roi_line", caget("PINK:EIGER:ROI1:MinY_RBV"))
        save_dataset("detector/eiger/roi_sizex", Eiger_ROI_X)
        save_dataset("detector/eiger/roi_sizey", Eiger_ROI_Y)
        save_dataset("detector/eiger/energy", caget("PINK:EIGER:cam1:PhotonEnergy_RBV"))
        save_dataset("detector/eiger/threshold", caget("PINK:EIGER:cam1:ThresholdEnergy_RBV"))

        ## Update status data
        caput("PINK:AUX:ps_filename_RBV", self.get_filename())
        #print("Filename: " + self.get_filename())
        caput("PINK:AUX:ps_sample", sample) # Update sample name
        caput("PINK:AUX:ps_sample2", array('b', str(sample))) # Update long sample name

        if DEBUG: log("Set eiger")
        ## setup eiger
        caput("PINK:EIGER:cam1:AcquireTime", exposure)
        sleep(1)
        caput("PINK:EIGER:cam1:AcquirePeriod", exposure+0.002)
        caput("PINK:EIGER:cam1:NumImages", Ypoints)
        caput("PINK:EIGER:cam1:NumTriggers", Xpoints*passes)
        # manual trigger enable
        caput("PINK:EIGER:cam1:ManualTrigger", 1)
        sleep(0.5)
        ## arm detector
        Eiger_acquire.write(1)

        if DEBUG: log("Set cae")
        ## setup caenels 1 and 2
        if DEBUG: log("Setup CAE1", data_file = False)
        caput("PINK:CAE1:AcquireMode", 0) ## continuous
        caput("PINK:CAE1:Range", 1) ## 0: mA / 1: uA
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:ValuesPerRead", exposure*1000)
        caput("PINK:CAE1:TriggerMode", 1) ## ext trigger
        caputq("PINK:CAE1:Acquire", 1)

        if DEBUG: log("Setup CAE2", data_file = False)
        caput("PINK:CAE2:AcquireMode", 0) ## continuous
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:ValuesPerRead", exposure*1000)
        caput("PINK:CAE2:TriggerMode", 1) ## ext trigger
        caputq("PINK:CAE2:Acquire", 1)

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        #self.setup_delaygen(1, [0, (exposure+0.001)], [0, 0], [0, 0], [0, 0])
        self.setup_delaygen(5, [0, (exposure+0.001)], [0, 0], [0, 0], [0, 0])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 3)
        caput("PINK:RPISW:select_B", 2)

        ## create ascii spec arrays
        ascii_specpass = []
        ascii_specsum = [0 for i in range(Eiger_ROI_X)]

        ## create dataset for pass spectrum for eiger
        create_dataset("detector/eiger/processed/spectrum_sum", 'd', False, (0, Eiger_ROI_X))

        WD_keep_alive.write(0)
        if DEBUG: log("Star pass loop")
        try:
            ### Pass loop
            while pass_id <= passes:
                passpath = "pass"+'{:02d}'.format(pass_id)
                scan_dir=1

                caput("PINK:GEYES:Scan:progress", 0) # Reset pass progress
                caput("PINK:AUX:countdown.B", exposure) # setup frame countdown
                caput("PINK:EIGER:specsum_reset", 0) # clean spectrum sum
                caput("PINK:EIGER:specsum_reset", 1) # enable spectrum sum

                #Display_status.write("Continuous scan pass "+ '{:02d}'.format(pass_id) +" / "+ '{:02d}'.format(passes))
                passstr = "Continuous scan pass {:02d}/{:02d}".format(pass_id, passes)
                Display_status.write(passstr)
                print(passstr)

                ##   eta_calc(exposure, Ypoints, Xpoints, passes, linedelay)
                self.eta_calc(exposure, Ypoints, Xpoints, (1+passes-pass_id), linedelay)

                initial_frame = Eiger_frameID.read()

                ## create dataset for passes
                create_dataset("passes/"+passpath+"/detector/eiger/processed/image", 'd', False, (0, Eiger_ROI_Y, Eiger_ROI_X), features=data_compression)
                create_dataset("passes/"+passpath+"/detector/eiger/processed/spectrum", 'd', False, (0, Eiger_ROI_X))
                create_dataset("passes/"+passpath+"/detector/eiger/raw/frame_id", 'd', False)
                create_dataset("passes/"+passpath+"/station/izero_profile", 'd', False, (0, profile_size))
                create_dataset("passes/"+passpath+"/station/izero", 'd', False)
                create_dataset("passes/"+passpath+"/station/tfy_profile", 'd', False, (0, profile_size))
                create_dataset("passes/"+passpath+"/station/tfy", 'd', False)
                create_dataset("passes/"+passpath+"/station/ring_current", 'd', False)
                create_dataset("passes/"+passpath+"/timestamps", 'd', False)
                create_dataset("passes/"+passpath+"/positioners/cryo_x", 'd', False, (0,2))
                create_dataset("passes/"+passpath+"/positioners/cryo_y", 'd', False, (0,2))
                #create_dataset("passes/"+passpath+"/positioners/speed_profile", 'd', False, (0,0))
                create_dataset("passes/"+passpath+"/positioners/line_speed", 'd', False, (0,0))

                ## create pressure dataset
                for pd in pdev:
                    datasetpath = "passes/"+passpath+"/station/pressure/"+pd[0]
                    create_dataset(datasetpath, 'd', False)
                    set_attribute(datasetpath, "DESC", pd[2])

                ## Move Sec_el_y to start position
                #Sec_el_y_VELO.write(20000)
                #Sec_el_y.write(Y0)
                #Sec_el_y_RBV.waitValueInRange(Y0, 1, 60000)
                #Sec_el_y_DMOV.waitValueInRange(1, 0.1, 60000)

                ## create plot
                pl1h=plot(None, "Line 1", title="scan")
                pl1 = pl1h.get(0)
                pl1.setTitle("Sample Speed - Pass {}".format(pass_id))
                pl1.getAxis(pl1.AxisId.Y).setLabel("Speed (um/s)")
                pl1.getAxis(pl1.AxisId.X).setMin(0)
                pl1.getAxis(pl1.AxisId.X).setMax(images_per_line-1)
                for i in range(num_lines-1):
                    pl1.addSeries(LinePlotSeries("Line {:d}".format(int(i)+2)))
                    if i==2:
                        pl1.getSeries(3).setColor(Color(0,192,192))
                pl1.setLegendVisible(True)
                set_setting("last_plot_title", "scan")

                ## move to first position
                if DEBUG: log("Move first pos")
                print("Moving to first spot...")
                log("Moving to Y middle first...")
                cryo_y_FREQ.write(cryo_y_BASEFREQ.take())

                ## move y to closest safe position
                if (cryo_y_RBV.read()<5000.0):
                    tempdest = 5000.0
                else:
                    tempdest = 10000.0
                cryo_y.write(tempdest)
                res=self.wait_piezo(tempdest, 120)                 

                #cryo_y.write(10000.0)
                #res=self.wait_piezo2(10000.0, 120, cryo_y_RBV)
                #res=self.wait_piezo(10000.0, 120)

                log("Moving to first X line...")
                cryo_x.write(X0)
                cryo_x_DMOV.waitValueInRange(1.0, 0.5, 120000)                
                log("Moving to Y0...")
                cryo_y.write(Y0)
                sleep(1)
                if DEBUG: log("Waiting for piezo..")
                #res=self.wait_piezo2(Y0, 60, cryo_y_RBV)
                res=self.wait_piezo(Y0, 60)
                if res==False:
                    raise Exception("Piezo did not reached target position")
                if DEBUG: log("Piezo reached ok")
                print("Scanning...")

                if DEBUG: log("Start line loop")
                line_id=0
                ## Line loop
                for xpos in x_positions:
                    cryo_x.write(xpos)
                    cryo_x_RBV.waitValueInRange(xpos, 10.0, 10000)
                    cryo_x_DMOV.waitValueInRange(1.0, 0.5, 10000)

                    if(scan_dir):
                        ydest = float(Y1)
                    else:
                        ydest = float(Y0)

                    line_avg_speed=[]
                    if(ydest>cryo_y_RBV.read()):
                        attofreq = atto_freqs[0]
                    else:
                        attofreq = atto_freqs[1]

                    ## start motion
                    cryo_y_FREQ.write(attofreq)
                    sleep(0.5)
                    cryo_y.write(ydest)

                    # open fast shutter
                    #caput("PINK:PLCGAS:ei_B01", 1)
                    FSHT.write(1)

                    ## start detector
                    Eiger_trigger.write(1)

                    ## start atto speed profile capture
                    #atto.acq_start()
                    #lastspeedarray=[]
                    dt_array = [0.0,0.0]

                    loop_id=1
                    ## spot loop
                    for point_id in range(int(Ypoints)):
                        if DEBUG: log("[loop] id:{}, frame:{}, chk=1".format(loop_id, Eiger_frameID.value), data_file=False)
                        ## Ping watchdog
                        WD_keep_alive.write(0)
                        sposx = [0,0]
                        sposy = [0,0]
                        sposx[0]=cryo_x_RBV.read()
                        sposy[0]=cryo_y_RBV.read()
                        #sposx[0]=cryo_x_RBV.value
                        #sposy[0]=cryo_y_RBV.value
                        dt_array[0]=time.clock()
                        #append_dataset("passes/"+passpath+"/positioners/cryo_x", cryo_x_RBV.take())
                        #append_dataset("passes/"+passpath+"/positioners/cryo_y", cryo_y_RBV.take())
                        Frame_countdown.write(100) # Initiate frame countdown
                        if DEBUG: log("[loop] id:{}, frame:{}, chk=2".format(loop_id, Eiger_frameID.value), data_file=False)
                        resp=Eiger_Spectra.waitCacheChange(int((exposure*1000)+1000))
                        if resp==False:
                            msg="** Eiger failed to delivery new data. Attempting 1 time to retrigger detector..."
                            #print(msg)
                            log(msg)
                            #Eiger_trigger.write(0)
                            #Eiger_acquire.write(0)
                            #timetowait=exposure+10
                            #ttw0 = time.time()
                            #ttwdt = 0.0
                            #while (Eiger_status.value) and (ttwdt<timetowait):
                            #    ttwdt = time.time()-ttw0
                            #    sleep(0.25)
                            #Eiger_acquire.write(1)
                            #sleep(1)
                            #Eiger_trigger.write(1)
                            #sleep(1)
                            #msg="** Eiger retriggered. Hope it works."
                            #print(msg)
                            #log(msg)
                            #Eiger_Spectra.waitCacheChange(int((exposure*1000)+1000))
                        sleep(0.01)
                        if DEBUG: log("[loop] id:{}, frame:{}, chk=3".format(loop_id, Eiger_frameID.value), data_file=False)
                        #nowspeedarray=atto.get_data()
                        ## append to dataset
                        append_dataset("passes/"+passpath+"/detector/eiger/processed/image", Convert.reshape(Eiger_roi_array.take(), Eiger_ROI_Y, Eiger_ROI_X))
                        append_dataset("passes/"+passpath+"/detector/eiger/processed/spectrum", Eiger_Spectra.take())
                        append_dataset("passes/"+passpath+"/detector/eiger/raw/frame_id", Eiger_frameID.take())
                        append_dataset("passes/"+passpath+"/station/izero_profile", IZero_profile.take())
                        append_dataset("passes/"+passpath+"/station/izero", IZero.take())
                        append_dataset("passes/"+passpath+"/station/tfy_profile", TFY_profile.take())
                        append_dataset("passes/"+passpath+"/station/tfy", TFY.take())
                        append_dataset("passes/"+passpath+"/station/ring_current", Ring_current.take())
                        append_dataset("passes/"+passpath+"/timestamps", Eiger_frameID.getTimestampNanos())
                        sposx[1]=cryo_x_RBV.read()
                        sposy[1]=cryo_y_RBV.read()
                        #sposx[1]=cryo_x_RBV.value
                        #sposy[1]=cryo_y_RBV.value
                        dt_array[1]=time.clock()
                        append_dataset("passes/"+passpath+"/positioners/cryo_x", to_array(sposx, 'd'))
                        append_dataset("passes/"+passpath+"/positioners/cryo_y", to_array(sposy, 'd'))
                        if DEBUG: log("[loop] id:{}, frame:{}, chk=4".format(loop_id, Eiger_frameID.value), data_file=False)
                        ## append to pressure devices
                        for pd in pdev:
                            datasetpath = "passes/"+passpath+"/station/pressure/"+pd[0]
                            append_dataset(datasetpath, pd[1].take())

                        Progress.write(self.calc_progress(initial_frame, Eiger_frameID.take(), Xpoints*Ypoints))
                        ## calc average speed for spot
                        #spot_speed_avg = mean(nowspeedarray[len(lastspeedarray):])
                        spot_speed_avg = abs(sposy[1]-sposy[0])/(dt_array[1]-dt_array[0])
                        line_avg_speed.append(spot_speed_avg)
                        #print("Avg speed: {:.3f}".format(spot_speed_avg))
                        #lastspeedarray=nowspeedarray[:]
                        ## plot update
                        pl1.getSeries(line_id).setData(line_avg_speed)
                        if DEBUG: log("[loop] id:{}, frame:{}, chk=5".format(loop_id, Eiger_frameID.value), data_file=False)
                        loop_id+=1
                        ## close shutter if speed is too low
                        chkspeed = abs(cryo_y_SPEED.value)
                        if chkspeed < (0.1*sample_speed):
                            if FSHT.read()>0:
                                FSHT.write(0)
                                log("Shutter closed due to motor low speed : {} um/s".format(chkspeed), data_file=False)    
                        ## end of spot loop

                    # close fast shutter
                    #caput("PINK:PLCGAS:ei_B01", 0)
                    FSHT.write(0)

                    ## End atto speed profile capture
                    #atto.acq_stop()
                    #append_dataset("passes/"+passpath+"/positioners/speed_profile", atto.get_data())
                    append_dataset("passes/"+passpath+"/positioners/line_speed", line_avg_speed)
                    #atto.clean()

                    ## Move cryo Y to destination
                    cryo_y_FREQ.write(cryo_y_BASEFREQ.take())
                    sleep(0.5)
                    cryo_y_RBV.update()
                    try:
                        if DEBUG: log("Waiting to reach ydest...")
                        #cryo_y_RBV.waitValueInRange(ydest, 20.0, 5000)
                        res=self.wait_piezo(ydest, 15)
                        if res==False:
                            log("Piezo did not reached target position")
                        if DEBUG: log("continue")
                    except:
                        #log(errvalue)
                        print("Axis Y could not reach position {:.1f}. Actual position is: {:.1f}".format(ydest, cryo_y_RBV.value))

                    scan_dir = abs(scan_dir-1)
                    sleep(linedelay)
                    line_id+=1
                    ## end of line loop

                ## save after scan data
                save_dataset("passes/"+passpath+"/detector/eiger/processed/spectrum_sum", Eiger_Spectra_sum.read())

                ## save after pass data
                append_dataset("detector/eiger/processed/spectrum_sum", Eiger_Spectra_sum.take())

                ## save plot data
                append_dataset("plot/y", Eiger_Spectra_sum.read())
                append_dataset("plot/y_desc", "Pass "+'{:d}'.format(pass_id))

                ## save spec filename
                self.save_specfile(pass_id, extrafname="", spectrum=Eiger_Spectra_sum.take())

                ## increment pass counter
                pass_id = pass_id+1

                ## add/append ascii arrays
                ascii_specpass.append(Eiger_Spectra_sum.take())
                ascii_specsum = arradd(ascii_specsum, Eiger_Spectra_sum.take())

                ## end of pass loop

        except Exception, errvalue:
            log(errvalue)
            tnow = time.ctime()
            scan_abort = True
            msg = "scan aborted [ " + tnow + " ]"
            print(msg)
            log(msg, data_file = True)
            show_message(msg, blocking=False)
            Display_status.write(msg)
            ## save the current spectrum sum
            save_dataset("passes/"+passpath+"/detector/eiger/processed/spectrum_sum", Eiger_Spectra_sum.read())
            append_dataset("detector/eiger/processed/spectrum_sum", Eiger_Spectra_sum.take())
            Eiger_acquire.write(0)

        # close fast shutter
        FSHT.write(0)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()
        #Display_status.write("Saving beamline snapshot...")
        #run("config/bl_snapshot_config.py")
        #for spdev in snapshot_pvlist:
        #    save_dataset(spdev[0], caget(spdev[1]))

        ## save ascii files
        if passes>1:
            pink._save_ascii_file("_passes", ascii_specpass)
        pink._save_ascii_file("", ascii_specsum)  

        ## setup Cryo Y
        cryo_y_FREQ.write(cryo_y_BASEFREQ.take())

        ## config asynchronous data gatherer
        #atto.reset()

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, 0], [0, 0], [0, 0], [0, 0])

        ## Stop eiger
        if Eiger_status.read():
            Eiger_acquire.write(0)
            if DEBUG: log("Eiger Stop", data_file = False)
            while(Eiger_status.read()):
                sleep(1)
            if DEBUG: log("Eiger Idle", data_file = False)

        ## save final scan time
        save_dataset("scan/end_time", time.ctime())

        ## setup eiger
        caput("PINK:EIGER:cam1:AcquireTime", 1)
        sleep(1)
        caput("PINK:EIGER:cam1:AcquirePeriod", 1)
        caput("PINK:EIGER:cam1:NumImages", 1)
        caput("PINK:EIGER:cam1:NumTriggers", 1)
        # manual trigger enable
        caput("PINK:EIGER:cam1:ManualTrigger", 0)

        if scan_abort:
            Display_status.write("scan aborted - " + tnow)
            save_dataset("scan/status", "aborted")
        else:
            Display_status.write("Continuous scan completed. OK")
            print("Continuous scan completed.")

        ## Sound alarm for end of scan
        caput("PINK:SCNALM:scanDone", 1)

        ## Disables Watchdog
        caput("PINK:ALERT:WD:enable", 0)


    ################################################################################################

    ### progress calculation
    def calc_progress(self, initial_frame, last_frame, images):
        progress = round(((last_frame-initial_frame)/images)*100.0)
        return progress

    ### ETA calculation
    def eta_calc(self, exposure, Ypoints, Xpoints, passes, linedelay):
        bgtime = 0
        linetime = (Ypoints*(exposure+0.001))+1.7+linedelay
        passtime = (Xpoints * linetime) + bgtime
        scantime = passes*passtime
        tnow = time.time()
        passETA = time.ctime(tnow + passtime)
        scanETA = time.ctime(tnow + scantime)
        caput("PINK:AUX:pass_eta", passETA)
        caput("PINK:AUX:scan_eta", scanETA)

    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname

    ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
    ## trigger [5:single shot] [1: Ext rising edge]
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])

    def save_specfile(self, passid, extrafname="", spectrum=[]):
        try:
            datafilepath = get_exec_pars().getPath()
            fpath = datafilepath.split("/")
            fpath = datafilepath.split(fpath[-1])
            fpath = fpath[0]+"mca"
            if os.path.isdir(fpath) == False:
                os.mkdir(fpath)
            specfname = datafilepath.split("/")[-1].split(".h5")[0]+extrafname+".mca"
            specfname = fpath+"/"+specfname
            spectext = []
            spectext.append("#S "+'{:d}'.format(int(passid))+" pass"+'{:03d}'.format(int(passid))+'\n')
            spectext.append("#N 1\n")
            spectext.append("#L Counts\n")
            for ct in spectrum:
                spectext.append('{:d}'.format(int(ct))+'\n')
            spectext.append("\n")
            fspec = open(specfname, 'a+')
            for lines in spectext:
                fspec.write(lines)
            fspec.close()
        except:
            print("[Error]: Failed to create mca file")

    def scantime_calc(self, exposure=0, Ypoints=0, Xpoints=0, passes=0, linedelay=0):
        ## eiger
        bgtime = 0
        linetime = (Ypoints*(exposure+0.001))+1.7+linedelay
        passtime = (Xpoints * linetime) + bgtime
        scantime = round(passes*passtime)
        msg = self.timeformater(scantime)
        return msg

    def timeformater(self, tseconds):
        scantime = round(tseconds)
        sh = int(scantime/3600.0)
        sm = int(scantime/60)%60
        ss = int(scantime%60)
        msg = "{:02d}h {:02d}m {:02d}s".format(sh,sm,ss)
        return msg

    ### Calculates the necessary frequencies for forward and backwards motion for a given speed
    def speed2freq(self, spd):
        def atto_reset():
            initpos = 6000.0
            basefreq = caget("PINK:ANC01:ACT0:AuxFreq1")
            if basefreq < 1.0:
                basefreq = 800.0
                caput("PINK:ANC01:ACT0:AuxFreq1", basefreq)
            #caput("PINK:ANC01:ACT0:CMD:FREQ", 500.0)
            caput("PINK:ANC01:ACT0:CMD:FREQ", basefreq)
            caput("PINK:ANC01:ACT0:CMD:TARGET", initpos)
            running=True
            while(running):
                err = abs(initpos - caget("PINK:ANC01:ACT0:POSITION"))
                if err<5.0:
                    running=False
                sleep(1)
    
        ## variables
        spd=float(spd)
        DEBUG=False    
        freqs=[round(spd),round(spd)]
        test_period=20
        test_runs=2
        #PVpos = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
        #PVpos.setMonitored(True)
        pospv = "PINK:ANC01:ACT0:POSITION"
        DIRPV = ["PINK:ANC01:ACT0:CMD:CONT_FWD", "PINK:ANC01:ACT0:CMD:CONT_BCK"]
        sleep(2)

        atto_reset()

        for runid in range(test_runs):
            for dirid in range(2):
                log("Run ID {} - Piezo dir: {}".format(runid,dirid))
                caput("PINK:ANC01:ACT0:CMD:FREQ", freqs[dirid])
                sleep(0.5)
                dirpv = DIRPV[dirid]
                caput(dirpv, 1)
                sleep(1)
                pos0=caget(pospv)
                t0=time.clock()
                sleep(test_period)
                pos1=caget(pospv)
                t1=time.clock()
                caput(dirpv, 0)
                spd_now=abs(pos1-pos0)/(t1-t0)
                if DEBUG: print("Atto speed at {}Hz: {:.1f} um/s".format(freqs[dirid],spd_now))
                freqs[dirid]=round((freqs[dirid]*spd)/spd_now)
                #if DEBUG: print("Atto freq for {:.1f} um/s: {:.1f} Hz".format(spd,freqs[dirid]))
                sleep(1)
            log("Run ID {}: {}".format(runid,freqs))
        return freqs        


    ## piezo waiter
    ## timeout in seconds
    def wait_piezo(self, dest, timeout):
        timeout=int(timeout)
        while True:
            posnow=caget("PINK:ANC01:ACT0:POSITION")
            perr = abs(dest-posnow)
            log("P:{:.1f} Target:{:.1f} Err:{:.1f}".format(posnow,dest,perr))
            if perr<10.0:
                return True
            if timeout<0:
                return False
            timeout-=1
            sleep(1)

    def wait_piezo2(self, dest, timeout, mm):
        timeout=int(timeout)
        while True:
            #posnow=caget("PINK:ANC01:ACT0:POSITION")
            posnow=mm.value
            perr = abs(dest-posnow)
            log("P:{:.1f} Target:{:.1f} Err:{:.1f}".format(posnow,dest,perr))
            if perr<10.0:
                return True
            if timeout<0:
                return False
            timeout-=1
            sleep(1)
