class SAMPLESCAN():
    def scan(self, axis, start, end, step, exposure, moveback):
        print("Sample scan for ge")

        #clean up epics connections
        Epics.destroy()
        Epics.create()

        ## variables
        DEBUG=0
        sensor = []
        motor = []
        prescan_pos = 0
        specroi = []

        ## GE channels
        GE_acquire = create_channel_device("PINK:GEYES:cam1:Acquire", type='i')
        GE_status = create_channel_device("PINK:GEYES:cam1:DetectorState_RBV", type='i')
        GE_status.setMonitored(True)
        GE_FrameID = create_channel_device("PINK:GEYES:cam1:ArrayCounter_RBV", type='i')
        GE_FrameID.setMonitored(True)
        SENSOR = create_channel_device("PINK:GEYES:spectra_avg", type='d')
        SENSOR.setMonitored(True)
        SPECROI = create_channel_device("PINK:GEYES:specint", type='d')
        SPECROI.setMonitored(True)        

        ## sample motor
        if axis=="x":
            MOTOR = create_channel_device("PINK:PHY:AxisJ.VAL", type='d')
            MOTOR_RBV = create_channel_device("PINK:PHY:AxisJ.RBV", type='d')
            MOTOR_RBV.setMonitored(True)
            MOTOR_DMOV = create_channel_device("PINK:PHY:AxisJ.DMOV", type='d')
            MOTOR_DMOV.setMonitored(True)
            other_axis = "Y: {:.0f}".format(caget("PINK:ANC01:ACT0:POSITION"))
            
        if axis=="y":
            MOTOR = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
            MOTOR_RBV = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
            MOTOR_RBV.setMonitored(True)
            MOTOR_DMOV = create_channel_device("PINK:ANC01:ACT0:IN_TARGET", type='d')
            MOTOR_DMOV.setMonitored(True)
            other_axis = "X: {:.0f}".format(caget("PINK:PHY:AxisJ.RBV"))

        ## setup filename
        set_exec_pars(open=False, name="sample_scan_ge", reset=True)

        ## print some info
        print("******************************************************")
        print("   Filename: " + self.get_filename())
        print("  Scan type: sample scan")
        print("     Sensor: Greateyes detector")
        print("    Chamber: Cryogenic chamber")
        print("       Axis: " + axis)
        print("          " + other_axis)
        print("   Exposure: {:.2f} seconds".format(float(exposure)))
        print("      Start: {:.2f}".format(float(start)))
        print("        End: {:.2f}".format(float(end)))
        print("       Step: {:.2f}".format(float(step)))
        print("******************************************************")

        fcall="scan.sample_scan(axis.{}(), detector.greateyes(), start={}, end={}, step={}, exposure={}, moveback={})".format(axis,start,end,step,exposure,moveback)
        elab.put(fcall)
        elab.put("Filename: " + self.get_filename())
        elab.put(other_axis)
 
        ## save initial scan data
        save_dataset("scan/start_time", time.ctime())
        save_dataset("scan/type", "sample scan with ge")

        ## configure scan positions
        positionarray = linspace(start, end, step)

        ## plot setup
        if axis=='x':
            ypos = '{:.1f}'.format(caget("PINK:ANC01:ACT0:POSITION"))
            xlabel = 'X position'
            #plottitle = 'Horizontal Sample Scan (Y='+ypos+')'
        else:
            ypos = '{:.1f}'.format(caget("PINK:PHY:AxisJ.RBV"))
            xlabel = 'Y position'
            #plottitle = 'Vertical Sample Scan (X='+ypos+')'
        plottitle="Max Count"
        plottitle2="Horizontal ROI Sum"
        [p1, p2]=plot([None, None], [plottitle, plottitle2], title="Sample Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p2.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p1.getAxis(p1.AxisId.X).setLabel("Position")
        p2.getAxis(p2.AxisId.X).setLabel("Position")
        set_setting("last_plot_title", "Sample Scan")
        
        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, exposure-0.02], [0, 0], [0, exposure+0.15], [0, 0.001])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 1)
        caput("PINK:RPISW:select_B", 1)

        ## Stop greateyes
        if GE_status.read():
            GE_acquire.write(0)
            #if DEBUG: log("GE Stop", data_file = False)
            while(GE_status.read()):
                sleep(1)
            #if DEBUG: log("GE Idle", data_file = False)

        ## take GE background image
        self.save_GE_BG(exposure)


        ## setup caenels 1 and 2
        caput("PINK:CAE1:AcquireMode", 0) ## continuous
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:ValuesPerRead", exposure*1000)
        caput("PINK:CAE1:TriggerMode", 1) ## ext trigger
        caputq("PINK:CAE1:Acquire", 1)

        caput("PINK:CAE2:AcquireMode", 0) ## continuous
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:ValuesPerRead", exposure*1000)
        caput("PINK:CAE2:TriggerMode", 1) ## ext trigger
        caputq("PINK:CAE2:Acquire", 1)


        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(1, [0, exposure-0.02], [0, 0], [0, exposure+0.15], [0, 0.001])

        ## setup greateyes
        caput("PINK:GEYES:cam1:AcquireTime", exposure)
        caput("PINK:GEYES:cam1:ImageMode", 0) # single image

        print("Scanning...")

        # saving pre scan position
        prescan_pos = MOTOR_RBV.read()

        # Move to first position
        MOTOR.write(positionarray[0])
        # wait up to 3 minutes to reach first position
        sleep(0.25)
        MOTOR_DMOV.waitValueInRange(1, 0.5, 180000)

        # Open fast shutter
        #caput("PINK:PLCGAS:ei_B01", 1)        
        ## Main loop
        for pos in positionarray:
            MOTOR.write(pos)
            sleep(0.25)
            #MOTOR_RBV.waitValueInRange(pos, 1.0, 60000)
            MOTOR_DMOV.waitValueInRange(1, 0.5, 60000)
            GE_acquire.write(1)
            #resp = SENSOR.waitCacheChange(1000*int(exposure+2))
            resp = GE_FrameID.waitCacheChange(1000*int(exposure+2))
            sleep(0.5)
            if resp==False:
                print("Timeout: No data from ge")
                continue
            sensor.append(SENSOR.read())
            motor.append(MOTOR_RBV.read())
            specroi.append(SPECROI.read())
            p1.getSeries(0).setData(motor, sensor)
            p2.getSeries(0).setData(motor, specroi)

        # close fast shutter
        #caput("PINK:PLCGAS:ei_B01", 0)            

        ## Save data
        save_dataset("raw/sensor", sensor)
        save_dataset("raw/position", motor)
        save_dataset("raw/spectrum_roi", specroi)

        ## Save plot data
        save_dataset("plot/title", plottitle)
        save_dataset("plot/title2", plottitle2)
        save_dataset("plot/xlabel", "Position")
        save_dataset("plot/ylabel", "")
        save_dataset("plot/y_desc", "")
        save_dataset("plot/x", motor)
        create_dataset("plot/y", 'd', False, (0, len(sensor)))
        append_dataset("plot/y", sensor)
        create_dataset("plot/y2", 'd', False, (0, len(specroi)))
        append_dataset("plot/y2", specroi) 
        
        ## save data
        save_dataset("scan/finish_time", time.ctime())

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, exposure-0.02], [0, 0], [0, exposure+0.15], [0, 0.001])

        ## Move back to original position
        if (moveback!=0):
            MOTOR.write(prescan_pos)
            #MOTOR_RBV.waitValueInRange(pos, 1.0, 60000)
            MOTOR_DMOV.waitValueInRange(1, 0.5, 60000)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        if caget("PINK:SESSION:sessionstate", type='i'):
            elab.send_last_plot()

        print("Scan complete")


    ##################################
    ###  Extra functions  ############
    ##################################
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])

    ################################################################################################
    ### GE save background image function
    def save_GE_BG(self, exposure):
        DEBUG=0
        if DEBUG: log("Saving GE background", data_file = False)
        caput("PINK:AUX:ps_status", "Acquiring background image")

        ## Stop GE acquisition
        if caget("PINK:GEYES:cam1:DetectorState_RBV", type='i'):
            caputq("PINK:GEYES:cam1:Acquire", 0)
            if DEBUG: log("GE Stop", data_file = False)
            while(caget("PINK:GEYES:cam1:DetectorState_RBV", type='i')):
                sleep(1)
            if DEBUG: log("GE Idle", data_file = False)

        caput("PINK:PLCGAS:ei_B01", 0) # Close fast shutter
        caput("PINK:GEYES:cam1:AcquireTime", exposure)
        caput("PINK:GEYES:cam1:ImageMode", 0) # GE single image
        caput("PINK:AUX:countdown.B", exposure) # setup frame countdown
        caput("PINK:GEYES:Proc1:EnableBackground",0) #Disable GE BG Processing
        sleep(1)
        caput("PINK:AUX:countdown.VAL", 100) # Initiate frame countdown
        caput("PINK:GEYES:cam1:Acquire", 1) # acquire 1 image
        caput("PINK:GEYES:savebg", 1) # copy bg image to record
        caput("PINK:GEYES:Proc1:SaveBackground", 1) # save BG image into process record
        caput("PINK:GEYES:Proc1:EnableBackground",1) # Enable GE BG Processing
        caput("PINK:AUX:ps_status", "Background image OK")
        if DEBUG: log("Saving GE background OK", data_file = False)

    ## extra functions
    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname        
        
