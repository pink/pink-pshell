from mathutils import *
from plotutils import *

class BPM():
    def __init__(self):
        # BPM1 angle = 52.5 degrees
        # could replace with: math.sin(math.radians(52.5))
        self.bpm1_vert_CF=49
        self.bpm1_hor_CF=49*0.793
        # BPM2 angle = 45 degrees
        self.bpm2_vert_CF=13
        self.bpm2_hor_CF=13*0.707
        # BPM3 angle = 45 degrees
        self.bpm3_vert_CF=8.6
        self.bpm3_hor_CF=8.6*0.707
        # BPM4 angle = 0 degrees
        self.bpm4_vert_CF=9.5
        self.bpm4_hor_CF=9.5
        self.roiposx=0
        self.roiposy=0
        self.roisizex=0
        self.roisizey=0
        self.cam_exposure=0
        self.cam_gain=0
        self.sig2fwmh=2.355
        self.data_compression = {"compression":"True", "shuffle":"True"}
        self.crossx=0
        self.crossy=0

    def __mesh_calibrate(self, Xrange=300.0, Xstep=50.0, Yrange=100.0, Ystep=10.0, exposure=1.0):
        run("plib/bpm_mesh.py")
        mesh = MESH()
        mesh.calibrate(Xrange, Xstep, Yrange, Ystep, exposure)
        del mesh

    def __mesh_load(self, filename="", smoothing=0):
        run("plib/bpm_mesh.py")
        mesh = MESH()
        mesh.load(filename, smoothing)
        del mesh

    def __print_info(self, label, a, b, amp, com, sigma, conv_fact, comabs):
        print(" ")
        print("*** " + label + " Gaussian Fit ***:")
        print("   Inclination: " + '{:.3e}'.format(a))
        print("    Background: " + '{:.3f}'.format(b))
        print("     Amplitude: " + '{:.3f}'.format(amp))
        print("         Sigma: " + '{:.3f}'.format(sigma))
        print("          FWHM: " + '{:.3f}'.format(self.sig2fwmh*sigma))
        print("          Mean: " + '{:.3f}'.format(comabs))
        print("     Sigma(um): " + '{:.3f}'.format(sigma*conv_fact))
        print("      FWHM(um): " + '{:.3f}'.format(sigma*conv_fact*self.sig2fwmh))
        print(" ")

    def __BPM1_Vertical_Profile(self):
        label = "BPM1 Vertical Profile"
        print("Analysing...")
        self.__grabdata_bpm1()
        vec = caget("PINK:PG01:Stats2:ProfileAverageY_RBV")
        N = int(self.roisizey)
        pos0 = self.roiposy
        pos1 = self.roiposy+self.roisizey
        myCF = self.bpm1_vert_CF
        filename="bpm1_ver"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM1_Horizontal_Profile(self):
        label = "BPM1 Horizontal Profile"
        print("Analysing...")
        self.__grabdata_bpm1()
        vec = caget("PINK:PG01:Stats2:ProfileAverageX_RBV")
        N = int(self.roisizex)
        pos0 = self.roiposx
        pos1 = self.roiposx+self.roisizex
        myCF = self.bpm1_hor_CF
        filename="bpm1_hor"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM2_Vertical_Profile(self):
        label = "BPM2 Vertical Profile"
        print("Analysing...")
        self.__grabdata_bpm2()
        vec = caget("PINK:PG04:Stats2:ProfileAverageY_RBV")
        N = int(self.roisizey)
        pos0 = self.roiposy
        pos1 = self.roiposy+self.roisizey
        myCF = self.bpm2_vert_CF
        filename="bpm2_ver"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM2_Horizontal_Profile(self):
        label = "BPM2 Horizontal Profile"
        print("Analysing...")
        self.__grabdata_bpm2()
        vec = caget("PINK:PG04:Stats2:ProfileAverageX_RBV")
        N = int(self.roisizex)
        pos0 = self.roiposx
        pos1 = self.roiposx+self.roisizex
        myCF = self.bpm2_hor_CF
        filename="bpm2_hor"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM3_Vertical_Profile(self):
        label = "BPM3 Vertical Profile"
        print("Analysing...")
        self.__grabdata_bpm3()
        vec = caget("PINK:PG03:Stats2:ProfileAverageY_RBV")
        N = int(self.roisizey)
        pos0 = self.roiposy
        pos1 = self.roiposy+self.roisizey
        myCF = self.bpm3_vert_CF
        filename="bpm3_ver"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM3_Horizontal_Profile(self):
        label = "BPM3 Horizontal Profile"
        print("Analysing...")
        self.__grabdata_bpm3()
        vec = caget("PINK:PG03:Stats2:ProfileAverageX_RBV")
        N = int(self.roisizex)
        pos0 = self.roiposx
        pos1 = self.roiposx+self.roisizex
        myCF = self.bpm3_hor_CF
        filename="bpm3_hor"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM4_Vertical_Profile(self):
        label = "BPM4 Vertical Profile"
        print("Analysing...")
        self.__grabdata_bpm4()
        vec = caget("PINK:PG02:Stats2:ProfileAverageY_RBV")
        N = int(self.roisizey)
        pos0 = self.roiposy
        pos1 = self.roiposy+self.roisizey
        myCF = self.bpm4_vert_CF
        filename="bpm4_ver"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def __BPM4_Horizontal_Profile(self):
        label = "BPM4 Horizontal Profile"
        print("Analysing...")
        self.__grabdata_bpm4()
        vec = caget("PINK:PG02:Stats2:ProfileAverageX_RBV")
        N = int(self.roisizex)
        pos0 = self.roiposx
        pos1 = self.roiposx+self.roisizex
        myCF = self.bpm4_hor_CF
        filename="bpm4_hor"
        self.__bpmanalysis(vec, pos0, pos1, N, label, filename, myCF)

    def BPM1_profiles(self):
        label = ["BPM1 Horizontal Profile", "BPM1 Vertical Profile"]
        print("Analysing...")
        self.__grabdata_bpm1()
        N = [int(self.roisizex), int(self.roisizey)]
        pos0 = [self.roiposx, self.roiposy]
        pos1 = [self.roiposx+self.roisizex, self.roiposy+self.roisizey]
        myCF = [self.bpm1_hor_CF, self.bpm1_vert_CF]
        filename = "bpm1"
        title = "BPM1"
        self.__bpmanalysis2(self.vec, pos0, pos1, N, label, filename, myCF, title)

    def BPM2_profiles(self):
        label = ["BPM2 Horizontal Profile", "BPM2 Vertical Profile"]
        print("Analysing...")
        self.__grabdata_bpm2()
        N = [int(self.roisizex), int(self.roisizey)]
        pos0 = [self.roiposx, self.roiposy]
        pos1 = [self.roiposx+self.roisizex, self.roiposy+self.roisizey]
        myCF = [self.bpm2_hor_CF, self.bpm2_vert_CF]
        filename = "bpm2"
        title = "BPM2"
        self.__bpmanalysis2(self.vec, pos0, pos1, N, label, filename, myCF, title)

    def BPM3_profiles(self):
        label = ["BPM3 Horizontal Profile", "BPM3 Vertical Profile"]
        print("Analysing...")
        self.__grabdata_bpm3()
        N = [int(self.roisizex), int(self.roisizey)]
        pos0 = [self.roiposx, self.roiposy]
        pos1 = [self.roiposx+self.roisizex, self.roiposy+self.roisizey]
        myCF = [self.bpm3_hor_CF, self.bpm3_vert_CF]
        filename = "bpm3"
        title = "BPM3"
        self.__bpmanalysis2(self.vec, pos0, pos1, N, label, filename, myCF, title)               


    def BPM4_profiles(self):
        label = ["BPM4 Horizontal Profile", "BPM4 Vertical Profile"]
        print("Analysing...")
        self.__grabdata_bpm4()
        N = [int(self.roisizex), int(self.roisizey)]
        pos0 = [self.roiposx, self.roiposy]
        pos1 = [self.roiposx+self.roisizex, self.roiposy+self.roisizey]
        myCF = [self.bpm4_hor_CF, self.bpm4_vert_CF]
        filename = "bpm4"
        title = "BPM4"
        self.__bpmanalysis2(self.vec, pos0, pos1, N, label, filename, myCF, title)   

    def __grabdata_bpm1(self):
        self.roiposx = caget("PINK:PG01:ROI1:MinX_RBV")
        self.roiposy = caget("PINK:PG01:ROI1:MinY_RBV")
        self.roisizex = caget("PINK:PG01:ROI1:SizeX_RBV")
        self.roisizey = caget("PINK:PG01:ROI1:SizeY_RBV")
        self.cam_exposure = caget("PINK:PG01:AcquireTime_RBV")
        self.cam_gain = caget("PINK:PG01:Gain_RBV")
        self.nord = int(caget("PINK:PG01:image2:ArrayData.NORD"))
        img = caget("PINK:PG01:image2:ArrayData", size=self.nord)
        self.img = Convert.reshape(img, self.roisizey, self.roisizex)
        self.vec = [caget("PINK:PG01:Stats2:ProfileAverageX_RBV"), caget("PINK:PG01:Stats2:ProfileAverageY_RBV")]
        self.crossx = caget("PINK:PG01:Over1:5:CenterX_RBV")
        self.crossy = caget("PINK:PG01:Over1:5:CenterY_RBV")


    def __grabdata_bpm2(self):
        self.roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
        self.roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
        self.roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
        self.roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
        self.cam_exposure = caget("PINK:PG04:AcquireTime_RBV")
        self.cam_gain = caget("PINK:PG04:Gain_RBV")
        self.nord = int(caget("PINK:PG04:image2:ArrayData.NORD"))
        img = caget("PINK:PG04:image2:ArrayData", size=self.nord)
        self.img = Convert.reshape(img, self.roisizey, self.roisizex)
        self.vec = [caget("PINK:PG04:Stats2:ProfileAverageX_RBV"), caget("PINK:PG04:Stats2:ProfileAverageY_RBV")]
        self.crossx = caget("PINK:PG04:Over1:5:CenterX_RBV")
        self.crossy = caget("PINK:PG04:Over1:5:CenterY_RBV")       

    def __grabdata_bpm3(self):
        self.roiposx = caget("PINK:PG03:ROI1:MinX_RBV")
        self.roiposy = caget("PINK:PG03:ROI1:MinY_RBV")
        self.roisizex = caget("PINK:PG03:ROI1:SizeX_RBV")
        self.roisizey = caget("PINK:PG03:ROI1:SizeY_RBV")
        self.cam_exposure = caget("PINK:PG03:AcquireTime_RBV")
        self.cam_gain = caget("PINK:PG03:Gain_RBV")
        self.nord = int(caget("PINK:PG03:image2:ArrayData.NORD"))
        img = caget("PINK:PG03:image2:ArrayData", size=self.nord)
        self.img = Convert.reshape(img, self.roisizey, self.roisizex)
        self.vec = [caget("PINK:PG03:Stats2:ProfileAverageX_RBV"), caget("PINK:PG03:Stats2:ProfileAverageY_RBV")]
        self.crossx = caget("PINK:PG03:Over1:5:CenterX_RBV")
        self.crossy = caget("PINK:PG03:Over1:5:CenterY_RBV")            

    def __grabdata_bpm4(self):
        self.roiposx = caget("PINK:PG02:ROI1:MinX_RBV")
        self.roiposy = caget("PINK:PG02:ROI1:MinY_RBV")
        self.roisizex = caget("PINK:PG02:ROI1:SizeX_RBV")
        self.roisizey = caget("PINK:PG02:ROI1:SizeY_RBV")
        self.cam_exposure = caget("PINK:PG02:AcquireTime_RBV")
        self.cam_gain = caget("PINK:PG02:Gain_RBV")
        self.nord = int(caget("PINK:PG02:image2:ArrayData.NORD"))
        img = caget("PINK:PG02:image2:ArrayData", size=self.nord)
        self.img = Convert.reshape(img, self.roisizey, self.roisizex)
        self.vec = [caget("PINK:PG02:Stats2:ProfileAverageX_RBV"), caget("PINK:PG02:Stats2:ProfileAverageY_RBV")]
        self.crossx = caget("PINK:PG02:Over1:5:CenterX_RBV")
        self.crossy = caget("PINK:PG02:Over1:5:CenterY_RBV")            

    def __bpmanalysis(self, vec, pos0, pos1, N, label, filename, myCF):
        vecb = vec[0:N]
        xvecabs = range(pos0,pos1)
        xvec = range(0,N)
        weights = [ 1.0] * len(xvec)
        #pl = plot(vecb, label , xvec)[0]
        pl = plot(vecb, label, xvecabs)[0]
        pl.setLegendVisible(True)
        (a, b, amp, com, sigma) = fit_gaussian_linear(vecb, xvec, None, weights)
        self.__print_info(label, a, b, amp, com, sigma, myCF, com+pos0)
        ff = Gaussian(amp, com, sigma)
        gauss = [ff.value(i) + a*i + b for i in xvec]
        ss = LinePlotSeries("Gaussian Fit")
        pl.addSeries(ss)
        #ss.setData(xvec, gauss)
        ss.setData(xvecabs, gauss)
        self.__setup_file(fname=filename)
        self.__Save_Data(label, myCF, a, b, amp, com, com+pos0, sigma, sigma*self.sig2fwmh, sigma*myCF, sigma*self.sig2fwmh*myCF, xvecabs, vecb, gauss)
        ## save beamline/station snapshot
        pink_save_bl_snapshot()
        print("Done")

    def __bpmanalysis2(self, vec, pos0, pos1, N, label, filename, myCF, title):
        gdata = []
        gauss = []
        for i in range(2):
            num = N[i]
            vecb = vec[i][0:num]
            xvec = range(0,num)
            weights = [ 1.0] * len(xvec)
            gtemp = fit_gaussian_linear(vecb, xvec, None, weights)
            gdata.append(gtemp)
            (a, b, amp, com, sigma) = gtemp
            ff = Gaussian(amp, com, sigma)
            gauss.append([ff.value(j) + a*j + b for j in xvec])
            self.__print_info(label[i], a, b, amp, com, sigma, myCF[i], com+pos0[i])

        ## plot
        vecb_h = vec[0][0:N[0]]
        xvecabs_h = range(pos0[0],pos1[0])
        xvec_h = range(0,N[0])
        vecb_v = vec[1][0:N[1]]
        xvecabs_v = range(pos0[1],pos1[1])
        xvec_v = range(0,N[1])
        [p1, p2]=plot([None, None], [label[0], label[1]], title=title)  
        p1.getSeries(0).setData(xvecabs_h, vecb_h)
        p2.getSeries(0).setData(xvecabs_v, vecb_v)
        ss_h = LinePlotSeries("Gaussian Fit")
        ss_v = LinePlotSeries("Gaussian Fit")
        p1.addSeries(ss_h)
        p2.addSeries(ss_v)
        ss_h.setData(xvecabs_h, gauss[0])
        ss_v.setData(xvecabs_v, gauss[1])

        ## save data
        self.__setup_file(fname=filename)
        save_dataset("info/bpm", title+" profiles")
        save_dataset("camera/exposure", self.cam_exposure)
        save_dataset("camera/gain", self.cam_gain)
        save_dataset("camera/hor_conv_factor", myCF[0])
        save_dataset("camera/vert_conv_factor", myCF[1])
        save_dataset("camera/image", self.img, features = self.data_compression)
        save_dataset("ROI/MinX", self.roiposx)
        save_dataset("ROI/MinY", self.roiposy)
        save_dataset("ROI/SizeX", self.roisizex)
        save_dataset("ROI/SizeY", self.roisizey)
        save_dataset("ROI/CrossX", self.crossx)
        save_dataset("ROI/CrossY", self.crossy)        
        
        (a, b, amp, com, sigma) = gdata[0]
        save_dataset("horizontal/data", vecb_h)
        save_dataset("horizontal/fitdata", gauss[0])
        save_dataset("horizontal/gaussian/inclination", a)
        save_dataset("horizontal/gaussian/background", b)
        save_dataset("horizontal/gaussian/amplitude", amp)
        save_dataset("horizontal/gaussian/mean_roi", com)
        save_dataset("horizontal/gaussian/mean_absolute", com+pos0[0])
        save_dataset("horizontal/gaussian/sigma", sigma)
        save_dataset("horizontal/gaussian/FWHM", sigma*self.sig2fwmh)
        save_dataset("horizontal/gaussian/sigma_um", sigma*myCF[0])
        save_dataset("horizontal/gaussian/FWHM_um", sigma*self.sig2fwmh*myCF[0])

        (a, b, amp, com, sigma) = gdata[1]
        save_dataset("vertical/data", vecb_v)
        save_dataset("vertical/fitdata", gauss[1])
        save_dataset("vertical/gaussian/inclination", a)
        save_dataset("vertical/gaussian/background", b)
        save_dataset("vertical/gaussian/amplitude", amp)
        save_dataset("vertical/gaussian/mean_roi", com)
        save_dataset("vertical/gaussian/mean_absolute", com+pos0[1])
        save_dataset("vertical/gaussian/sigma", sigma)
        save_dataset("vertical/gaussian/FWHM", sigma*self.sig2fwmh)
        save_dataset("vertical/gaussian/sigma_um", sigma*myCF[1])
        save_dataset("vertical/gaussian/FWHM_um", sigma*self.sig2fwmh*myCF[1])

        save_dataset("plot/type", "bpm")

        save_dataset("plot/title", label[0])
        save_dataset("plot/title2", label[1])

        save_dataset("plot/x", xvecabs_h)
        save_dataset("plot/x2", xvecabs_v)
        save_dataset("plot/xlabel", "")
        save_dataset("plot/ylabel", "")

        save_dataset("plot/y", [vecb_h, gauss[0]])
        save_dataset("plot/y2", [vecb_v, gauss[1]])
        
        pink_save_bl_snapshot()
        print("Done")
        

    def __setup_file(self, fname="bpm"):
        set_exec_pars(open=False, name=fname, reset=True)

    def __Save_Data(self, label, CV, ga, gb, gamp, gmean, gmeanabs, gsigma, gfwhm, gsigmaum, gfwhmum, xdata, yprofile, ygauss):
        save_dataset("info/bpm", label)
        save_dataset("camera/exposure", self.cam_exposure)
        save_dataset("camera/gain", self.cam_gain)
        save_dataset("camera/conv_factor", CV)
        save_dataset("ROI/MinX", self.roiposx)
        save_dataset("ROI/MinY", self.roiposy)
        save_dataset("ROI/SizeX", self.roisizex)
        save_dataset("ROI/SizeY", self.roisizey)
        save_dataset("gaussian/inclination", ga)
        save_dataset("gaussian/background", gb)
        save_dataset("gaussian/amplitude", gamp)
        save_dataset("gaussian/mean_roi", gmean)
        save_dataset("gaussian/mean_absolute", gmeanabs)
        save_dataset("gaussian/sigma", gsigma)
        save_dataset("gaussian/FWHM", gfwhm)
        save_dataset("gaussian/sigma_um", gsigmaum)
        save_dataset("gaussian/FWHM_um", gfwhmum)
        save_dataset("plot/xdata", xdata)
        save_dataset("plot/profile", yprofile)
        save_dataset("plot/gauss_fit", ygauss)
