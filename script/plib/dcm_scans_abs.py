class DCMSCAN():
    def __init__(self):
        pass

    ########################################
    ### scan
    ########################################

    def scan(self, *args, **kwargs):
        ## arguments
        detector=kwargs["detector"]
        start=kwargs["start"]
        end=kwargs["end"]
        step=kwargs["step"]
        exposure=kwargs["exposure"]
        point_delay=kwargs["point_delay"]
        sample=kwargs["sample"]

        DEBUG=False

        print("DCM Transmission Scan")

        ## reset EPICS connections
        Epics.destroy()
        Epics.create()

        if DEBUG: log("Creating channels...", data_file = False)
        ## Creating channels
        if detector != "bpm3":
            raise Exception("Detector not available")

        SENSOR = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
        SENSOR.setMonitored(True)
        ACQ = create_channel_device("PINK:DG01:TriggerDelayBO", type='d')
        SENSOR_profile = create_channel_device("PINK:CAE1:image3:ArrayData", type='[d', size=100)
        SENSOR_profile.setMonitored(True)
        ENERGY = create_channel_device("u171dcm1:monoSetEnergy")
        ENERGY_RBV = create_channel_device("u171dcm1:monoGetEnergy")
        ENERGY_RBV.setMonitored(True)
        ENERGY_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
        ENERGY_DMOV.setMonitored(True)
        #ENERGY_DMOV = tdmov
        GAP = create_channel_device("U17IT6R:Ndi1PmsPos")
        GAP.setMonitored(True)
        IZERO = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
        IZERO.setMonitored(True)
        IZero_profile = create_channel_device("PINK:CAE2:image1:ArrayData", type='[d', size=100)
        IZero_profile.setMonitored(True)
        IZeroposx = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV")
        IZeroposx.setMonitored(True)
        IZeroposy = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV")
        IZeroposy.setMonitored(True)        
        BPM2VerLength = int(caget("PINK:PG04:ROI1:SizeY_RBV"))
        BPM2Ver = create_channel_device("PINK:PG04:Stats2:ProfileAverageY_RBV", type='[d', size=BPM2VerLength)
        BPM2HorLength = int(caget("PINK:PG04:ROI1:SizeX_RBV"))
        BPM2Hor = create_channel_device("PINK:PG04:Stats2:ProfileAverageX_RBV", type='[d', size=BPM2HorLength)
        BPM2xfit = create_channel_device("PINK:BPM:2:X:center")
        BPM2xfit.setMonitored(True)
        BPM2yfit = create_channel_device("PINK:BPM:2:Y:center")
        BPM2yfit.setMonitored(True)
        BPM3xfit = create_channel_device("PINK:BPM:3:X:center")
        BPM3xfit.setMonitored(True)
        BPM3yfit = create_channel_device("PINK:BPM:3:Y:center")
        BPM3yfit.setMonitored(True)

        ## delay to connect epics channels
        sleep(2)

        ##Channel cut
        ch_cut_status = caget("u171dcm1:disableCT", type='i')

        ## plot vectors
        energy = []
        izero = []
        sensor = []
        absvec = []

        ## Setup
        #print("Scanning ...")
        set_exec_pars(open=False, name="dcm", reset=True)
        save_dataset("scan/scantype", "DCM transmission scan")
        save_dataset("scan/detector", "diode bpm3") 
        save_dataset("scan/sample", sample) 
        save_dataset("scan/start_time", time.ctime())
        save_dataset("dcm/channel_cut", ch_cut_status)
        save_dataset("dcm/IdSlope", caget("u171dcm1:aiIdSlope"))
        save_dataset("dcm/IdOffset", caget("u171dcm1:aiIdOffset"))

        ## create datasets
        create_dataset("data/energy", 'd', False)
        create_dataset("data/bpm3diode", 'd', False)
        create_dataset("data/bpm3diode_profile", 'd', False, (0, 100))
        create_dataset("data/izero", 'd', False)
        create_dataset("data/izero_profile", 'd', False, (0, 100))
        create_dataset("data/izero_posx", 'd', False)
        create_dataset("data/izero_posy", 'd', False)
        create_dataset("data/gap", 'd', False)
        create_dataset("data/transmission", 'd', False)
        create_dataset("bpm2/X", 'd', False)
        create_dataset("bpm2/X_profile", 'd', False, (0, BPM2HorLength))
        create_dataset("bpm2/Y", 'd', False)
        create_dataset("bpm2/Y_profile", 'd', False, (0, BPM2VerLength))
        create_dataset("bpm3/X", 'd', False)
        create_dataset("bpm3/Y", 'd', False)

        ## configure scan positions
        positionarray = linspace(start, end, step)

        detlabel = "diode bpm3"

        ## print some info
        print("******************************************************")
        print("  Filename: " + self.get_filename())
        print(" Scan type: dcm transmission scan")
        print("  Detector: {}".format(detector))
        print("    Energy: [{:.0f},{:.0f}]".format(start, end))
        print("    Points: {}".format(len(positionarray)))
        print("  Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        print("    Sample: {}".format(sample))
        print("******************************************************")

        fcall="scan.dcm_transmission(detector.{}(), start={}, end={}, step={}, exposure={}, point_delay={}, sample={})".format(detector,start,end,step,exposure, point_delay, sample)
        elab.put(fcall)
        elab.put("******************************************************")
        elab.put("Filename: " + self.get_filename())
        elab.put("Scan type: dcm transmission scan")
        elab.put("Detector: {}".format(detector))
        elab.put("Energy: [{:.0f},{:.0f}]".format(start, end))
        elab.put("Points: {}".format(len(positionarray)))
        elab.put("Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        elab.put("Sample: {}".format(sample))
        elab.put("******************************************************")        

        if DEBUG: log("Setting up Caenels...", data_file = False)
        ## Setup caenels
        caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:TriggerMode", 1)
        caput("PINK:CAE2:AcquireMode", 0)
        caputq("PINK:CAE2:Acquire", 1)

        caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:TriggerMode", 1)
        caput("PINK:CAE1:AcquireMode", 0)
        caputq("PINK:CAE1:Acquire", 1)

        if DEBUG: log("Setup plot...", data_file = False)
        ## Setup plot
        plottitle="IZero"
        plottitle2="Diode BPM3"
        plottitle3="Normalized"
        [p1, p2, p3]=plot([None, None, None], [plottitle, plottitle2, plottitle3], title="DCM Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p2.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p3.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p1.getAxis(p1.AxisId.X).setLabel("Energy")
        p2.getAxis(p1.AxisId.X).setLabel("Energy")
        p3.getAxis(p1.AxisId.X).setLabel("Energy")
        set_setting("last_plot_title", "DCM Scan")

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, 0], [0, 0], [0, 0], [0, 0.01])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 0)
        caput("PINK:RPISW:select_B", 1)

        if DEBUG: log("Moving to first pos", data_file = False)
        ## Moving to first position
        print("Moving DCM to start position...")
        pos = positionarray[0]
        ENERGY.write(pos)
        sleep(1)
        self.__waitdcm(ENERGY_DMOV, point_delay)

        if DEBUG: log("Scanning...", data_file = False)
        print("Scanning...")

        ## open shutter
        caput("PINK:PLCGAS:ei_B01", 1)

        ## Scan loop
        try:
            ## Main loop
            for pos in positionarray:
                ## move and wait for dcm
                if DEBUG: log("moving", data_file = False)
                ENERGY.write(pos)
                sleep(0.5)
                self.__waitdcm(ENERGY_DMOV, point_delay)

                ##trigger acq
                if DEBUG: log("trigger", data_file = False)
                ACQ.write(1)
                resp = SENSOR.waitCacheChange(int(1000*(exposure+2)))
                sleep(0.2)

                ## append to vecto
                if DEBUG: log("appending to vectors", data_file = False)
                energy.append(ENERGY_RBV.read())
                #energy.append(pos)
                sensor.append(SENSOR.read())
                izero.append(IZERO.read())
                transcalc = math.log(abs(IZERO.take())/abs(SENSOR.take()))
                absvec.append(transcalc)

                ## update plot
                if DEBUG: log("updating plots", data_file = False)
                p1.getSeries(0).setData(energy, izero)
                p2.getSeries(0).setData(energy, sensor)
                p3.getSeries(0).setData(energy, absvec)

                ## append data
                if DEBUG: log("appending data", data_file = False)
                append_dataset("data/energy", ENERGY_RBV.take())
                append_dataset("data/bpm3diode", SENSOR.take())
                append_dataset("data/bpm3diode_profile", SENSOR_profile.take())
                append_dataset("data/izero", IZERO.take())
                append_dataset("data/izero_profile", IZero_profile.take())
                append_dataset("data/izero_posx", IZeroposx.take())
                append_dataset("data/izero_posy", IZeroposy.take())
                append_dataset("data/gap", GAP.read())
                append_dataset("data/transmission", transcalc)
                append_dataset("bpm2/X", BPM2xfit.value)
                append_dataset("bpm2/Y", BPM2yfit.value)
                append_dataset("bpm2/X_profile", BPM2Hor.read())
                append_dataset("bpm2/Y_profile", BPM2Ver.read())
                append_dataset("bpm3/X", BPM3xfit.value)
                append_dataset("bpm3/Y", BPM3yfit.value)                

        except Exception, value:
            print("DCM scan aborted.")
            print(value)

        ## close shutter
        caput("PINK:PLCGAS:ei_B01", 0)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        ## save data
        save_dataset("scan/end_time", time.ctime())
        save_dataset("plot/title", plottitle)
        save_dataset("plot/title2", plottitle2)
        save_dataset("plot/title3", plottitle3)
        save_dataset("plot/x", positionarray)
        create_dataset("plot/y", 'd', False, (0, len(izero)))
        append_dataset("plot/y", izero)
        create_dataset("plot/y2", 'd', False, (0, len(sensor)))
        append_dataset("plot/y2", sensor)
        create_dataset("plot/y3", 'd', False, (0, len(absvec)))
        append_dataset("plot/y3", absvec)
        save_dataset("plot/y_desc", "")
        save_dataset("plot/xlabel", "Energy")
        save_dataset("plot/ylabel", "")

        #send plot to elab
        if caget("PINK:SESSION:sessionstate", type='i'):
            elab.send_last_plot()  

        print("Scan finished")


    ## extra functions
    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname

    def __waitdcm(self, dmov, delay):
        ## sim
        #while(dmov.read()<1):
        while(dmov.value):
            sleep(0.1)
        sleep(delay)

    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])

