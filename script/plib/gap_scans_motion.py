#gap_scans.py

class GAPSCAN():
    def __init__(self):
        pass

########################################
### scan
########################################

    def scan(self, start=0, end=0, step=0, exposure=0.0, fit=False, trigger=None, reverse=None):

        if exposure==0:
            print("abort: exposure = 0")
            return

        Epics.destroy()
        Epics.create()

        #set_exec_pars(open=False, name="gap", reset=True)

        verbose=True
        DEBUG=False

        if verbose: print("Creating channels")

        ## channels
        MOTOR = create_channel_device("U17IT6R:BaseParGapsel.B")
        MOTOR_SET = create_channel_device("U17IT6R:BaseCmdCalc.PROC")
        MOTOR_RBV = create_channel_device("U17IT6R:Ndi1PmsPos")
        MOTOR_RBV.setMonitored(True)
        motor_deadband = 0.001

        SENSOR = create_channel_device("u171dcm1:monoGetEnergy")
        SENSOR.setMonitored(True)

        ## variables
        sensor = []
        motor = []

        ## Setup
        #print("Scanning ...")
        set_exec_pars(open=False, name="gap", reset=True)
        save_dataset("scan/scantype", "Gap scan")

        ## configure scan positions
        positionarray = linspace(start, end, step)

        ## plot setup
        if verbose: print("Setup plot")
        [p1] = plot(None, "Scan", title="Gap Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p1.addSeries(LinePlotSeries("Fit"))
        set_setting("last_plot_title", "Gap Scan")

        ## print some info
        print("******************************************************")
        print("   Filename: " + self.get_filename())
        print("  Scan type: gap motion")
        print("      Start: {}".format(start))
        print("        End: {}".format(end))
        print("       Step: {}".format(step))
        print("   Exposure: {}".format(exposure))
        print("******************************************************")

        elab.put("******************************************************")
        elab.put("Filename: " + self.get_filename())
        elab.put("Scan type: gap motion")
        elab.put("Start: {}".format(start))
        elab.put("End: {}".format(end))
        elab.put("Step: {}".format(step))
        elab.put("Exposure: {}".format(exposure))
        elab.put("******************************************************")

        if verbose: print("Moving gap to start position")
        pos = positionarray[0]
        MOTOR.write(pos)
        MOTOR_SET.write(1)
        while(abs(pos-MOTOR_RBV.read()) > motor_deadband):
            mystat = "Gap: " + str(MOTOR_RBV.take())
            set_status(mystat)
            sleep(0.5)
        #MOTOR_RBV.waitValueInRange(positionarray[0], motor_deadband, 60000)
        mystat = "Gap: " + str(MOTOR_RBV.take())
        set_status(mystat)

        if trigger!=None:
            print("Waiting for dcm to reach trigger energy: {:.2f} eV".format(trigger))
            if reverse==None:
                while(SENSOR.value<trigger):
                    sleep(0.1)
            else:
                while(SENSOR.value>trigger):
                    sleep(0.1)

        print("Scanning...")
        try:
            ## Main loop
            for pos in positionarray:
                MOTOR.write(pos)
                sleep(0.05)
                MOTOR_SET.write(1)
                sleep(exposure/2)
                sensor.append(SENSOR.take())
                motor.append(MOTOR_RBV.take())
                p1.getSeries(0).setData(motor, sensor)
                sleep(exposure/2)
        except:
            print("Gap scan aborted.")

        save_dataset("plot/x", motor)
        create_dataset("plot/y", 'd', False, (0, len(sensor)))
        append_dataset("plot/y", sensor)
        save_dataset("plot/title", "Gap Scan")
        save_dataset("plot/xlabel", "Gap")
        save_dataset("plot/ylabel", "Current")
        save_dataset("plot/y_desc", "scan 0")
        #save_dataset("scan/scantype", "Gap scan without fitting")
        save_dataset("raw/izero", sensor)
        save_dataset("raw/gap", motor)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        ## elab send plot
        if caget("PINK:SESSION:sessionstate", type='i'):
            elab.send_last_plot()

        print("Scan finished. OK")

###########################################
### Support functions
###########################################

    def __gauss_fit(self, ydata, xdata):
        (a, b, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i)+(a*i)+b for i in xgauss]
        fwhm = 2.355*sigma
        return (a, b, amp, com, sigma, gauss, fwhm, xgauss)

    def __gauss_exp_fit(self, ydata, xdata):
        (incl, off, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        start_point=[1, 1, amp, com, sigma]
        (eamp, decay, amp, com, sigma) = fit_gaussian_exp_bkg(ydata, xdata, start_point=start_point)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i) + eamp*math.exp(-i/decay) for i in xgauss]
        fwhm = 2.355*sigma
        return (eamp, decay, amp, com, sigma, gauss, fwhm, xgauss)

    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname         
