class PINKDEV_CCD:
    def __init__(self):
        self.test = "test ccd"
    def takebackground(self, exposure=1.0, images=1, discard=0, **kwgs):
        DEBUG=True
        set_status("CCD background images with Fast Fourier filtering")
        if DEBUG: print("CCD background images with Fast Fourier filtering")
        thrs_pv = "PINK:AUX:FFT:Thrs"
        fft_start_pv = "PINK:AUX:FFT:start"
        fft_ready_pv = "PINK:AUX:FFT:ready"
        fft_max_pv = "PINK:AUX:FFT:max"
        fft_bkg_pv = "PINK:AUX:FFT:bkg"
        fft_max2bkg_pv = "PINK:AUX:FFT:max2bkg"
        fft_enable_pv = "PINK:AUX:FFT:enable"
        fft_maxbgresets_pv = "PINK:AUX:FFT:maxbgresets"
        #spectrum_pv = "PINK:GEYES:spectrum_RBV"

        passpath="pass01"
        ##################################################################################################################################


        GE_X_fft = caget("PINK:GEYES:image4:ArraySize0_RBV")
        GE_Y_fft = caget("PINK:GEYES:image4:ArraySize1_RBV")
        GE_raw_array = create_channel_device("PINK:GEYES:image1:ArrayData", type='[d', size=int(GE_X_fft*GE_Y_fft))
        GE_raw_array.setMonitored(True)
        max_bgresets = caget(fft_maxbgresets_pv)
        
        ##################################################################################################################################

        if 'ccdid' in kwgs:
            ccdid = kwgs['ccdid']
        else:
            ccdid=None
        
        #stops ccd
        isrunning = caget("PINK:GEYES:cam1:DetectorState_RBV", type='i')
        if isrunning:
            set_status("CCD stopping")
            caputq("PINK:GEYES:cam1:Acquire", 0)
            # waits for status change
            self.__waitforvalue("PINK:GEYES:cam1:DetectorState_RBV", 0, type='i', ccdid=None)
            #timeout= 1.0 + caget("PINK:GEYES:cam1:AcquireTime_RBV", type='d') 
            #cawait("PINK:GEYES:cam1:DetectorState_RBV", 0, timeout=timeout, type='i')
        # setup
        set_status("CCD Setup")
        caput("PINK:PLCGAS:ei_B01", 0) # Close fast shutter
        caput("PINK:GEYES:cam1:AcquireTime", exposure)
        N = images+discard
        caput("PINK:GEYES:cam1:NumImages", 20) #N
        caput("PINK:GEYES:cam1:ImageMode", 1)
        caput("PINK:GEYES:Proc1:EnableBackground", 0)
        caput("PINK:GEYES:Proc1:EnableFilter", 1)
        caput("PINK:GEYES:Proc1:NumFilter", images)
        caput("PINK:GEYES:Proc1:ResetFilter", 1)
        sleep(1)
        set_status("CCD image: 0/{}".format(N))
        if DEBUG: print("CCD image: 0/{}".format(N))
        if ccdid==None:
            id0 = int(caget("PINK:GEYES:cam1:ArrayCounter_RBV"))
        else:
            id0 = int(ccdid.read())
        #timeout = 2*exposure
        caputq("PINK:GEYES:cam1:Acquire", 1)

        #+++++++++++++++++++++++++++ add here while ++++++++++++++++++++++++++++++++++++
        id=0 #frame ID
        idd=0 #ID for discared image
        retry_count = 0

        #max_bgresets = caget(fft_maxbgresets_pv)
        if DEBUG: print("creating data section fo bg_image_attempts...")
        #create_dataset("passes/"+passpath+"/detector/ccd/raw/bg_image_attempts", 'd', False, (0, GE_Y_fft, GE_X_fft), features=data_compression)

        fft_enable=True
        #fft_enable=caget(fft_enable_pv)
        while (idd<N) or (retry_count<=max_bgresets):
            id+=1 #frame ID
            idd+=1 #ID for discared image
            
            if DEBUG: print("loop {}".format(id))
            self.__waitforvalue("PINK:GEYES:cam1:ArrayCounter_RBV", float(id0+id), type='d', ccdid=ccdid) #waiting for frame to arrive
            set_status("CCD image: {}/{}".format(idd, N))
            if DEBUG: print("waiting passed.. appending dataset")
            append_dataset("passes/"+passpath+"/detector/ccd/raw/bg_image_attempts", Convert.reshape(GE_raw_array.take(), GE_Y_fft, GE_X_fft)) #waiting raw image
            if DEBUG: print("saving image...")

            if fft_enable==True:
                Thrs=caget(thrs_pv)
                caput(fft_ready_pv, 0)
                caput(fft_start_pv, 1)

                waittimefft=0
                while caget(fft_ready_pv)==0:
                    sleep(0.1)
                    waittimefft += 0.1
                    if waittimefft>=(exposure/2):
                        print("something went wrong... fft container did not return 'ready' for long time")
                        break
                fft_max=caget(fft_max_pv)
                fft_bkg=caget(fft_bkg_pv)
                max2bkg=caget(fft_max2bkg_pv)
                print("Background attempt {}/{}. Spectrum quality: max2bkg = {:.2f}, Thrs = {:.2f}, max = {:.2f}, bkg = {:.2f}".format(retry_count, max_bgresets, max2bkg, Thrs, fft_max, fft_bkg))
            
                if (max2bkg>Thrs) and (retry_count=<max_bgresets):
                    idd=0
                    retry_count += 1
                    if DEBUG: print("FFT threshold reached.. Reseting filter")
                    caput("PINK:GEYES:Proc1:ResetFilter", 1)
                elif (retry_count>=max_bgresets):
                    if DEBUG: print("FFT threshold reached, but maximum retries exceeded. Proceeding with the current background image.")

            if (discard>0) and (idd==discard):
                caput("PINK:GEYES:Proc1:ResetFilter", 1)
            if DEBUG: print("Idd= {}".format(idd))
            
        if DEBUG: print("exiting cycle....")
        cawait("PINK:GEYES:cam1:DetectorState_RBV", 0, timeout=exposure+0.5, type='i')
        set_status("CCD images acquired")
        # copy bg image to record
        caput("PINK:GEYES:savebg", 1)
        # setup
        caput("PINK:GEYES:Proc1:EnableFilter", 0)
        caput("PINK:GEYES:Proc1:SaveBackground", 1)
        caput("PINK:GEYES:Proc1:EnableBackground", 1)
        set_status(" ")
        #print("CCD images acquired OK")

    def __waitforvalue(self, pvname, value, type='d', ccdid=None):
        if ccdid==None:
            v = caget(pvname, type=type)
            while(v!=value):
                v = caget(pvname, type=type)
                #print("now: {}, target: {}".format(v,value))
                sleep(0.4)
        else:
            v = int(ccdid.read())
            while(v!=value):
                v = int(ccdid.read())
                #print("now: {}, target: {}".format(v,value))
                sleep(0.4)
    





        
