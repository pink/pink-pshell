class PINKDEV_CCD:
    def __init__(self):
        self.test = "test ccd"
    def takebackground(self, exposure=1.0, images=1, discard=0, **kwgs):
        set_status("CCD background images")
        if 'ccdid' in kwgs:
            ccdid = kwgs['ccdid']
        else:
            ccdid=None
        #print("Acquiring CCD background images")
        #stops ccd
        isrunning = caget("PINK:GEYES:cam1:DetectorState_RBV", type='i')
        if isrunning:
            set_status("CCD stopping")
            caputq("PINK:GEYES:cam1:Acquire", 0)
            # waits for status change
            self.__waitforvalue("PINK:GEYES:cam1:DetectorState_RBV", 0, type='i', ccdid=None)
            #timeout= 1.0 + caget("PINK:GEYES:cam1:AcquireTime_RBV", type='d') 
            #cawait("PINK:GEYES:cam1:DetectorState_RBV", 0, timeout=timeout, type='i')
        # setup
        set_status("CCD Setup")
        caput("PINK:PLCGAS:ei_B01", 0) # Close fast shutter
        caput("PINK:GEYES:cam1:AcquireTime", exposure)
        N = images+discard
        caput("PINK:GEYES:cam1:NumImages", N)
        caput("PINK:GEYES:cam1:ImageMode", 1)
        caput("PINK:GEYES:Proc1:EnableBackground", 0)
        caput("PINK:GEYES:Proc1:EnableFilter", 1)
        caput("PINK:GEYES:Proc1:NumFilter", images)
        caput("PINK:GEYES:Proc1:ResetFilter", 1)
        sleep(1)
        set_status("CCD image: 0/{}".format(N))
        if ccdid==None:
            id0 = int(caget("PINK:GEYES:cam1:ArrayCounter_RBV"))
        else:
            id0 = int(ccdid.read())
        #timeout = 2*exposure
        caputq("PINK:GEYES:cam1:Acquire", 1)
        for i in range(N):
            id=i+1
            self.__waitforvalue("PINK:GEYES:cam1:ArrayCounter_RBV", float(id0+id), type='d', ccdid=ccdid)
            #cawait("PINK:GEYES:cam1:ArrayCounter_RBV", int(id0+id), timeout=timeout, type='i')
            set_status("CCD image: {}/{}".format(id, N))
            if (discard>0) and (id==discard):
                caput("PINK:GEYES:Proc1:ResetFilter", 1)
        cawait("PINK:GEYES:cam1:DetectorState_RBV", 0, timeout=exposure+0.5, type='i')
        set_status("CCD images acquired")
        # copy bg image to record
        caput("PINK:GEYES:savebg", 1)
        # setup
        caput("PINK:GEYES:Proc1:EnableFilter", 0)
        caput("PINK:GEYES:Proc1:SaveBackground", 1)
        caput("PINK:GEYES:Proc1:EnableBackground", 1)
        set_status(" ")
        #print("CCD images acquired OK")

    def __waitforvalue(self, pvname, value, type='d', ccdid=None):
        if ccdid==None:
            v = caget(pvname, type=type)
            while(v!=value):
                v = caget(pvname, type=type)
                #print("now: {}, target: {}".format(v,value))
                sleep(0.4)
        else:
            v = int(ccdid.read())
            while(v!=value):
                v = int(ccdid.read())
                #print("now: {}, target: {}".format(v,value))
                sleep(0.4)      





        