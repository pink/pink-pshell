class CAE1:
    def __init__(self):
        ## Data path
        self.meas_ch1 = "/meas/diode_diag"
        self.meas_ch2 = "/meas/TFY"
        self.meas_ch2_profile = "/meas/TFY_profile"
        self.meas_ch3 = "/meas/diode_bpm"
        self.meas_ch4 = "/meas/ch4"
        self.attb_ch1_name = "Diode_diag"
        self.attb_ch2_name = "TFY"
        self.attb_ch3_name = "Diode_BPM"
        self.attb_ch4_name = "CH4"
        ## PVs
        self.n_pv="PINK:CAE1:NumAcquire"
        self.acquire_pv="PINK:CAE1:Acquire"
        self.ch1_pv="PINK:CAE1:Current1:MeanValue_RBV"
        self.ch2_pv="PINK:CAE1:Current2:MeanValue_RBV"
        self.ch2_profile_pv="PINK:CAE1:image2:ArrayData"
        self.ch3_pv="PINK:CAE1:Current3:MeanValue_RBV"
        self.ch4_pv="PINK:CAE1:Current4:MeanValue_RBV"
        # Acq mode, 0:Continuos, 1:Multiple, 2:Single
        self.acquire_mode_pv="PINK:CAE1:AcquireMode"
        self.averaging_time_pv="PINK:CAE1:AveragingTime"
        self.values_per_read_pv="PINK:CAE1:ValuesPerRead"
        # Trig mode, 0:free run, 1:ext trig
        self.trigger_mode_pv="PINK:CAE1:TriggerMode"
        self.id_pv="PINK:CAE1:image1:UniqueId_RBV"
        # Range, 0:1E-3, 1:1E-6
        self.range_pv="PINK:CAE1:Range"
        ## channels
        self.ch1_ch = create_channel_device(self.ch1_pv, type='d')
        self.ch1_ch.setMonitored(True)
        self.ch2_ch = create_channel_device(self.ch2_pv, type='d')
        self.ch2_ch.setMonitored(True)
        self.ch2_profile_ch = create_channel_device(self.ch2_profile_pv, type='[d', size=100)
        self.ch2_profile_ch.setMonitored(True)
        self.ch3_ch = create_channel_device(self.ch3_pv, type='d')
        self.ch3_ch.setMonitored(True)
        self.ch4_ch = create_channel_device(self.ch4_pv, type='d')
        self.ch4_ch.setMonitored(True)
        self.id_ch = create_channel_device(self.id_pv, type='d')
        self.id_ch.setMonitored(True)
        ## attributes
        self.exposure=1
        self.en_list = [0,0,0,0]

    def enable(self, en_list):
        # Channels: 1:diag diode, 2:tfy, 3:bpm diode, 4:NC
        self.en_list = en_list

    def set_trigger_SW(self, exposure):
        self.exposure=exposure
        caput(self.acquire_mode_pv, 2)
        caput(self.averaging_time_pv, exposure)
        caput(self.values_per_read_pv, exposure*1000)
        caput(self.trigger_mode_pv, 0)

    def trigger(self):
        caputq(self.acquire_pv, 1)

    def trigger_and_wait(self):
        caputq(self.acquire_pv, 1)
        self.id_ch.waitCacheChange(int((self.exposure*1000)+1000))

    def read(self):
        val_list = [self.ch1_ch.read(), self.ch2_ch.read(), self.ch3_ch.read(), self.ch4_ch.read()]
        return val_list

    def take(self):
        val_list = [self.ch1_ch.take(), self.ch2_ch.take(), self.ch3_ch.take(), self.ch4_ch.take()]
        return val_list

    def test(self):
        print(self.n_pv)

    def help(self):
        print("ch1: Diode diag\nch2: TFY\nch3: Diode BPM\nch4: NC")

    ## Data saving
    def create_dset(self):
        if self.en_list[0]:
            create_dataset(self.meas_ch1, 'd', False)
        if self.en_list[1]:
            create_dataset(self.meas_ch2, 'd', False)
            create_dataset(self.meas_ch2_profile, 'd', False, (0, 100))
        if self.en_list[2]:
            create_dataset(self.meas_ch3, 'd', False)
        if self.en_list[3]:
            create_dataset(self.meas_ch4, 'd', False)

    def append_dset(self):
        if self.en_list[0]:
            append_dataset(self.meas_ch1, self.ch1_ch.take())
        if self.en_list[1]:
            append_dataset(self.meas_ch2, self.ch2_ch.take())
            append_dataset(self.meas_ch2_profile, self.ch2_profile_ch.take())
        if self.en_list[2]:
            append_dataset(self.meas_ch3, self.ch3_ch.take())
        if self.en_list[3]:
            append_dataset(self.meas_ch4, self.ch4_ch.take())

    def save_attb(self):
        if self.en_list[0]:
            set_attribute(PINK_attb_path, self.attb_ch1_name, self.ch1_ch.read())
        if self.en_list[1]:
            set_attribute(PINK_attb_path, self.attb_ch2_name, self.ch2_ch.read())
        if self.en_list[2]:
            set_attribute(PINK_attb_path, self.attb_ch3_name, self.ch3_ch.read())
        if self.en_list[3]:
            set_attribute(PINK_attb_path, self.attb_ch4_name, self.ch4_ch.read())

class CAE2:
    def __init__(self):
        ## Data path
        self.meas_izero = "/meas/izero"
        self.meas_izero_profile = "/meas/izero_profile"
        self.attb_name = "izero"
        ## PVs
        self.n_pv="PINK:CAE2:NumAcquire"
        self.acquire_pv="PINK:CAE2:Acquire"
        self.izero_pv="PINK:CAE2:SumAll:MeanValue_RBV"
        self.izero_profile_pv="PINK:CAE2:image1:ArrayData"
        # Acq mode, 0:Continuos, 1:Multiple, 2:Single
        self.acquire_mode_pv="PINK:CAE2:AcquireMode"
        self.averaging_time_pv="PINK:CAE2:AveragingTime"
        self.values_per_read_pv="PINK:CAE2:ValuesPerRead"
        # Trig mode, 0:free run, 1:ext trig
        self.trigger_mode_pv="PINK:CAE2:TriggerMode"
        self.id_pv="PINK:CAE2:image1:UniqueId_RBV"
        ## channels
        self.izero_ch = create_channel_device(self.izero_pv, type='d')
        self.izero_ch.setMonitored(True)
        self.izero_profile_ch = create_channel_device(self.izero_profile_pv, type='[d', size=100)
        self.izero_profile_ch.setMonitored(True)
        self.id_ch = create_channel_device(self.id_pv, type='d')
        self.id_ch.setMonitored(True)
        self.exposure=1

    def set_trigger_SW(self, exposure):
        self.exposure=exposure
        caput(self.acquire_mode_pv, 2)
        caput(self.averaging_time_pv, exposure)
        caput(self.values_per_read_pv, exposure*1000)
        caput(self.trigger_mode_pv, 0)

    def trigger_and_wait(self):
        caputq(self.acquire_pv, 1)
        self.id_ch.waitCacheChange(int((self.exposure*1000)+1000))

    def read(self):
        return self.izero_ch.read()

    def take(self):
        return self.izero_ch.take()

    def test(self):
        print(self.n_pv)

    ## Data saving
    def create_dset(self):
        create_dataset(self.meas_izero, 'd', False)
        create_dataset(self.meas_izero_profile, 'd', False, (0, 100))

    def append_dset(self):
        append_dataset(self.meas_izero, self.izero_ch.take())
        append_dataset(self.meas_izero_profile, self.izero_profile_ch.take())


    def save_attb(self):
        set_attribute(PINK_attb_path, self.attb_name, self.izero_ch.read())


       