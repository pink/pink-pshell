if get_exec_pars().args:
    filename=get_exec_pars().args[0]
else:  
    err = "Invalid argument"
    print err
    raise Exception(err)

def pink_plot(filename):
    pname = filename.split('_')
    pname = pname[2]
    
    ## load data from file
    try:
        title = load_data("plot/title", root=filename)
        xlabel = load_data("plot/xlabel", root=filename)
        ylabel = load_data("plot/ylabel", root=filename)
        x = load_data("plot/x", root=filename)
        y = load_data("plot/y", root=filename)
        y_desc = load_data("plot/y_desc", root=filename)
        if get_data_info("plot/title2", root = filename).containsKey("Data Type"):
            title2 = load_data("plot/title2", root=filename)
            y2 = load_data("plot/y2", root=filename)
            plottype=2
        else:
            plottype=1
    except:
        err = "Invalid pink file"
        print err
        raise Exception(err)

    if plottype==1:
        p1h=plot(None, y_desc[0], title=pname)
        p1 = p1h.get(0)
        p1.setTitle(title)
        p1.getAxis(p1.AxisId.X).setLabel(xlabel)
        p1.getAxis(p1.AxisId.Y).setLabel(ylabel)
        for i in range(len(y)):
            p1.addSeries(LinePlotSeries(y_desc[i]))
            p1.getSeries(i).setData(x, y[i])
        p1.setLegendVisible(True)
    elif plottype==2:
        [p1, p2]=plot([None, None], [title, title2], title=pname)    
        p1.setTitle(title)
        p1.getAxis(p1.AxisId.X).setLabel(xlabel)
        p1.getAxis(p1.AxisId.Y).setLabel(ylabel)
        p2.getAxis(p2.AxisId.X).setLabel(xlabel)
        p2.getAxis(p2.AxisId.Y).setLabel(ylabel)
        p1.getSeries(0).setData(x, y[0])
        p2.getSeries(0).setData(x, y2[0])
    else:
        err = "Invalid pink file"
        print err
        raise Exception(err)
        
    set_setting("last_plot_title", pname)
    elab.send_last_plot()

pink_plot(filename)        