class DCMSCAN():
    def __init__(self):
        pass

    ########################################
    ### scan
    ########################################

    def scan(self, *args, **kwargs):
        ## arguments
        detector=kwargs["detector"]
        start=kwargs["start"]
        end=kwargs["end"]
        step=kwargs["step"]
        exposure=kwargs["exposure"]
        point_delay=kwargs["point_delay"]
        hroifrom=kwargs["hroifrom"]
        hroito=kwargs["hroito"]
        sample=kwargs["sample"]

        #clean up epics connections
        Epics.destroy()
        Epics.create()
        
        DEBUG=False
        data_compression = {"compression":"True", "shuffle":"True"}

        print("DCM Emission Scan")
                
        if DEBUG: log("Creating channels...", data_file = False)

        if detector=="eiger":
            DET_ROI_X = caget("PINK:EIGER:image3:ArraySize0_RBV")
            DET_ROI_Y = caget("PINK:EIGER:image3:ArraySize1_RBV")        
            SENSOR = create_channel_device("PINK:EIGER:specint", type='d')
            SENSOR.setMonitored(True)
            ACQ = create_channel_device("PINK:EIGER:cam1:Trigger", type='d')
            DET_frameID = create_channel_device("PINK:EIGER:cam1:ArrayCounter_RBV", type='d')
            DET_frameID.setMonitored(True)
            DET_roi_array = create_channel_device("PINK:EIGER:image3:ArrayData", type='[d', size=int(DET_ROI_X*DET_ROI_Y))
            DET_roi_array.setMonitored(True)
            DET_Spectra = create_channel_device("PINK:EIGER:spectrum_RBV", type='[d', size=DET_ROI_X)
            DET_Spectra.setMonitored(True)
            DET_roiline = caget("PINK:EIGER:ROI1:MinY_RBV")
            if type(hroifrom)==type(None):
                DET_roi_h_start = caget("PINK:EIGER:roie0")
            else:
                DET_roi_h_start = hroifrom
                caput("PINK:EIGER:roie0", hroifrom)   
                             
            if type(hroito)==type(None):                
                DET_roi_h_end = caget("PINK:EIGER:roie1")
            else:
                DET_roi_h_end = hroito
                caput("PINK:EIGER:roie1", hroito)
        elif detector=="ge":
            DET_Y = caget("PINK:GEYES:image1:ArraySize0_RBV")
            DET_X = caget("PINK:GEYES:image1:ArraySize1_RBV")
            ## CCD background
            print("Acquiring CCD background...")
            ## Setup delay generator
            ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
            ## [trigger mode] [5:single shot] [1: Ext rising edge]
            self.setup_delaygen(5, [0, exposure-0.02], [0, exposure+0.15], [0, 0], [0, 0.001])

            ## take GE background image
            #self.save_GE_BG(exposure)
            DEVCCD = pinkdevice.ccd()
            DEVCCD.takebackground(exposure=exposure, images=3, discard=3)  
            BGrawArray = caget("PINK:GEYES:image1:ArrayData", type='[d', size=int(DET_X*DET_Y))

            DET_ROI_X = caget("PINK:GEYES:image3:ArraySize0_RBV")
            DET_ROI_Y = caget("PINK:GEYES:image3:ArraySize1_RBV") 
            SENSOR = create_channel_device("PINK:GEYES:specint", type='d')
            SENSOR.setMonitored(True)
            ACQ = create_channel_device("PINK:GEYES:cam1:Acquire", type='d')
            DET_frameID = create_channel_device("PINK:GEYES:cam1:ArrayCounter_RBV", type='d')
            DET_frameID.setMonitored(True)
            DET_roi_array = create_channel_device("PINK:GEYES:image3:ArrayData", type='[d', size=int(DET_ROI_X*DET_ROI_Y))
            DET_roi_array.setMonitored(True)
            DET_raw_array = create_channel_device("PINK:GEYES:image1:ArrayData", type='[d', size=int(DET_X*DET_Y))
            DET_raw_array.setMonitored(True)            
            DET_Spectra = create_channel_device("PINK:GEYES:spectrum_RBV", type='[d', size=DET_ROI_X)
            DET_Spectra.setMonitored(True)    
            DET_roiline = caget("PINK:GEYES:ROI1:MinY_RBV")
            if type(hroifrom)==type(None):
                DET_roi_h_start = caget("PINK:GEYES:roie0")
            else:
                DET_roi_h_start = hroifrom
                caput("PINK:GEYES:roie0", hroifrom)                
            if type(hroito)==type(None):                
                DET_roi_h_end = caget("PINK:GEYES:roie1")
            else:
                DET_roi_h_end = hroito
                caput("PINK:GEYES:roie1", hroito)            
        else:
            raise Exception("Detector not available")
        
        ## DCM
        ENERGY = create_channel_device("u171dcm1:monoSetEnergy")
        ENERGY_RBV = create_channel_device("u171dcm1:monoGetEnergy")
        ENERGY_RBV.setMonitored(True)
        ENERGY_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
        ENERGY_DMOV.setMonitored(True)
        #ENERGY_DMOV = tdmov
        GAP = create_channel_device("U17IT6R:Ndi1PmsPos")
        GAP.setMonitored(True)
        IZERO = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
        IZERO.setMonitored(True)
        IZero_profile = create_channel_device("PINK:CAE2:image1:ArrayData", type='[d', size=100)
        IZero_profile.setMonitored(True)        
        IZeroposx = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV")
        IZeroposx.setMonitored(True)
        IZeroposy = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV")
        IZeroposy.setMonitored(True)        
        BPM2VerLength = int(caget("PINK:PG04:ROI1:SizeY_RBV"))
        BPM2Ver = create_channel_device("PINK:PG04:Stats2:ProfileAverageY_RBV", type='[d', size=BPM2VerLength)
        BPM2HorLength = int(caget("PINK:PG04:ROI1:SizeX_RBV"))
        BPM2Hor = create_channel_device("PINK:PG04:Stats2:ProfileAverageX_RBV", type='[d', size=BPM2HorLength)
        BPM2xfit = create_channel_device("PINK:BPM:2:X:center")
        BPM2xfit.setMonitored(True)
        BPM2yfit = create_channel_device("PINK:BPM:2:Y:center")
        BPM2yfit.setMonitored(True)
        DBPM3 = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV", type='d')
        DBPM3.setMonitored(True)
        DBPM3_profile = create_channel_device("PINK:CAE1:image3:ArrayData", type='[d', size=100)
        DBPM3_profile.setMonitored(True)
        BPM3xfit = create_channel_device("PINK:BPM:3:X:center")
        BPM3xfit.setMonitored(True)
        BPM3yfit = create_channel_device("PINK:BPM:3:Y:center")
        BPM3yfit.setMonitored(True)

        ##Channel cut
        ch_cut_status = caget("u171dcm1:disableCT", type='i')

        ## handles BPM3 diode insertion
        diode3Out = caget("PINK:PLCGAS:vdiode_FA_LS", type='i')
        if diode3Out:
            caput("PINK:PLCGAS:vdiode_FA_open", 1)

        ## plot vectors
        energy = []
        izero = []
        sensor = []
        absvec = []

        ## Setup
        #print("Scanning ...")
        set_exec_pars(open=False, name="dcm", reset=True)
        save_dataset("scan/scantype", "DCM Emission scan")   
        save_dataset("scan/detector", detector)   
        save_dataset("scan/start_time", time.ctime())
        save_dataset("scan/sample", sample)
        save_dataset("dcm/channel_cut", ch_cut_status)
        save_dataset("dcm/IdSlope", caget("u171dcm1:aiIdSlope"))
        save_dataset("dcm/IdOffset", caget("u171dcm1:aiIdOffset"))

        ## Saving detectors settings
        save_dataset("detector/exposure", exposure)
        save_dataset("detector/roi_line", DET_roiline)
        save_dataset("detector/roi_sizex", DET_ROI_X)
        save_dataset("detector/roi_sizey", DET_ROI_Y)
        save_dataset("detector/roi_h_start", DET_roi_h_start)
        save_dataset("detector/roi_h_end", DET_roi_h_end)

        if detector=="eiger":
            save_dataset("detector/eiger_energy", caget("PINK:EIGER:cam1:PhotonEnergy"))
            save_dataset("detector/eiger_threshold", caget("PINK:EIGER:cam1:ThresholdEnergy"))        

        ## create datasets
        create_dataset("detector/processed/image", 'd', False, (0, DET_ROI_Y, DET_ROI_X), features=data_compression)
        if detector=="ge":
            create_dataset("detector/raw/image", 'd', False, (0, DET_Y, DET_X), features=data_compression)
            save_dataset("detector/raw/bg_image", Convert.reshape(BGrawArray, DET_Y, DET_X), features=data_compression)

        create_dataset("detector/processed/spectrum", 'd', False, (0, DET_ROI_X))
        create_dataset("detector/raw/frame_id", 'd', False)
        create_dataset("data/energy", 'd', False)
        create_dataset("data/detector", 'd', False)
        create_dataset("data/izero", 'd', False)
        create_dataset("data/izero_profile", 'd', False, (0, 100))
        create_dataset("data/gap", 'd', False)
        create_dataset("data/emission", 'd', False)
        create_dataset("data/izero_posx", 'd', False)
        create_dataset("data/izero_posy", 'd', False)
        create_dataset("bpm2/X", 'd', False)
        create_dataset("bpm2/X_profile", 'd', False, (0, BPM2HorLength))
        create_dataset("bpm2/Y", 'd', False)
        create_dataset("bpm2/Y_profile", 'd', False, (0, BPM2VerLength))
        create_dataset("bpm3/X", 'd', False)
        create_dataset("bpm3/Y", 'd', False)
        create_dataset("bpm3/diode", 'd', False)
        create_dataset("bpm3/diode_profile", 'd', False, (0, 100))

        ## configure scan positions
        positionarray = linspace(start, end, step)    

        detlabel=self.getdetlabel(detector)

        ## print some info
        print("******************************************************")
        print("  Filename: " + self.get_filename())
        print(" Scan type: DCM emission scan")
        print("  Detector: {}".format(detlabel))
        print("    Energy: [{:.0f},{:.0f}]".format(start, end))
        print("    Points: {}".format(len(positionarray)))
        print("  Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        print("    Sample: {}".format(sample))
        print("******************************************************")

        fcall="scan.dcm_emission(detector.{}(), start={}, end={}, step={}, exposure={}, hroifrom={}, hroito={}, point_delay={}, sample={})".format(detlabel,start,end,step,exposure,hroifrom,hroito,point_delay,sample)
        elab.put(fcall)
        elab.put("******************************************************")
        elab.put("Filename: " + self.get_filename())
        elab.put("Scan type: DCM emission scan")
        elab.put("Detector: {}".format(detlabel))
        elab.put("Energy: [{:.0f},{:.0f}]".format(start, end))
        elab.put("Points: {}".format(len(positionarray)))
        elab.put("Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        elab.put("Sample: {}".format(sample))
        elab.put("******************************************************")  

        if DEBUG: log("Setting up Caenels...", data_file = False)
        ## Setup caenels
        caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:TriggerMode", 1)
        caput("PINK:CAE2:AcquireMode", 0)
        caputq("PINK:CAE2:Acquire", 1)

        caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:TriggerMode", 1)
        caput("PINK:CAE1:AcquireMode", 0)
        caputq("PINK:CAE1:Acquire", 1)

        if DEBUG: log("Setup plot...", data_file = False)
        ## Setup plot
        plottitle = "IZero"
        plottitle2 = detlabel
        plottitle3 = "Normalized"
        [p1, p2, p3]=plot([None, None, None], [plottitle, plottitle2, plottitle3], title="DCM Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p2.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p3.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
        p1.getAxis(p1.AxisId.X).setLabel("Energy")
        p2.getAxis(p1.AxisId.X).setLabel("Energy")
        p3.getAxis(p1.AxisId.X).setLabel("Energy")
        set_setting("last_plot_title", "DCM Scan") 

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(1, [0, exposure-0.02], [0, exposure+0.15], [0, 0], [0, 0.001])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        if detector=="eiger":
            caput("PINK:RPISW:select_A", 3)
        else:
            caput("PINK:RPISW:select_A", 1)
        caput("PINK:RPISW:select_B", 2)    

        if detector=="eiger":        
            ## setup eiger
            if DEBUG: log("Stop eiger", data_file = False)
            caput("PINK:EIGER:cam1:Acquire", 0)
            sleep(1)
            caput("PINK:EIGER:cam1:AcquireTime", exposure)
            sleep(1)
            caput("PINK:EIGER:cam1:AcquirePeriod", exposure+0.002)
            caput("PINK:EIGER:cam1:NumImages", 1)
            caput("PINK:EIGER:cam1:NumTriggers", len(positionarray))
            # manual trigger enable
            caput("PINK:EIGER:cam1:ManualTrigger", 1)
            sleep(0.5)
            ## arm detector
            caputq("PINK:EIGER:cam1:Acquire", 1)
        else:
            ACQ.write(0)
            sleep(2)
            ## setup greateyes
            caput("PINK:GEYES:cam1:AcquireTime", exposure)
            caput("PINK:GEYES:cam1:ImageMode", 0) # single image              

        if DEBUG: log("Moving to first pos", data_file = False)
        ## Moving to first position
        print("Moving DCM to start position...")
        pos = positionarray[0]
        ENERGY.write(pos)
        sleep(1)
        self.__waitdcm(ENERGY_DMOV, point_delay)

        if DEBUG: log("Scanning...", data_file = False)        
        print("Scanning...")
        
        ## Scan loop
        try:
            ## Main loop
            for pos in positionarray:
                ## move and wait for dcm
                if DEBUG: log("moving", data_file = False)
                ENERGY.write(pos)
                sleep(0.5)
                self.__waitdcm(ENERGY_DMOV, point_delay)

                ##trigger acq
                if DEBUG: log("trigger", data_file = False)
                ACQ.write(1)
                resp = SENSOR.waitCacheChange(int(1000*(exposure+2)))
                sleep(0.2)

                ## append to vecto
                if DEBUG: log("appending to vectors", data_file = False)
                energy.append(ENERGY_RBV.read())
                #energy.append(pos)
                sensor.append(SENSOR.read())
                izero.append(IZERO.read())
                absvec.append(SENSOR.take()/IZERO.take())

                ## update plot
                if DEBUG: log("updating plots", data_file = False)
                p1.getSeries(0).setData(energy, izero)
                p2.getSeries(0).setData(energy, sensor)
                p3.getSeries(0).setData(energy, absvec)

                ## append data
                if DEBUG: log("appending data", data_file = False)
                append_dataset("data/energy", ENERGY_RBV.take())
                append_dataset("data/detector", SENSOR.take())
                append_dataset("data/izero", IZERO.take())
                append_dataset("data/izero_profile", IZero_profile.take())
                append_dataset("data/izero_posx", IZeroposx.take())
                append_dataset("data/izero_posy", IZeroposy.take())
                append_dataset("data/gap", GAP.read())
                append_dataset("bpm3/diode", DBPM3.take())
                append_dataset("bpm3/diode_profile", DBPM3_profile.read())
                append_dataset("data/emission", SENSOR.take()/IZERO.take())
                append_dataset("bpm2/X", BPM2xfit.value)
                append_dataset("bpm2/Y", BPM2yfit.value)
                append_dataset("bpm2/X_profile", BPM2Hor.read())
                append_dataset("bpm2/Y_profile", BPM2Ver.read())
                append_dataset("bpm3/X", BPM3xfit.value)
                append_dataset("bpm3/Y", BPM3yfit.value)                     
                append_dataset("detector/processed/image", Convert.reshape(DET_roi_array.take(), DET_ROI_Y, DET_ROI_X))
                if detector=="ge":
                    append_dataset("detector/raw/image", Convert.reshape(DET_raw_array.take(), DET_Y, DET_X))
                append_dataset("detector/processed/spectrum", DET_Spectra.take())
                append_dataset("detector/raw/frame_id", DET_frameID.take())

        except Exception, value:
            print("DCM scan aborted.")
            print(value)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        ## save data
        save_dataset("scan/end_time", time.ctime())
        save_dataset("plot/title", plottitle)
        save_dataset("plot/title2", plottitle2)
        save_dataset("plot/title3", plottitle3)
        save_dataset("plot/x", positionarray)
        create_dataset("plot/y", 'd', False, (0, len(izero)))
        append_dataset("plot/y", izero)
        create_dataset("plot/y2", 'd', False, (0, len(sensor)))
        append_dataset("plot/y2", sensor)
        create_dataset("plot/y3", 'd', False, (0, len(absvec)))
        append_dataset("plot/y3", absvec)
        save_dataset("plot/y_desc", "")
        save_dataset("plot/xlabel", "Energy")
        save_dataset("plot/ylabel", "")

        if detector=="eiger":
            ## setup eiger
            if DEBUG: log("Stop eiger", data_file = False)
            caput("PINK:EIGER:cam1:Acquire", 0)
            sleep(1)
            # manual trigger enable
            caput("PINK:EIGER:cam1:ManualTrigger", 0)
            sleep(0.5)

        ## move diode to previous state
        if diode3Out:
            caput("PINK:PLCGAS:vdiode_FA_close", 1)

        #send plot to elab
        if caget("PINK:SESSION:sessionstate", type='i'):
            elab.send_last_plot()  

        print("Scan finished")

    ## extra functions
    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname  

    def __waitdcm(self, dmov, delay):
        ## sim
        #while(dmov.read()<1):
        while(dmov.read()):
            sleep(0.5)
        sleep(delay)
        
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])            
            
    def bpmanalysis(self, vec, pos0, pos1, N, myCF):
        vecb = vec[0:N]
        xvecabs = range(pos0,pos1)
        xvec = range(0,N)
        weights = [ 1.0] * len(xvec)
        try:
            (a, b, amp, com, sigma) = fit_gaussian_linear(vecb, xvec, None, weights)
            abspos=com+pos0
        except:
            log("Error on bpm fitting", data_file = False)
            abspos=0
        return abspos

    def beampos_bpm2_v(self, PV):
        bpm2_vert_CF=13
        bpm2_hor_CF=13*0.707
        sig2fwmh=2.355
        roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
        roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
        roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
        roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
        N = int(roisizey)
        pos0 = roiposy
        pos1 = roiposy+roisizey
        myCF = bpm2_vert_CF
        #vec = caget("PINK:PG04:Stats2:ProfileAverageY_RBV")
        vec = PV.read()
        beampos = self.bpmanalysis(vec, pos0, pos1, N, myCF)
        return beampos

    def beampos_bpm2_h(self, PV):
        bpm2_vert_CF=13
        bpm2_hor_CF=13*0.707
        sig2fwmh=2.355
        roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
        roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
        roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
        roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
        N = int(roisizex)
        pos0 = roiposx
        pos1 = roiposx+roisizex
        myCF = bpm2_hor_CF
        #vec = caget("PINK:PG04:Stats2:ProfileAverageX_RBV")
        vec = PV.read()
        beampos = self.bpmanalysis(vec, pos0, pos1, N, myCF)
        return beampos            

    def getdetlabel(self, det):
        if det=='bpm3':
            l='direct_diode_BPM3'
        elif det=='eiger':
            l='eiger'
        elif det=='ge':
            l='greateyes'
        elif det=='mythen':
            l='mythen'
        elif det=='2color':
            l='twocolor'
        else:
            l=' '
        return l