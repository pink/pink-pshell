class M2():
    def TX(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseX", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseX")))

    def TY(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseY", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseY")))

    def TZ(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseZ", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseZ")))           

    def RX(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseA", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseA")))

    def RY(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseB", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseB")))   

    def RZ(*args):
        if len(args)>1:
            v = float(args[1])
            if caget("HEX2OS12L:hexapod:mbboRunAfterValue", type='i') != 0:
                caput("HEX2OS12L:hexapod:mbboRunAfterValue", 0)
                sleep(0.5)
            caput("HEX2OS12L:hexapod:setPoseC", v)
        else:
            print("TX: {:.2f}".format(caget("HEX2OS12L:hexapod:getReadPoseC")))   