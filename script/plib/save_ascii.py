class SASCII():
    def save_ascii_file(self, fname, data):
        try:
            ## simply return if no data
            if len(data)==0:
                return

            ## setup file path and name
            datafilepath = get_exec_pars().getPath()
            fpath = datafilepath.split("/")
            fpath = datafilepath.split(fpath[-1])
            fpath = fpath[0]+"ascii"
            if os.path.isdir(fpath) == False:
                os.mkdir(fpath)
            asciifname = datafilepath.split("/")[-1].split(".h5")[0]+fname+".csv"
            asciifname = fpath+"/"+asciifname
            
            ## get data structure
            if type(data[0])==type(0.0):
                ncol=1
                nrow=len(data)
                atype = 0
            else:
                ncol=len(data)
                nrow=len(data[0])
                atype = 1

            ## create channel array
            channel = [i for i in range(nrow)]

            ## create lines to be saved
            lines = []
            for i in range(nrow):
                line="{}".format(channel[i])
                if atype==0:
                    line += " {}".format(data[i])
                else:
                    for j in range(ncol):
                        line += " {}".format(data[j][i])
                lines.append(line+'\n')

            ## save data to file
            f = open(asciifname, 'a+')
            for l in lines:
                f.write(l)
            f.close()      

            ## debug
            #print("col: {}, row: {}".format(ncol,nrow))
            #for l in lines:
            #    print(l)

        except:
            print("[Error]: Failed to create ascii file")