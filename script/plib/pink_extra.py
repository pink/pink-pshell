from org.python.core.util import StringUtil
import Queue
import threading
import time

## PINK Extra functions
def pink_save_bl_snapshot():
    print("Saving beamline snapshot...")
    run("config/bl_snapshot_config.py")
    for spdev in snapshot_pvlist:
        try:
            pval = caget(spdev[1])
        except:
            if (spdev[1] != "PINK:PG02:Over1:5:CenterY_RBV") and (spdev[1] != "PINK:PG02:Over1:5:CenterX_RBV"):
                msg = "[snapshot]: caget failed for PV: " + spdev[1]
                print(msg)
                log(msg, data_file = True)
            pval = "----"
        save_dataset(spdev[0], pval)

def linspace(start, end, step):
    if start==end:
        return [start]
    if type(step)==type(int(0)):
        if step<2:
            return[start]
        step = abs(step)
        N = int(abs(step))
        ds = float((end-start))/(step-1)
    else:
        if step==0.0:
            return [start]
        N = abs(int(math.floor(abs((end-start)/step))))+1
        ds = abs(step)
        if end-start < 0:
            ds = -1*ds
    resp = [start+(i*ds) for i in range(N)]
    return resp

## Sample motion functions
def amvx(pos):
    if get_setting("chamber")=="cryo":
        mvpvdic={
            "axis":"X",
            "position":"PINK:PHY:AxisJ.VAL",
            "rbv":"PINK:PHY:AxisJ.RBV",
            "dmov":"PINK:PHY:AxisJ.DMOV"
        }
    else:
        mvpvdic={
            "axis":"X",
            "position":"PINK:SMA01:m10.VAL",
            "rbv":"PINK:SMA01:m10.RBV",
            "dmov":"PINK:SMA01:m10.DMOV"
        }
    __movemotor_a(mvpvdic, pos)

def amvy(pos):
    if get_setting("chamber")=="cryo":
        mvpvdic={
            "axis":"Y",
            "position":"PINK:ANC01:ACT0:CMD:TARGET",
            "rbv":"PINK:ANC01:ACT0:POSITION",
            "dmov":"PINK:ANC01:ACT0:IN_TARGET"
        }
    else:
        mvpvdic={
            "axis":"Y",
            "position":"PINK:SMA01:m9.VAL",
            "rbv":"PINK:SMA01:m9.RBV",
            "dmov":"PINK:SMA01:m9.DMOV"
        }
    __movemotor_a(mvpvdic, pos)

def rmvx(pos):
    if get_setting("chamber")=="cryo":
        mvpvdic={
            "axis":"X",
            "position":"PINK:PHY:AxisJ.VAL",
            "rbv":"PINK:PHY:AxisJ.RBV",
            "dmov":"PINK:PHY:AxisJ.DMOV"
        }
    else:
        mvpvdic={
            "axis":"X",
            "position":"PINK:SMA01:m10.VAL",
            "rbv":"PINK:SMA01:m10.RBV",
            "dmov":"PINK:SMA01:m10.DMOV"
        }
    __movemotor_r(mvpvdic, pos)

def rmvy(pos):
    if get_setting("chamber")=="cryo":
        mvpvdic={
            "axis":"Y",
            "position":"PINK:ANC01:ACT0:CMD:TARGET",
            "rbv":"PINK:ANC01:ACT0:POSITION",
            "dmov":"PINK:ANC01:ACT0:IN_TARGET"
        }
    else:
        mvpvdic={
            "axis":"X",
            "position":"PINK:SMA01:m9.VAL",
            "rbv":"PINK:SMA01:m9.RBV",
            "dmov":"PINK:SMA01:m9.DMOV"
        }
    __movemotor_r(mvpvdic, pos)    
        
def wx():
    if get_setting("chamber")=="cryo":
        print("X: {:.1f} um".format(caget("PINK:PHY:AxisJ.RBV", type='d')))
    else:
        print("X: {:.1f} um".format(caget("PINK:SMA01:m10.RBV", type='d')))

def wy():
    if get_setting("chamber")=="cryo":
        print("X: {:.1f} um".format(caget("PINK:ANC01:ACT0:POSITION", type='d')))
    else:
        print("X: {:.1f} um".format(caget("PINK:SMA01:m9.RBV", type='d')))        
       
def __movemotor_a(mvpvdic, pos):
    print("---------------")
    caputq(mvpvdic["position"],float(pos))
    sleep(0.5)
    while(caget(mvpvdic["dmov"], type='i')==0):
        print("{}: {:.1f} um".format(mvpvdic["axis"], caget(mvpvdic["rbv"], type='d')))
        sleep(2)
    print("{}: {:.1f} um".format(mvpvdic["axis"], caget(mvpvdic["rbv"], type='d')))
    print("---------------")

def __movemotor_r(mvpvdic, pos):
    print("---------------")
    pos = caget(mvpvdic["position"], type='d')+float(pos)
    caputq(mvpvdic["position"],float(pos))
    sleep(0.5)
    while(caget(mvpvdic["dmov"], type='i')==0):
        print("{}: {:.1f} um".format(mvpvdic["axis"], caget(mvpvdic["rbv"], type='d')))
        sleep(1)
    print("{}: {:.1f} um".format(mvpvdic["axis"], caget(mvpvdic["rbv"], type='d')))
    print("---------------")    


def timeformater(tseconds):
    scantime = round(tseconds)
    sh = int(scantime/3600.0)
    sm = int(scantime/60)%60
    ss = int(scantime%60)
    msg = "{:02d}h {:02d}m {:02d}s".format(sh,sm,ss)
    return msg


# ******** Pseudo Devices ************

class Array2Matrix(ReadonlyRegisterBase, ReadonlyRegisterMatrix):
    def __init__(self, name, src_array, src_width, src_height):
        ReadonlyRegisterBase.__init__(self, name)
        self.src_array = src_array
        self.src_width = src_width
        self.src_height = src_height

    def doRead(self):
        data = self.src_array.take()
        h = self.getHeight()
        w = self.getWidth()
        ret = Convert.reshape(data, h, w)
        return ret

    def getWidth(self):
        return int(self.src_width.take())

    def getHeight(self):
        return int(self.src_height.take())

##add_device(Array2Matrix("GE_BG_Image", GE_BG_Array, GE_BG_SizeX, GE_BG_SizeY), True)
##sleep(1)
##GE_BG_Image.read()


## Extra classes
class ELAB():
    from org.python.core.util import StringUtil
    #import Queue
    
    def __init__(self):
        def post_thread(self):
            #print("ELAB post thread has started.")
            print("..")
            while(True):
                self.__post()
                time.sleep(5)
            
        self.debug = False
        self.qbuff = Queue.Queue(100)
        self.ready = False
        self.busy = False
        try:
            self.postpv = create_channel_device("PINK:SESSION:elab_queue", type='[b',size=2048)
            self.ready=True
        except:
            self.ready=False
        ## thread
        self.pt = threading.Thread(target=post_thread, args=(self,))
        self.pt.start()
        
    def put(self, msg):
        if self.qbuff.full()==False:
            self.qbuff.put_nowait(msg)

    def activate(self):
        caput("PINK:SESSION:sessionstate", 1)

    def deactivate(self):
        caput("PINK:SESSION:sessionstate", 0)        

    def __post(self):
        if (self.ready):
            while self.qbuff.qsize():
                msg = self.qbuff.get()
                #print("[->] " + msg)
                try:
                    self.postpv.write(StringUtil.toBytes(msg))
                except:
                    pass
                sleep(0.250)
        else:
            while self.qbuff.qsize():
                discard = self.qbuff.get()

    def __get_snapshots(self, title = None, file_type = "png", size = None, temp_path = get_context().setup.getContextPath()):
        time.sleep(0.1) #Give some time to plot to be finished - it is not sync  with acquisition
        ret = []
        if size != None:
            size = Dimension(size[0], size[1])
        plots = get_plots(title)
        for i in range(len(plots)):
            p = plots[i]
            name = "img_{:02d}".format(int(i))
            file_name = os.path.abspath(temp_path + "/" + name + "." + file_type)
            p.saveSnapshot(file_name , file_type, size)
            ret.append(file_name)
        return ret


    def send_last_plot(self):
        image_width=1000
        image_height=500
        ptitle=get_setting("last_plot_title")
        if len(get_plots(ptitle))>0:
            log("[ELAB] Sending plot: " + ptitle)
            #imgnames=get_plot_snapshots(ptitle, file_type = "png", size = [image_width,image_height], temp_path="/home/epics/Pinkdata/Control/images")
            imgnames=self.__get_snapshots(ptitle, file_type = "png", size = [image_width,image_height], temp_path="/home/epics/Pinkdata/Control/images")
            if len(imgnames)>0:
                fnames=[]
                for imgn in imgnames:
                    fname=imgn.split("/")[-1]
                    fname = fname.replace(" ","%20")
                    fnames.append(fname)
                msgrec=",".join(fnames)
                log("[ELAB] Image names: {}".format(str(msgrec)))
                caput("PINK:SESSION:elab_images", StringUtil.toBytes(msgrec))
        else:
            raise Exception("Plot {} not available".format(str(ptitle)))
       