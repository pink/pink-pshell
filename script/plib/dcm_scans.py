#dcm_scans.py

class DCMSCAN():
    def __init__(self):
        pass

    ########################################
    ### scan
    ########################################

    def scan(self, source, start=0, end=0, step=0, exposure=0.0, idon="on", point_delay=0, fit=False):
        ## reset EPICS connections
        Epics.destroy()
        Epics.create()

        idon=str(idon).lower()
        if (idon=="on" or idon=="1"):
            idon=True
            idonlabel = 'on'
        else:
            idon=False
            idonlabel = 'off'

        if exposure==0:
            print("abort: exposure = 0")
            return

        verbose=False
        DEBUG=False

        if verbose: print("Creating channels")
        ## channels
        IZERO = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
        IZERO.setMonitored(True)
        BPM3 = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
        BPM3.setMonitored(True)
        if source=="izero":
            SENSOR=IZERO
            SDPath = None
        elif source=="bpm3":
            SENSOR=BPM3
            SDPath = None
        elif source=="diag":
            SENSOR = create_channel_device("PINK:CAE1:Current1:MeanValue_RBV")
            SENSOR.setMonitored(True)
            SDPath = "data/diagdiode"
        elif source=="diodedcm":
            SENSOR = create_channel_device("EMILEL:Keithley09:rdCur")
            SENSOR.setMonitored(True)
            SDPath = "data/dcmdiode"
        else:
            print("DCM scan is not available for this source.")
            return
        
        ACQ=create_channel_device("PINK:DG01:TriggerDelayBO", type='i')
        MOTOR = create_channel_device("u171dcm1:monoSetEnergy")
        MOTOR_RBV = create_channel_device("u171dcm1:monoGetEnergy")
        MOTOR_RBV.setMonitored(True)
        MOTOR_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
        MOTOR_DMOV.setMonitored(True)

        ## extra channels
        BPM2xfit = create_channel_device("PINK:BPM:2:X:center")
        BPM2xfit.setMonitored(True)
        BPM2yfit = create_channel_device("PINK:BPM:2:Y:center")
        BPM2yfit.setMonitored(True)
        BPM3xfit = create_channel_device("PINK:BPM:3:X:center")
        BPM3xfit.setMonitored(True)
        BPM3yfit = create_channel_device("PINK:BPM:3:Y:center")
        BPM3yfit.setMonitored(True)
        IZerod1 = create_channel_device("PINK:CAE2:Current1:MeanValue_RBV")
        IZerod1.setMonitored(True)
        IZerod2 = create_channel_device("PINK:CAE2:Current2:MeanValue_RBV")
        IZerod2.setMonitored(True)
        IZerod3 = create_channel_device("PINK:CAE2:Current3:MeanValue_RBV")
        IZerod3.setMonitored(True)
        IZerod4 = create_channel_device("PINK:CAE2:Current4:MeanValue_RBV")
        IZerod4.setMonitored(True)
        IZeroposx = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV")
        IZeroposx.setMonitored(True)
        IZeroposy = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV")
        IZeroposy.setMonitored(True)
        gappv = create_channel_device("U17IT6R:Ndi1PmsPos")
        gappv.setMonitored(True)

        ## delay for epics connections
        sleep(2)

        ## variables
        sensor = []
        motor = []

        ## Setup
        set_exec_pars(open=False, name="dcm", reset=True)
        save_dataset("scan/scantype", "DCM scan")

        ## configure scan positions
        positionarray = linspace(start, end, step)

        ## print some info
        print("******************************************************")
        print("  Filename: " + self.get_filename())
        print(" Scan type: dcm scan")
        print("  Detector: {}".format(source))
        print("    Energy: [{:.0f},{:.0f}]".format(start, end))
        print("    Points: {}".format(len(positionarray)))
        print("  Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        print("******************************************************")

        fcall="scan.dcm(source.{}(), start={}, end={}, step={}, exposure={}, IdOn={}, point_delay={})".format(source,start,end,step,exposure,idonlabel, point_delay)
        elab.put(fcall)
        elab.put("******************************************************")
        elab.put("Filename: " + self.get_filename())
        elab.put("Scan type: dcm scan")
        elab.put("Detector: {}".format(source))
        elab.put("Energy: [{:.0f},{:.0f}]".format(start, end))
        elab.put("Points: {}".format(len(positionarray)))
        elab.put("Exposure: " + '{:.1f}'.format(float(exposure)) + " seconds")
        elab.put("******************************************************")

        ## setup DCM
        # Set IdOn {0:off, 1:on}
        if idon:
            caput("u171dcm1:SetIdOn", 1)
            #dcm.ID_on()
        else:
            caput("u171dcm1:SetIdOn", 0)
            #dcm.ID_off()

        if verbose: print("Setup ampmeter")
        ## Configure ampmeter
        exposure=float(exposure)
        sleep(1)
        caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:TriggerMode", 1)
        caput("PINK:CAE2:AcquireMode", 0)

        caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:TriggerMode", 1)
        caput("PINK:CAE1:AcquireMode", 0)

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, 0], [0, 0], [0, 0], [0, 1e-3])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 0)
        caput("PINK:RPISW:select_B", 1)

        ## create datasets
        create_dataset("data/izero", 'd', False)
        create_dataset("data/bpm3", 'd', False)
        create_dataset("data/bpm2_xpos", 'd', False)
        create_dataset("data/bpm2_ypos", 'd', False)
        create_dataset("data/bpm3_xpos", 'd', False)
        create_dataset("data/bpm3_ypos", 'd', False)
        create_dataset("data/izero_xpos", 'd', False)
        create_dataset("data/izero_ypos", 'd', False)
        create_dataset("data/izero_d1", 'd', False)
        create_dataset("data/izero_d2", 'd', False)
        create_dataset("data/izero_d3", 'd', False)
        create_dataset("data/izero_d4", 'd', False)
        create_dataset("data/gap", 'd', False)

        ## save datasets
        save_dataset("dcm/IdSlope", caget("u171dcm1:aiIdSlope"))
        save_dataset("dcm/IdOffset", caget("u171dcm1:aiIdOffset"))

        ## plot setup
        if verbose: print("Setup plot")
        [p1] = plot(None, "Scan", title="DCM Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p1.addSeries(LinePlotSeries("Fit"))
        set_setting("last_plot_title", "DCM Scan")

        print("Moving DCM to start position")
        initpos = MOTOR_RBV.take()
        pos = positionarray[0]
        MOTOR.write(pos)
        sleep(0.2)
        self.__waitdcm(point_delay, MOTOR_DMOV)

        print("Scanning...")

        set_status("Scanning...")
        try:
            ## Main loop
            for pos in positionarray:
                set_status("Energy: {} eV".format(pos))

                ## move and wait for dcm
                MOTOR.write(pos)
                sleep(0.2)
                self.__waitdcm(point_delay, MOTOR_DMOV)

                ACQ.write(1)
                IZERO.waitCacheChange(int(1000*(exposure+2)))
                sleep(0.1)
                sensor.append(SENSOR.read())
                motor.append(MOTOR_RBV.take())
                p1.getSeries(0).setData(motor, sensor)

                ## save data
                append_dataset("data/izero", IZERO.take())
                append_dataset("data/bpm3", BPM3.take())
                append_dataset("data/bpm2_xpos", BPM2xfit.take())
                append_dataset("data/bpm2_ypos", BPM2yfit.take())
                append_dataset("data/bpm3_xpos", BPM3xfit.take())
                append_dataset("data/bpm3_ypos", BPM3yfit.take())
                append_dataset("data/izero_xpos", IZeroposx.take())
                append_dataset("data/izero_ypos", IZeroposy.take())
                append_dataset("data/izero_d1", IZerod1.take())
                append_dataset("data/izero_d2", IZerod2.take())
                append_dataset("data/izero_d3", IZerod3.take())
                append_dataset("data/izero_d4", IZerod4.take())
                append_dataset("data/gap", gappv.take())

        except Exception, errvalue:
            log(errvalue)
            print(errvalue)
            print("DCM scan aborted.")

        set_status("End of scan...")

        ## save data
        if SDPath!=None:
            save_dataset(SDPath, sensor)    
        save_dataset("data/energy", motor)
        save_dataset("plot/x", motor)
        create_dataset("plot/y", 'd', False, (0, len(sensor)))
        append_dataset("plot/y", sensor)
        save_dataset("plot/title", "DCM Scan")
        save_dataset("plot/xlabel", "DCM")
        save_dataset("plot/ylabel", "Current")
        save_dataset("plot/y_desc", "scan 0")

        #send plot to elab
        if caget("PINK:SESSION:sessionstate", type='i'):
            elab.send_last_plot()  

        pink_save_bl_snapshot()

        print("Scan finished. OK")

    ###########################################
    ### Support functions
    ###########################################

    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname    

    def __gauss_fit(self, ydata, xdata):
        (a, b, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i)+(a*i)+b for i in xgauss]
        fwhm = 2.355*sigma
        return (a, b, amp, com, sigma, gauss, fwhm, xgauss)

    def __gauss_exp_fit(self, ydata, xdata):
        (incl, off, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        start_point=[1, 1, amp, com, sigma]
        (eamp, decay, amp, com, sigma) = fit_gaussian_exp_bkg(ydata, xdata, start_point=start_point)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i) + eamp*math.exp(-i/decay) for i in xgauss]
        fwhm = 2.355*sigma
        return (eamp, decay, amp, com, sigma, gauss, fwhm, xgauss)

    def __waitdcm(self, point_delay, statuspv):
        #set_status("Waiting for dcm...")
        #log("Waiting for dcm...")
        while(statuspv.value):
            sleep(0.1)
        sleep(point_delay)

    ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
    ## trigger [5:single shot] [1: Ext rising edge]
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])
