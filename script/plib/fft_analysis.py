import numpy as np
#import scipy.fftpack
    

def fft_noise(arr_y): 
    # Input: experimental spectrum. X- pixels, Y - intensity
    # This function doing fft transformation and searching for a maximum
    # of intencity between x_fft = 17-22. After that it calculates an averaged background
    # between points = 6-10 and 25-51. 
    # The output parameters: max, bkg and max/bkg
    # later on max/bkg ratio can be used for decrimination
    print("fft analysis started...")
    N=len(arr_y)
    yf=np.fft.fft(arr_y) # Fourier transformation (complex numbers)
    #yf=scipy.fftpack.fft(arr_y) # Fourier transformation (complex numbers)
    #----------------------------
    y = 2.0/N*np.abs(yf)  # Convert to real space N*np.abs(yf[:N // 2])

    #maximum intensity of the noise peak
    fft_max = np.max(y[17:22])

    #calculate background
    fft_bkg = (np.sum(y[6:11])+np.sum(y[25:52]))/32
    max2bkg = fft_max / fft_bkg # Avoid division by zero if fft_bkg != 0 else np.inf

    return fft_max, fft_bkg, max2bkg
    #----------------------------