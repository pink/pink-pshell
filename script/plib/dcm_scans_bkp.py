#dcm_scans.py

class DCMSCAN():
    def __init__(self):
        pass

    ########################################
    ### scan
    ########################################

    def scan(self, source, start=0, end=0, step=0, exposure=0.0, idon="on", point_delay=0, fit=False):
        
        print("DCM scan debug")
        

        Epics.destroy()
        Epics.create()

        idon=str(idon).lower()
        if (idon=="on" or idon=="1"):
            idon=True
        else:
            idon=False

        if exposure==0:
            print("abort: exposure = 0")
            return

        verbose=True
        DEBUG=False

        if verbose: print("Creating channels")
        ## channels
        IZERO = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
        IZERO.setMonitored(True)
        BPM3 = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
        BPM3.setMonitored(True)
        if source=="izero":
            #SENSOR = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV")
            #SENSOR.setMonitored(True)
            SENSOR=IZERO
            #ACQ = create_channel_device("PINK:CAE2:Acquire", type='i')
            #ACQ.setMonitored(True)
        elif source=="bpm3":
            #SENSOR = create_channel_device("PINK:CAE1:Current3:MeanValue_RBV")
            #SENSOR.setMonitored(True)
            SENSOR=BPM3
            #ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
            #ACQ.setMonitored(True)
        elif source=="diag":
            SENSOR = create_channel_device("PINK:CAE1:Current1:MeanValue_RBV")
            SENSOR.setMonitored(True)
            #ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
            #ACQ.setMonitored(True)
        elif source=="diodedcm":
            SENSOR = create_channel_device("EMILEL:Keithley09:rdCur")
            SENSOR.setMonitored(True)
            #ACQ = create_channel_device("PINK:CAE1:Acquire", type='i')
            #ACQ.setMonitored(True)
        else:
            print("DCM scan is not available for this source.")
            return
        
        ACQ=create_channel_device("PINK:DG01:TriggerDelayBO", type='i')
        #print(ACQ.read())
        #return
        #MOTOR = create_channel_device("U17IT6R:BaseParGapsel.B")
        #MOTOR_SET = create_channel_device("U17IT6R:BaseCmdCalc.PROC")
        #MOTOR_RBV = create_channel_device("U17IT6R:BasePmGap.A")
        #MOTOR_RBV.setMonitored(True)
        #motor_deadband = 0.02

        MOTOR = create_channel_device("u171dcm1:monoSetEnergy")
        MOTOR_RBV = create_channel_device("u171dcm1:monoGetEnergy")
        MOTOR_RBV.setMonitored(True)
        MOTOR_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
        MOTOR_DMOV.setMonitored(True)

        ## extra channels
        BPM2xfit = create_channel_device("PINK:BPM:2:X:center")
        BPM2xfit.setMonitored(True)
        BPM2yfit = create_channel_device("PINK:BPM:2:Y:center")
        BPM2yfit.setMonitored(True)
        BPM3xfit = create_channel_device("PINK:BPM:3:X:center")
        BPM3xfit.setMonitored(True)
        BPM3yfit = create_channel_device("PINK:BPM:3:Y:center")
        BPM3yfit.setMonitored(True)
        IZerod1 = create_channel_device("PINK:CAE2:Current1:MeanValue_RBV")
        IZerod1.setMonitored(True)
        IZerod2 = create_channel_device("PINK:CAE2:Current2:MeanValue_RBV")
        IZerod2.setMonitored(True)
        IZerod3 = create_channel_device("PINK:CAE2:Current3:MeanValue_RBV")
        IZerod3.setMonitored(True)
        IZerod4 = create_channel_device("PINK:CAE2:Current4:MeanValue_RBV")
        IZerod4.setMonitored(True)
        IZeroposx = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV")
        IZeroposx.setMonitored(True)
        IZeroposy = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV")
        IZeroposy.setMonitored(True)
        gappv = create_channel_device("U17IT6R:Ndi1PmsPos")
        gappv.setMonitored(True)

        ## variables
        sensor = []
        motor = []

        ## Setup
        #print("Scanning ...")
        set_exec_pars(open=False, name="dcm", reset=True)
        save_dataset("scan/scantype", "DCM scan")

        ## setup DCM
        # Set IdOn {0:off, 1:on}
        if idon:
            caput("u171dcm1:SetIdOn", 1)
            #dcm.ID_on()
        else:
            caput("u171dcm1:SetIdOn", 0)
            #dcm.ID_off()

        if verbose: print("Setup ampmeter")
        ## Configure ampmeter
        exposure=float(exposure)
        #ACQ.write(0)
        sleep(1)
        caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE2:AveragingTime", exposure)
        caput("PINK:CAE2:TriggerMode", 1)
        caput("PINK:CAE2:AcquireMode", 0)

        caput("PINK:CAE1:ValuesPerRead", int(1000*exposure))
        caput("PINK:CAE1:AveragingTime", exposure)
        caput("PINK:CAE1:TriggerMode", 1)
        caput("PINK:CAE1:AcquireMode", 0)
        #caput("PINK:CAE1:Range", 0)

        ## Setup delay generator
        ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
        ## [trigger mode] [5:single shot] [1: Ext rising edge]
        self.setup_delaygen(5, [0, 0], [0, 0], [0, 0], [0, 1e-3])

        ## Setup trigger switch
        ## A=Delaygen Trigger Source [0:OFF, 1:CCD, 2:mythen, 3:eiger]
        ## B=Caenels Trigger Source [0:OFF, 1:Delaygen, 2:Output A]
        caput("PINK:RPISW:select_A", 0)
        caput("PINK:RPISW:select_B", 1)

        sleep(1)

        ## create datasets
        create_dataset("data/izero", 'd', False)
        create_dataset("data/bpm3", 'd', False)
        create_dataset("data/bpm2_xpos", 'd', False)
        create_dataset("data/bpm2_ypos", 'd', False)
        create_dataset("data/bpm3_xpos", 'd', False)
        create_dataset("data/bpm3_ypos", 'd', False)
        create_dataset("data/izero_xpos", 'd', False)
        create_dataset("data/izero_ypos", 'd', False)
        create_dataset("data/izero_d1", 'd', False)
        create_dataset("data/izero_d2", 'd', False)
        create_dataset("data/izero_d3", 'd', False)
        create_dataset("data/izero_d4", 'd', False)
        create_dataset("data/gap", 'd', False)

        ## save datasets
        save_dataset("dcm/IdSlope", caget("u171dcm1:aiIdSlope"))
        save_dataset("dcm/IdOffset", caget("u171dcm1:aiIdOffset"))

        ## configure scan positions
        positionarray = linspace(start, end, step)

        ## plot setup
        if verbose: print("Setup plot")
        [p1] = plot(None, "Scan", title="DCM Scan")
        p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
        p1.addSeries(LinePlotSeries("Fit"))

        if verbose: print("Moving DCM to start position")
        initpos = MOTOR_RBV.take()
        pos = positionarray[0]
        MOTOR.write(pos)
        sleep(0.2)
        self.__waitdcm(point_delay)

        if verbose: print("Scanning...")

        print("Filename: " + self.get_filename())
        set_status("Scanning...")
        try:
            ## Main loop
            for pos in positionarray:
                set_status("Energy: {} eV".format(pos))

                ## move and wait for dcm
                MOTOR.write(pos)
                sleep(0.2)
                self.__waitdcm(point_delay)

                ACQ.write(1)
                IZERO.waitCacheChange(int(1000*(exposure+2)))
                sleep(0.1)
                sensor.append(SENSOR.read())
                motor.append(MOTOR_RBV.take())
                p1.getSeries(0).setData(motor, sensor)

                ## save data
                append_dataset("data/izero", IZERO.take())
                append_dataset("data/bpm3", BPM3.take())
                append_dataset("data/bpm2_xpos", BPM2xfit.take())
                append_dataset("data/bpm2_ypos", BPM2yfit.take())
                append_dataset("data/bpm3_xpos", BPM3xfit.take())
                append_dataset("data/bpm3_ypos", BPM3yfit.take())
                append_dataset("data/izero_xpos", IZeroposx.take())
                append_dataset("data/izero_ypos", IZeroposy.take())
                append_dataset("data/izero_d1", IZerod1.take())
                append_dataset("data/izero_d2", IZerod2.take())
                append_dataset("data/izero_d3", IZerod3.take())
                append_dataset("data/izero_d4", IZerod4.take())
                append_dataset("data/gap", gappv.take())

        except Exception, errvalue:
            log(errvalue)
            print(errvalue)
            print("DCM scan aborted.")

        set_status("End of scan...")

        save_dataset("data/energy", motor)
        save_dataset("plot/x", motor)
        create_dataset("plot/y", 'd', False, (0, len(sensor)))
        append_dataset("plot/y", sensor)
        save_dataset("plot/title", "DCM Scan")
        save_dataset("plot/xlabel", "DCM")
        save_dataset("plot/ylabel", "Current")
        save_dataset("plot/y_desc", "scan 0")
        #save_dataset("scan/scantype", "DCM scan without fitting")
        save_dataset("raw/izero", sensor)
        save_dataset("raw/dcm", motor)        

        # if fit == True:
        #     fittype = "exp"
        # else:
        #     fittype = None

        # ## Fitting options
        # if fittype=="linear":
        #     if verbose: print("analysing")
        #     (a, b, amp, com, sigma, gauss, fwhm, xgauss)=self.__gauss_fit(sensor, motor)
        #     p1.getSeries(1).setData(xgauss, gauss)
        #     p1.setLegendVisible(True)
        #     print(" ")
        #     print("Gaussian fit -- linear background")
        #     print("-----------------------------------------")
        #     print("inclination: " + '{:.3e}'.format(a))
        #     print("     offset: " + '{:.3e}'.format(b))
        #     print("  Amplitude: " + '{:.3e}'.format(amp))
        #     print("       Mean: " + '{:.3f}'.format(com))
        #     print("      Sigma: " + '{:.3f}'.format(sigma))
        #     print("       FWHM: " + '{:.3f}'.format(fwhm))
        #     print("-----------------------------------------")

        #     if verbose: print("Saving data")
        #     #save_dataset("scan/scantype", "DCM scan with linear bg gaussian fitting")
        #     save_dataset("gaussian/inclination", a)
        #     save_dataset("gaussian/offset", b)
        #     save_dataset("gaussian/amplitude", amp)
        #     save_dataset("gaussian/mean", com)
        #     save_dataset("gaussian/sigma", sigma)
        #     save_dataset("gaussian/fwhm", fwhm)
        #     save_dataset("plot/x", xgauss)
        #     save_dataset("plot/y", gauss)
        #     save_dataset("raw/izero", sensor)
        #     save_dataset("raw/dcm", motor)

        # elif fittype == "exp":
        #     if verbose: print("analysing")
        #     (eamp, decay, amp, com, sigma, gauss, fwhm, xgauss)=self.__gauss_exp_fit(sensor,motor)
        #     p1.getSeries(1).setData(xgauss, gauss)
        #     p1.setLegendVisible(True)
        #     print(" ")
        #     print("Gaussian fit -- exponetial background")
        #     print("-----------------------------------------")
        #     print("Exp. amplitude: " + '{:.3e}'.format(eamp))
        #     print("Exp.     decay: " + '{:.3e}'.format(decay))
        #     print("     Amplitude: " + '{:.3e}'.format(amp))
        #     print("          Mean: " + '{:.3f}'.format(com))
        #     print("         Sigma: " + '{:.3f}'.format(sigma))
        #     print("          FWHM: " + '{:.3f}'.format(fwhm))
        #     print("-----------------------------------------")

        #     if verbose: print("Saving data")
        #     save_dataset("scan/scantype", "DCM scan with exponential bg gaussian fitting")
        #     save_dataset("gaussian/exp_amplitude", eamp)
        #     save_dataset("gaussian/exp_decay", decay)
        #     save_dataset("gaussian/amplitude", amp)
        #     save_dataset("gaussian/mean", com)
        #     save_dataset("gaussian/sigma", sigma)
        #     save_dataset("gaussian/fwhm", fwhm)
        #     save_dataset("plot/x", xgauss)
        #     save_dataset("plot/y", gauss)
        #     save_dataset("raw/izero", sensor)
        #     save_dataset("raw/dcm", motor)

        # else:
        #     save_dataset("plot/x", motor)
        #     create_dataset("plot/y", 'd', False, (0, len(sensor)))
        #     append_dataset("plot/y", sensor)
        #     save_dataset("plot/title", "DCM Scan")
        #     save_dataset("plot/xlabel", "DCM")
        #     save_dataset("plot/ylabel", "Current")
        #     save_dataset("plot/y_desc", "scan 0")
        #     #save_dataset("scan/scantype", "DCM scan without fitting")
        #     save_dataset("raw/izero", sensor)
        #     save_dataset("raw/dcm", motor)

        ## save beamline/station snapshot
        pink_save_bl_snapshot()

        # end routine
        #if source=="izero":
        #    caput("PINK:CAE2:AcquireMode", 0)
        #else:
        #    caput("PINK:CAE1:AcquireMode", 0)
        #    caput("PINK:CAE1:Range", 1)

        ## Setup CAE2
        #0:free run 1:ext trigger
        #caput("PINK:CAE2:TriggerMode", 0)
        #0:continuous 1:multiple 2:single
        #caput("PINK:CAE2:AcquireMode", 0)
        #caput("PINK:CAE2:ValuesPerRead", 1000)
        #caput("PINK:CAE2:AveragingTime", 1)
        #caputq("PINK:CAE2:Acquire", 1)

        ## Return DCM to initial position
        #MOTOR.write(initpos)
        #sleep(0.2)
        #self.__waitdcm(point_delay)

        print("Scan finished. OK")

    ###########################################
    ### Support functions
    ###########################################

    def get_filename(self):
        execinfo=get_exec_pars()
        filenameinfo=execinfo.path
        filenameinfo=filenameinfo.split("/")
        fname=filenameinfo[-1]
        return fname    

    def __gauss_fit(self, ydata, xdata):
        (a, b, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i)+(a*i)+b for i in xgauss]
        fwhm = 2.355*sigma
        return (a, b, amp, com, sigma, gauss, fwhm, xgauss)

    def __gauss_exp_fit(self, ydata, xdata):
        (incl, off, amp, com, sigma) = fit_gaussian_linear(ydata, xdata)
        start_point=[1, 1, amp, com, sigma]
        (eamp, decay, amp, com, sigma) = fit_gaussian_exp_bkg(ydata, xdata, start_point=start_point)
        f = Gaussian(amp, com, sigma)
        xgauss = linspace(min(xdata), max(xdata), 400)
        gauss = [f.value(i) + eamp*math.exp(-i/decay) for i in xgauss]
        fwhm = 2.355*sigma
        return (eamp, decay, amp, com, sigma, gauss, fwhm, xgauss)

    def __waitdcm(self, point_delay):
        #set_status("Waiting for dcm...")
        #log("Waiting for dcm...")
        while(caget("u171dcm1:GK_STATUS", type='i')):
            sleep(0.5)
        log(time.asctime())
        sleep(point_delay)
        log(time.asctime())

    ## (trigger mode, shutter, Mythen, Greateyes, Caenels)
    ## trigger [5:single shot] [1: Ext rising edge]
    def setup_delaygen(self, mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])
