scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.4, X0=84200, X1=86700, dX=600., Y0=3500, Y1=16400, passes=8, sample='s#7 70; Reduced 0.33% Pd/0.33% Au/0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=84200, X1=86700, dX=600., Y0=3600, Y1=16350, passes=14, sample='s#7 70; Reduced 0.33% Pd/0.33% Au/0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=72200, X1=74900, dX=650., Y0=3600, Y1=16350, passes=14, sample='s#6 69; Reduced 0.475% Pd/0.475% Au/0.05% Pt NPss', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=60500, X1=63100, dX=650., Y0=3600, Y1=16350, passes=14, sample='s#5 42; 0.475% Pd/0.475% Au/0.05% Pt NPs', linedelay=0)
scan.spot(detector.greateyes(), X=50000, Y=16000, exposure=10, images=100, sample='s#4 Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=14700, exposure=1, images=200, sample='s#4 Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=50000, Y=13300, exposure=10, images=100, sample='s#4 Pd foil with kapton, Lg3')
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=36550, X1=39200, dX=600., Y0=3600, Y1=16350, passes=18, sample='s#3 68; Reduced 0.5% Pd/0.5% Au NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=60550, X1=63100, dX=600., Y0=3600, Y1=16350, passes=4, sample='s#5 42; 0.475% Pd/0.475% Au/0.05% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=72250, X1=74900, dX=600., Y0=3600, Y1=16350, passes=4, sample='s#6 69; Reduced 0.475% Pd/0.475% Au/0.05% Pt NPss', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=84250, X1=86700, dX=600., Y0=3600, Y1=16350, passes=8, sample='s#7 70; Reduced 0.33% Pd/0.33% Au/0.33% Pt NPs', linedelay=0)







