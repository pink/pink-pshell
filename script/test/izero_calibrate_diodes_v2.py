# Find linear fit coefs to diode displacement 
def main():
    # distance from middle (um)
    drange = 300.0
    # exposure (sec)
    exposure = 5.0
    
    ## channels
    print("Creating channels...")
    xmotor = create_channel_device("PINK:SMA01:m10a.VAL", type='d')
    xmotor_rbv = create_channel_device("PINK:SMA01:m10a.RBV", type='d')
    xmotor_rbv.setMonitored(True)
    xmotor_dmov = create_channel_device("PINK:SMA01:m10a.DMOV", type='d')
    xmotor_dmov.setMonitored(True)
    ymotor = create_channel_device("PINK:SMA01:m9a.VAL", type='d')
    ymotor_rbv = create_channel_device("PINK:SMA01:m9a.RBV", type='d')
    ymotor_rbv.setMonitored(True)
    ymotor_dmov = create_channel_device("PINK:SMA01:m9a.DMOV", type='d')
    ymotor_dmov.setMonitored(True)
    xbeam = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV", type='d')
    xbeam.setMonitored(True)
    ybeam = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV", type='d')
    ybeam.setMonitored(True)
    cae2_acq = create_channel_device("PINK:CAE2:Acquire", type='i')
    sleep(2)
    
    ## cae2 setup
    print("Setup CAE2...")
    caputq("PINK:CAE2:Acquire", 0)
    sleep(2)
    caput("PINK:CAE2:AcquireMode", 2)
    caput("PINK:CAE2:ValuesPerRead", 1000*exposure)
    caput("PINK:CAE2:AveragingTime", exposure)
    sleep(1)

    ## arrays
    alabel = ["X", "Y"]
    amotor = [xmotor, ymotor]
    amotor_rbv = [xmotor_rbv, ymotor_rbv]
    amotor_dmov = [xmotor_dmov, ymotor_dmov]
    abeampos = [xbeam, ybeam]
    rvar = []
    
    ## calibration
    print("Finding middle positions...")

    for j in range(2):
        for i in range(2):
            label = alabel[i]
            motor = amotor[i]
            motor_rbv = amotor_rbv[i]
            motor_dmov = amotor_dmov[i]
            beampos = abeampos[i]
            print("#####################")
            print("#  Moving {} axis...".format(label))
            pos0 = motor_rbv.read()
            ## y0/x0
            pos=pos0-drange
            print("Moving to position {:.1f} and reading diodes...".format(pos))
            motor.write(pos)
            motor_rbv.waitValueInRange(pos, 1.0, 10000)
            motor_dmov.waitValueInRange(1, 0.5, 10000)
            while(cae2_acq.read()>0): sleep(1)
            cae2_acq.write(1)
            beampos.waitCacheChange(1000*int(exposure+20))
            y0 = beampos.read()
            x0 = motor_rbv.read()
            print("y0: {:.6f} x0: {:.6f}".format(y0,x0))
            ## y1/x1
            pos=pos0+drange
            print("Moving to position {:.1f} and reading diodes...".format(pos))
            motor.write(pos)
            motor_rbv.waitValueInRange(pos, 1.0, 10000)
            motor_dmov.waitValueInRange(1, 0.5, 10000)
            while(cae2_acq.read()>0): sleep(1)
            cae2_acq.write(1)
            beampos.waitCacheChange(1000*int(exposure+20))
            y1 = beampos.read()
            x1 = motor_rbv.read()
            print("y1: {:.6f} x1: {:.6f}".format(y1,x1))
            ## calc
            ra = (y1-y0)/(x1-x0)
            rb = y0-(ra*x0)
            rvar.append([ra,rb])
            print("a: {}\nb: {}".format(ra,rb))
            ## move to middle
            xm=-rb/ra
            print("Mid position: {:.3f}".format(xm))
            motor.write(xm)
            motor.write(pos0)
            sleep(2)
    ## End of loop
    # X axis
    rvec = rvar[2]
    a = rvec[0]
    b = rvec[1]
    XA = 1/a
    XB = -b/a
    # Y axis
    rvec = rvar[3]
    a = rvec[0]
    b = rvec[1]
    YA = 1/a
    YB = -b/a
    # Results
    print("\nXA: {}\nXB: {}".format(XA,XB))
    print("YA: {}\nYB: {}".format(YA,YB))
    # push results
    #caput("PINK:CAE2:PosCalcXA", XA)
    #caput("PINK:CAE2:PosCalcXB", XB)
    #caput("PINK:CAE2:PosCalcYA", YA)
    #caput("PINK:CAE2:PosCalcYB", YB)
            
main()
print("OK")