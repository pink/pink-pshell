class ATTO():
    def __init__(self):
        self.enable=True
        self.speed=190
        self.state=0
        self.deadband=10
        self.pos0=0
        self.ts0=0
        self.kp=0.5
        self.power = create_channel_device("PINK:ANC01:ACT0:CMD:OUTPUT_POWER", type='i')
        self.power.setMonitored(True)
        self.position = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
        self.position.setMonitored(True)
        self.target = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
        self.target.setMonitored(True)
        self.freq = create_channel_device("PINK:ANC01:ACT0:CMD:FREQ", type='d')
        self.freq.setMonitored(True)
        self.freq_RBV = create_channel_device("PINK:ANC01:ACT0:CLC_FREQ", type='d')
        self.freq_RBV.setMonitored(True)
        
    def worker(self):
        poserr = abs(self.target.take()-self.position.take())
        if(self.enable and self.power.take() and (poserr>self.deadband)):
            if self.state==0:
                self.pos0=self.position.take()
                self.ts0=time.clock()
                self.state=1
                log("Atto enabled")
            elif self.state==1:
                pos1 = self.position.take()
                ts1 = time.clock()
                avgspeed = abs(pos1-self.pos0)/(ts1-self.ts0)
                atto_avgspeed.write(avgspeed)
                speederr = (self.speed-avgspeed)/(self.speed)
                if abs(speederr)>0.01:
                    correction = self.kp*speederr
                    newfreq = round(self.freq_RBV.take()*(1+correction))
                    atto_freq.write(newfreq)
                    self.freq.write(newfreq)
        else:
            if self.state:
                log("Atto disabled")
                self.state=0
                
attotask = ATTO()