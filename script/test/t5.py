## find neighbours on 2d matrix

xdim = 3
ydim = 4
N = xdim*ydim

def getYindex(n, xdim, ydim):
    return n%ydim

def getXindex(n, xdim, ydim):
    return math.floor(float(n)/ydim)

for i in range(N):
    print("{}: {}".format(i,getYindex(i, xdim, ydim)))

for i in range(N):
    print("{}: {}".format(i,getXindex(i, xdim, ydim)))

for i in range(N):
    np = []
    if getYindex(i-1, xdim, ydim) < getYindex(i, xdim, ydim):
        if (i-1)>=0:
            np.append(i-1)
    if getYindex(i+1, xdim, ydim) > getYindex(i, xdim, ydim):
        if (i+1)<N:
            np.append(i+1)
    if getXindex(i-ydim, xdim, ydim) < getXindex(i, xdim, ydim):
        if (i-ydim)>=0:
            np.append(i-ydim)
    if getXindex(i+ydim, xdim, ydim) > getXindex(i, xdim, ydim):
        if (i+ydim)<N:
            np.append(i+ydim)
    print("{}: {}".format(i, np))
      
        
       