pos=[11.1,22.2]
nrgs = [100.0,200.0,300.0]
img=[[1,2,3],[4,5,6]]
spec_sum = [0,1,10,20,10,1,0]

for ispot in range(3):
    for ienergy in range(3):
        for ipass in range(2):
            dpath = "meas/spots/spot_{:02d}/energy_{:02d}/pass_{:02d}".format(ispot, ienergy, ipass)
            create_group(dpath)




dpath = "meas/spots/positions"
save_dataset(dpath, [pos,pos,pos])

for ispot in range(3):
    dpath = "meas/spots/spot_{:02d}/".format(ispot)
    save_dataset(dpath+'energies', nrgs)
    for ienergy in range(3):
        for ipass in range(2):
            dpath = "meas/spots/spot_{:02d}/energy_{:02d}/pass_{:02d}/".format(ispot, ienergy, ipass)
            save_dataset(dpath+'images', [img,img,img])
            save_dataset(dpath+'spec_sum', spec_sum)

