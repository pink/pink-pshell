def mbusy():
    err=abs(target.take()-position.take())
    if err>10.0:
        return True
    else:
        return False

def waitmotor():
    while(mbusy()):
        sleep(1)  

## speed guess
speed_target = 40.0
kp = 0.25

## setup
pos_orig = position.take()
freq.write(500.0)
target.write(15000.0)
waitmotor()

sleep(3)
print("Running...")
freq.write(speed_target)
atto_freq.write(speed_target)
speed_err.write(0.0)
target.write(5000.0)
sleep(1)
lastfreq=speed_target
for i in range(20):
    speederr = (speed_target-speed_avg_RBV.take())/(speed_target)
    correction = kp*speederr
    newfreq = round(freq_RBV.take()*(1+correction))
    atto_freq.write(newfreq)
    speed_err.write(speed_avg_RBV.take()-speed_target)
    freq.write(newfreq)
    sleep(1)
print("Last freq: {:.1f} Hz\t Last speed avg: {:.1f} um/s".format(newfreq, speed_avg_RBV.take()))    
print("Done")


    