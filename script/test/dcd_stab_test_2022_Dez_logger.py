#DCM energy scan stabilization tests
def main():
    ## Parameteres
    #start=1000
    #end=2000
    #step=250.0
    exposure=1.0
    point_delay=0.0
    note="DCM motor records tests"

    Epics.destroy()
    Epics.create()
    
    def waitdcm(dmov, delay):
        while(dmov.read()):
            sleep(0.5)
        sleep(delay)

    def setup_delaygen(mode, ch1, ch2, ch3, ch4):
        ## Trigger mode
        caput("PINK:DG01:TriggerSourceMO", mode)
        ## shutter delay
        caput("PINK:DG01:ADelayAO", ch1[0])
        ## shutter exposure
        caput("PINK:DG01:BDelayAO", ch1[1])
        ## mythen delay
        caput("PINK:DG01:CDelayAO", ch2[0])
        ## mythen exposure
        caput("PINK:DG01:DDelayAO", ch2[1])
        ## greateyes delay
        caput("PINK:DG01:EDelayAO", ch3[0])
        ## greateyes exposure
        caput("PINK:DG01:FDelayAO", ch3[1])
        ## extra channel delay
        caput("PINK:DG01:GDelayAO", ch4[0])
        ## extra channel exposure
        caput("PINK:DG01:HDelayAO", ch4[1])           

  
    ## calculate scan positions
    #positionarray = linspace(start, end, step)
    #print("Energy array: {}".format(positionarray))

    ## set parameters
    data_compression = {"compression":"True", "shuffle":"True"}
    
    ## create epics channels
    set_status("Creating EPICS channels")
    #DCM
    ENERGY = create_channel_device("u171dcm1:monoSetEnergy")
    ENERGY_RBV = create_channel_device("u171dcm1:monoGetEnergy")
    ENERGY_RBV.setMonitored(True)
    ENERGY_DMOV = create_channel_device("u171dcm1:GK_STATUS", type='i')
    ENERGY_DMOV.setMonitored(True)
    PZPITCH = create_channel_device("PINK:DCMSTAB:calc:pitch:avg")
    PZPITCH.setMonitored(True)
    PZPITCHVolt = create_channel_device("MONOY01U112L:Piezo2U1")
    PZPITCHVolt.setMonitored(True)
    ROLL = create_channel_device("PINK:DCMSTAB:calc:roll:avg")
    ROLL.setMonitored(True)
    ROLLVolt = create_channel_device("MONOY01U112L:Piezo3U1")
    ROLLVolt.setMonitored(True)    
    DCMidon = caget("u171dcm1:SetIdOn")
    # new dcm PVs
    #newEnergy = create_channel_device("myDCM:axis_Energy")
    #newEnergy.setMonitored(True
    #newTheta = create_channel_device("myDCM:axis_Theta")
    #newTheta.setMonitored(True
    #newThetaCryo = create_channel_device("myDCM:pmaxAxis4")
    #newThetaCryo.setMonitored(True

    #    #BPM3
    #    BPM3ROIxdim = int(caget("PINK:PG03:image2:ArraySize0_RBV"))
    #    BPM3ROIydim = int(caget("PINK:PG03:image2:ArraySize1_RBV"))    
    #    BPM3ROI = create_channel_device("PINK:PG03:image2:ArrayData", size=(BPM3ROIxdim*BPM3ROIydim))
    #    BPM3ROI.setMonitored(True)
    #    BPM3yfit = create_channel_device("PINK:BPM:3:Y:center")
    #    BPM3yfit.setMonitored(True)
    #    BPM3yprofile = create_channel_device("PINK:PG03:Stats2:ProfileAverageY_RBV", size=BPM3ROIydim)
    #    BPM3yprofile.setMonitored(True)
    #    BPM3xfit = create_channel_device("PINK:BPM:3:X:center")
    #    BPM3xfit.setMonitored(True)
    #    BPM3xprofile = create_channel_device("PINK:PG03:Stats2:ProfileAverageX_RBV", size=BPM3ROIxdim)
    #    BPM3xprofile.setMonitored(True)   
    
    #BPM4
    BPMROIxdim = int(caget("PINK:PG02:image2:ArraySize0_RBV"))
    BPMROIydim = int(caget("PINK:PG02:image2:ArraySize1_RBV"))    
    BPMROI = create_channel_device("PINK:PG02:image2:ArrayData", size=(BPMROIxdim*BPMROIydim))
    BPMROI.setMonitored(True)
    BPMyfit = create_channel_device("PINK:BPM:4:Y:center")
    BPMyfit.setMonitored(True)
    BPMyprofile = create_channel_device("PINK:PG02:Stats2:ProfileAverageY_RBV", size=BPMROIydim)
    BPMyprofile.setMonitored(True)
    BPMxfit = create_channel_device("PINK:BPM:4:X:center")
    BPMxfit.setMonitored(True)
    BPMxprofile = create_channel_device("PINK:PG02:Stats2:ProfileAverageX_RBV", size=BPMROIxdim)
    BPMxprofile.setMonitored(True)    

    #Izero
    ACQ = create_channel_device("PINK:CAE2:Acquire", type='i')
    IZero = create_channel_device("PINK:CAE2:SumAll:MeanValue_RBV", type='d')
    IZero.setMonitored(True)
    IZerod1 = create_channel_device("PINK:CAE2:Current1:MeanValue_RBV")
    IZerod1.setMonitored(True)
    IZerod2 = create_channel_device("PINK:CAE2:Current2:MeanValue_RBV")
    IZerod2.setMonitored(True)
    IZerod3 = create_channel_device("PINK:CAE2:Current3:MeanValue_RBV")
    IZerod3.setMonitored(True)
    IZerod4 = create_channel_device("PINK:CAE2:Current4:MeanValue_RBV")
    IZerod4.setMonitored(True)
    IZeroposx = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV")
    IZeroposx.setMonitored(True)
    IZeroposy = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV")
    IZeroposy.setMonitored(True)

    #gap
    GAP = create_channel_device("U17IT6R:BasePmGap.A")
    GAP.setMonitored(True)

    # wait for all connections
    sleep(2)

    ## Create datasets
    set_exec_pars(open=False, name="dcm_log_data_pink", reset=True)
    save_dataset("scan/scantype", "DCM test")
    save_dataset("scan/start_time", time.ctime())
    #save_dataset("scan/start", start)
    #save_dataset("scan/end", end)
    #save_dataset("scan/step", step)
    save_dataset("scan/exposure", exposure)
    save_dataset("scan/point_delay", point_delay)
    save_dataset("scan/note", note)
    save_dataset("DCM/idOn", DCMidon)

    create_dataset("data/energy", 'd', False)
    create_dataset("data/pzpitch", 'd', False)
    create_dataset("data/pzpitch_volt", 'd', False)
    create_dataset("data/roll", 'd', False)
    create_dataset("data/roll_volt", 'd', False)
    create_dataset("data/bpm4_ypos", 'd', False)
    create_dataset("data/bpm4_yprofile", 'd', False, (0, BPMROIydim))
    create_dataset("data/bpm4_xpos", 'd', False)
    create_dataset("data/bpm4_xprofile", 'd', False, (0, BPMROIxdim))
    create_dataset("data/bpm4_imgs", 'd', False, (0, BPMROIydim, BPMROIxdim), features=data_compression)
    create_dataset("data/IZero", 'd', False)
    create_dataset("data/IZero_d1", 'd', False)
    create_dataset("data/IZero_d2", 'd', False)
    create_dataset("data/IZero_d3", 'd', False)
    create_dataset("data/IZero_d4", 'd', False)
    create_dataset("data/IZero_posx", 'd', False)
    create_dataset("data/IZero_posy", 'd', False)
    create_dataset("data/gap", 'd', False)
    create_dataset("data/timestamp", 'l', False)
    ##new dcm pvs
    #create_dataset("data/newDCM/Energy", 'd', False)
    #create_dataset("data/newDCM/Theta", 'd', False)
    #create_dataset("data/newDCM/ThetaCryo", 'd', False)

    ## Create Plot
    plottitle="pzPitch"
    plottitle2="Quadrant Diodes Y"
    plottitle3="BPM4"
    plottitle4="Izero"
    [p1, p2, p3, p4]=plot([None, None, None, None], [plottitle, plottitle2, plottitle3, plottitle4], title="DCM Scan")
    #p1.getAxis(p1.AxisId.X).setRange(min(start, end),max(start,end))
    #p2.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
    #p3.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
    #p4.getAxis(p2.AxisId.X).setRange(min(start, end),max(start,end))
    #p1.getAxis(p1.AxisId.X).setLabel("Energy")
    #p2.getAxis(p1.AxisId.X).setLabel("Energy")
    #p3.getAxis(p1.AxisId.X).setLabel("Energy")
    #p4.getAxis(p1.AxisId.X).setLabel("Energy")

    ## Setup Caenels
    #caput("PINK:CAE2:ValuesPerRead", int(1000*exposure))
    #caput("PINK:CAE2:AveragingTime", exposure)
    #caput("PINK:CAE2:TriggerMode", 0)
    #caput("PINK:CAE2:AcquireMode", 0)    

    #Move DCM
    #print("Moving DCM to start position...")
    #pos = positionarray[0]
    #ENERGY.write(pos)
    #sleep(1)
    #waitdcm(ENERGY_DMOV, point_delay)
    #sleep(1)

    print("Logging...")
    print(time.asctime())
    t0=time.time()
    counter=1
    #print("Filename: {}".format(self.__get_filename()))

    ## Main loop
    energy=[]
    s1=[]
    s2=[]
    s3=[]
    s4=[]
    points=[]
    set_status("Logging...")
    log_enable.write(1.0)
    #ACQ.write(1)
    try:
        while (log_enable.value>0.0):
            #print("DCM set to {}".format(pos))
            #set_status("Moving DCM")
            #ENERGY.write(pos)
            #sleep(0.5)
            #waitdcm(ENERGY_DMOV, point_delay)
            #sleep(1)
    
            ##trigger acq
            #set_status("Measuring")
            #ACQ.write(1)
            resp = IZero.waitCacheChange(int(1000*(exposure+2)))
            sleep(0.2)
            #energy.append(ENERGY_RBV.take())
            #set_status("update plot")

            points.append(counter)
            energy.append(ENERGY_RBV.take())
            s1.append(PZPITCH.take())
            s2.append(IZeroposy.take())
            s3.append(BPMyfit.take())
            s4.append(IZero.take())

            ## update plot
            p1.getSeries(0).setData(points,s1)
            p2.getSeries(0).setData(points,s2)
            p3.getSeries(0).setData(points,s3)
            p4.getSeries(0).setData(points,s4)
    
            ## save data
            #set_status("save data")
            append_dataset("data/energy", ENERGY_RBV.take())
            append_dataset("data/pzpitch", PZPITCH.take())
            append_dataset("data/pzpitch_volt", PZPITCHVolt.take())
            append_dataset("data/roll", ROLL.take())
            append_dataset("data/roll_volt", ROLLVolt.take())
            append_dataset("data/bpm4_ypos", BPMyfit.take())
            append_dataset("data/bpm4_yprofile", BPMyprofile.take())
            append_dataset("data/bpm4_xpos", BPMxfit.take())
            append_dataset("data/bpm4_xprofile", BPMxprofile.take())     
            append_dataset("data/bpm4_imgs", Convert.reshape(BPMROI.take(), BPMROIydim, BPMROIxdim))
            append_dataset("data/IZero", IZero.take())
            append_dataset("data/IZero_d1", IZerod1.take())
            append_dataset("data/IZero_d2", IZerod2.take())
            append_dataset("data/IZero_d3", IZerod3.take())
            append_dataset("data/IZero_d4", IZerod4.take())
            append_dataset("data/IZero_posx", IZeroposx.take())
            append_dataset("data/IZero_posy", IZeroposy.take())
            append_dataset("data/gap", GAP.take())
            append_dataset("data/timestamp", IZero.getTimestampNanos())
            ##new dcm pvs
            #append_dataset("data/newDCM/Energy", newEnergy.take())
            #append_dataset("data/newDCM/Theta", newTheta.take())
            #append_dataset("data/newDCM/ThetaCryo", newThetaCryo.take())

            t1=time.time()
            msg="Logging. Points: {}, T: {:.0f}s".format(counter, (t1-t0))
            set_status(msg)
            counter+=1
    except Exception, value:
        print(value)
        
    print("Logging canceled")
    msg = "{} points in {:.0f} seconds".format(counter, (t1-t0))
    print(msg)
    #ACQ.write(0)

## Main
print("-- DCM Log --")
main()
print("-- Done --")

