class mclass():
    def myfun(self, *args, **kwargs):
        ab=kwargs["ab"]
        print(ab)
        print(args[0])

mc = mclass()
mc.myfun("34", ab="ok")    

def bpmanalysis(vec, pos0, pos1, N, myCF):
    vecb = vec[0:N]
    xvecabs = range(pos0,pos1)
    xvec = range(0,N)
    weights = [ 1.0] * len(xvec)
    (a, b, amp, com, sigma) = fit_gaussian_linear(vecb, xvec, None, weights)
    abspos=com+pos0
    return abspos

def beampos_bpm2_v():
    bpm2_vert_CF=13
    bpm2_hor_CF=13*0.707
    sig2fwmh=2.355
    roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
    roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
    roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
    roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
    N = int(roisizey)
    pos0 = roiposy
    pos1 = roiposy+roisizey
    myCF = bpm2_vert_CF
    vec = caget("PINK:PG04:Stats2:ProfileAverageY_RBV")
    beampos = bpmanalysis(vec, pos0, pos1, N, myCF)
    return beampos

def beampos_bpm2_h():
    bpm2_vert_CF=13
    bpm2_hor_CF=13*0.707
    sig2fwmh=2.355
    roiposx = caget("PINK:PG04:ROI1:MinX_RBV")
    roiposy = caget("PINK:PG04:ROI1:MinY_RBV")
    roisizex = caget("PINK:PG04:ROI1:SizeX_RBV")
    roisizey = caget("PINK:PG04:ROI1:SizeY_RBV")
    N = int(roisizex)
    pos0 = roiposx
    pos1 = roiposx+roisizex
    myCF = bpm2_hor_CF
    vec = caget("PINK:PG04:Stats2:ProfileAverageX_RBV")
    beampos = bpmanalysis(vec, pos0, pos1, N, myCF)
    return beampos

print("BPM abs pos VERT: {}".format(beampos_bpm2_v()))
print("BPM abs pos HOR: {}".format(beampos_bpm2_h()))