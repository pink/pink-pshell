## test multidimensions


img1 = [[4.0,5.0],[6.0,7.0]]
ev1 = [1,2,3]
sp1 = 3.3

dic1 = {"ab":1, "bc":2}

names=["spot", "energy", "img"]
types=['d','d','[d']
lengths=[0,0,3]

ex1=[1.2, 3.3, ev1]

create_table("test/t1", names=names, types=types, lengths=lengths, features = None)

append_table("test/t1", ex1)

save_dataset("test/ob1", dic1, type = 'o', unsigned = False, features = None)