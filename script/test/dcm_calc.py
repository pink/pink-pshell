## DCM calculations

Height_Offset=-579380
Pitch_Offset= -539680
Roll_Offset=-536446
counts_to_nm=5  #in micro meters
accuracy=0.5 #in microrad


value_height=(caget("u171dcm1:dcm:cr2TraHeightEncoder")-Height_Offset)*counts_to_nm
value_pitch=(caget("u171dcm1:dcm:cr2PitchEncoder")-Pitch_Offset)*counts_to_nm
value_roll=(caget("u171dcm1:dcm:cr2RollEncoder")-Roll_Offset)*counts_to_nm

# pzpit
temp_pitch=(value_pitch-value_height)/64.75e6
theta_pitch=math.atan(temp_pitch)*1000000
print("   pzpit: {:.3f} urad".format(theta_pitch))

## pzroll
temp_roll=(value_roll-value_height)/127.75e6
theta_roll=math.atan(temp_roll)*1000000
print("  pzroll: {:.3f} urad".format(theta_roll))

## height
print("pzheight: {:.3f} mm".format(value_height*1e-6))