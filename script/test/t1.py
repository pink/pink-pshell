def countermain():
    print("channels...")
    cpv = create_channel_device("PC:a1", type = 'd')
    cpv.setMonitored(True)
    sleep(2)
    print("counting...")
    enable=True
    while enable:
        try:
            t0=time.time()
            rr = cpv.waitCacheChange(2000)
            dt=1000.0*(time.time()-t0)
            print("cpv: {:04d}, dt={:.1f} ms".format(int(cpv.read()), dt))
        except Exception, errmsg:
            print("** Err:")
            print(errmsg)
            resp = get_option("problem. Continue?", type = 'YesNo')
            if resp=='No':
                enable=False

#try:
#    countermain()
#except Exception, value:
#    print(value)

countermain()
print("ok")

    