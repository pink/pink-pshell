def testme(offsets, Xpoints):
    if len(offsets)==0:
        doffs = array('d', [0.0 for i in range(Xpoints+1)])
    elif len(offsets)!=int(Xpoints):
        print("Error: Number of entries in offsets does not match number of lines in X direction")
        return 
    else:
        offsets.append(0.0)
        doffs = array('d', offsets)

    for o in doffs:
        print(o)
    print(str(offsets))

Xpoints = 3
offsets = [1,2.2, 1455e-3]
#offsets = []

testme(offsets,Xpoints)