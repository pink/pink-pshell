#test

y1 = [1,2,3,4,-5,4,3,2,1]
y2 = [x**2 for x in y1]
x1 = [(x+1)*100 for x in range(9)]

[p1, p2]=plot([None, None], ['',''], title="Sample Scan")
#p1 = p1h.get(0)
p1.getAxis(p1.AxisId.X).setRange(min(x1),max(x1))
p1.getSeries(0).setData(x1, y1)
p1.setTitle("Max Count")
p1.getAxis(p1.AxisId.X).setLabel("Position")
p1.getAxis(p1.AxisId.Y).setLabel("Y1")

p2.getAxis(p2.AxisId.X).setRange(min(x1),max(x1))
p2.getSeries(0).setData(x1, y2)
p2.setTitle("Horizontal ROI Sum")
p2.getAxis(p2.AxisId.X).setLabel("Position")
p2.getAxis(p2.AxisId.Y).setLabel("Y2")



