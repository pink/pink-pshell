## device test

def main():
    run("plib/devices/caenels")
    #cae1 = CAE("CAE1")
    #cae1.test()

    ## setup filename and datapaths
    set_exec_pars(open=False, name="dev_test", reset=True)
    run("config/pinkdatapath")
    create_group(PINK_attb_path)

    izero = CAE2()
    izero.set_trigger_SW(2)
    print("waiting...")
    izero.trigger_and_wait()
    izero.save_attb()

    izero.create_dset()
    for i in range(5):
        print("loop: {:02d}".format(i))
        izero.trigger_and_wait()
        izero.append_dset()

main()
del main
print("ok")