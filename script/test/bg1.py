def bg1():
    log("** bg1 A **")
    sleep(30)
    log("** bg1 B **")


class ATTO():
    def __init__(self):
        self.enable=False
        self.forklist = []
        self.job = []
        ## cryo x
        self.cryo_x = create_channel_device("PINK:PHY:AxisJ.VAL", type='d')
        self.cryo_x_RBV = create_channel_device("PINK:PHY:AxisJ.RBV", type='d')
        self.cryo_x_RBV.setMonitored(True)
        self.cryo_x_DMOV = create_channel_device("PINK:PHY:AxisJ.DMOV", type='d')
        self.cryo_x_DMOV.setMonitored(True)
        ## cryo y
        self.cryo_y = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
        self.cryo_y_RBV = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
        self.cryo_y_RBV.setMonitored(True)
        self.cryo_y_DMOV = create_channel_device("PINK:ANC01:ACT0:IN_TARGET", type='d')
        self.cryo_y_DMOV.setMonitored(True)
        self.cryo_y_SPEED = create_channel_device("PINK:ANC01:ACT0:calcspeed", type='d')
        self.cryo_y_SPEED.setMonitored(True)
        self.cryo_y_SPEED_AVG = create_channel_device("PINK:ANC01:ACT0:speedavg", type='d')
        self.cryo_y_SPEED_AVG.setMonitored(True)        

    def arun(self):
        print("arun started")
        self.enable=True
        self.posarray=[]
        while self.enable:
            self.cryo_y_SPEED.waitCacheChange(1000)
            self.posarray.append(self.cryo_y_SPEED.take())
        print("arun ended")

    def astart(self):
        self.forklist=fork(self.arun)
        self.job=self.forklist.get(0)

    def astop(self):
        self.enable=False
        t0=time.clock()
        join(self.forklist)
        t1=time.clock()
        print("Join waiting time: {} ms".format((t1-t0)*1000))


atto = ATTO()        
sleep(2)
print("start")
atto.astart()
sleep(10)
atto.astop()
print("finish")
aa = atto.posarray
print(aa)
del atto    