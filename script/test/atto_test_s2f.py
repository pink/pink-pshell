def atto_speed2freq(spd):
    def atto_reset():
        caput("PINK:ANC01:ACT0:CMD:FREQ", 500.0)
        caput("PINK:ANC01:ACT0:CMD:TARGET", 10000.0)
        running=True
        while(running):
            err = abs(10000.0 - caget("PINK:ANC01:ACT0:POSITION"))
            if err<5.0:
                running=False
            sleep(1)

    ## variables
    spd=float(spd)
    DEBUG=True    
    freqs=[0,0]
    
    ## Test speed forward
    atto_reset()
    caput("PINK:ANC01:ACT0:CMD:FREQ", 100.0)
    caput("PINK:ANC01:ACT0:CMD:CONT_FWD", 1)
    sleep(0.5)
    pos0=caget("PINK:ANC01:ACT0:POSITION")
    t0=time.clock()
    sleep(2)
    pos1=caget("PINK:ANC01:ACT0:POSITION")
    t1=time.clock()
    caput("PINK:ANC01:ACT0:CMD:CONT_FWD", 0)
    spd_now=abs(pos1-pos0)/(t1-t0)
    if DEBUG: print("Atto FWD speed at 100Hz: {:.1f}".format(spd_now))
    freqs[0]=round((100*spd)/spd_now)
    if DEBUG: print("Atto FWD freq for {:.1f}um/s: {:.1f}".format(spd,freqs[0]))
    
    ## Test speed backwards
    caput("PINK:ANC01:ACT0:CMD:CONT_BCK", 1)
    sleep(0.5)
    pos0=caget("PINK:ANC01:ACT0:POSITION")
    t0=time.clock()
    sleep(2)
    pos1=caget("PINK:ANC01:ACT0:POSITION")
    t1=time.clock()
    caput("PINK:ANC01:ACT0:CMD:CONT_BCK", 0)
    spd_now=abs(pos1-pos0)/(t1-t0)
    if DEBUG: print("Atto BWD speed at 100Hz: {:.1f}".format(spd_now))
    freqs[1]=round((100*spd)/spd_now)
    if DEBUG: print("Atto BWD freq for {:.1f}um/s: {:.1f}".format(spd,freqs[1]))        
    return freqs

freqs = atto_speed2freq(100.0)
print("Freq: {}".format(freqs))