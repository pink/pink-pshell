##rpiswitch reliability test

varlist = dir()
connDelay = False

if "ccdTrig" not in varlist:
    ccdTrig = create_channel_device("PINK:GEYES:cam1:Acquire", type='i')
    connDelay = True
    print("1")

if "caeID" not in varlist:
    caeID = create_channel_device("PINK:CAE2:NumAcquired", type='i')
    caeID.setMonitored(True)
    connDelay = True
    print("2")

if "ccdID" not in varlist:
    ccdID = create_channel_device("PINK:GEYES:cam1:ArrayCounter_RBV", type='i')
    ccdID.setMonitored(True)
    connDelay = True
    print("3")

if "rowA" not in varlist:
    rowA = create_channel_device("PINK:RPISW:select_A", type='i')
    connDelay = True
    print("4")

if "rowB" not in varlist:
    rowB = create_channel_device("PINK:RPISW:select_B", type='i')
    connDelay = True
    print("5")

## conn delay
if connDelay:
    print("Connecting new channels...")
    sleep(2)

## Test loop
N = 5
errTCount = 0
errCCDCount = 0
errCAECount = 0

print("## Test loop ##")
print("## N: {}".format(N))
for i in range(N):
    sleep(2)
    ## set switch ON
    rowA.write(0)
    sleep(0.5)
    rowB.write(0)
    sleep(0.5)
    lastccdID = ccdID.read()
    lastcaeID = caeID.read()
    set_status("CCD Trig... waiting ccd counter increment")
    ccdTrig.write(1)
    resp = ccdID.waitCacheChange(3000)
    sleep(0.1)
    currentccdID = ccdID.read()
    if (resp == False):
        errTCount += 1
    if (currentccdID==lastccdID):
        errCCDCount += 1
        print("current: {}, last: {}, read: {}".format(currentccdID, lastccdID, ccdID.read()))
    set_status("end step 1")

    ## set switch ON
    rowA.write(1)
    sleep(0.5)
    rowB.write(1)
    sleep(0.5)
    lastccdID = ccdID.read()
    lastcaeID = caeID.read()
    set_status("CCD Trig... waiting CCD and CAE counter increment")
    ccdTrig.write(1)
    resp = ccdID.waitCacheChange(3000)
    sleep(0.1)
    if (resp == False):
        errTCount += 1
    if (ccdID.read()==lastccdID):
        errCCDCount += 1
    if (caeID.read()==lastcaeID):
        errCAECount += 1
    set_status("end step 2")

    print("[Progress] {}/{} ({:.1f}%), [Errors] Timeout: {}, CCD: {}, CAE: {}".format((i+1),N,(100.0*(i+1)/N),errTCount,errCCDCount, errCAECount))

print("Done")


