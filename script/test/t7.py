Epics.destroy()
Epics.create()

GE_frameID = create_channel_device("PINK:GEYES:cam1:ArrayCounter_RBV", type='d')
GE_frameID.setMonitored(True)
sleep(2)

DEVCCD = pinkdevice.ccd()
#DEVCCD.takebackground(exposure=1.0, images=4, discard=1)
DEVCCD.takebackground(exposure=1.0, images=4, discard=1, ccdid=GE_frameID)
del DEVCCD

#5,4 400
#5,2 500
#5,0 400
#1,0 800
#2,1 800
#3,2 500
#5,0 400
#4,1 400
