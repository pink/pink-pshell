## ccd BG test
Epics.destroy()
Epics.create()

GE_ROI_X = caget("PINK:GEYES:image3:ArraySize0_RBV")
GE_ROI_Y = caget("PINK:GEYES:image3:ArraySize1_RBV")
GE_X = caget("PINK:GEYES:image4:ArraySize0_RBV")
GE_Y = caget("PINK:GEYES:image4:ArraySize1_RBV")
data_compression = {"compression":"True", "shuffle":"True"}

## epics channels
ccdAcq = create_channel_device("PINK:GEYES:cam1:Acquire", type='i')
ccdtemp = create_channel_device("PINK:GEYES:cam1:TemperatureActual")
ccdtemp.setMonitored(True)
ccdsum = create_channel_device("PINK:GEYES:Stats1:Total_RBV")
ccdsum.setMonitored(True)
ccdid = create_channel_device("PINK:GEYES:cam1:ArrayCounter_RBV")
ccdid.setMonitored(True)
ccdprofilex = create_channel_device("PINK:GEYES:Stats1:ProfileAverageX_RBV")
ccdprofilex.setMonitored(True)
ccdprofiley = create_channel_device("PINK:GEYES:Stats1:ProfileAverageY_RBV")
ccdprofiley.setMonitored(True)

GE_raw_array = create_channel_device("PINK:GEYES:image1:ArrayData", type='[d', size=int(GE_X*GE_Y))
GE_raw_array.setMonitored(True)
GE_roi_array = create_channel_device("PINK:GEYES:image3:ArrayData", type='[d', size=int(GE_ROI_X*GE_ROI_Y))
GE_roi_array.setMonitored(True)
GE_Spectra = create_channel_device("PINK:GEYES:spectrum_RBV", type='[d', size=GE_X)
GE_Spectra.setMonitored(True)

sleep(2)

passes = 5
images = 30

caput("PINK:GEYES:cam1:NumImages", images)

save_dataset("time/asctime", time.asctime())

create_dataset("ccd/raw/image", 'd', False, (0, GE_Y, GE_X), features=data_compression)
create_dataset("ccd/processed/image", 'd', False, (0, GE_ROI_Y, GE_ROI_X), features=data_compression)
create_dataset("ccd/processed/spectrum", 'd', False, (0, GE_ROI_X))

sumpasses = []
sumimage = []
profilesx = []
profilesy = []

print("* * * *")
print("pass:{}/{}, image:{}/{}".format(0,passes,0,images))
for j in range(passes):
    ccdAcq.write(1)
    for i in range(images):
        ccdsum.waitCacheChange(5000)
        sleep(0.1)
        sumimage.append(ccdsum.read())
        cid = ccdid.read()
        profilesx.append(ccdprofilex.read())
        profilesy.append(ccdprofiley.read())
        append_dataset("ccd/raw/image", Convert.reshape(GE_raw_array.take(), GE_Y, GE_X))
        append_dataset("ccd/processed/image", Convert.reshape(GE_roi_array.take(), GE_ROI_Y, GE_ROI_X))
        append_dataset("ccd/processed/spectrum", GE_Spectra.take())        
        print("pass:{}/{}, image:{}/{}, id:{}".format(j+1,passes,i+1,images,cid))
    sumpasses.append(sumimage)
    sumimage=[]
    print("sleeping...")
    sleep(30)
save_dataset("ccd/data", sumpasses, type = 'd', unsigned = False, features = None)
save_dataset("ccd/profilesx", profilesx, type = 'd', unsigned = False, features = None)
save_dataset("ccd/profilesy", profilesy, type = 'd', unsigned = False, features = None)
print("OK")
