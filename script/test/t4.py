## 3d plot

xdrange = 200
xstep = 20.0

ydrange = 100
ystep = 5.0

## positions
xpos = linspace(-xdrange, xdrange, xstep)
ypos = linspace(-ydrange, ydrange, ystep)
xdim = len(xpos)
ydim = len(ypos)
N = xdim*ydim

## plot data
xdata = [range(xdim)]*ydim
ydata = [[j]*xdim for j in range(ydim)]

#zxdata = [[0]*xdim*ydim
zxdata=[]
zydata=[]
for j in range(ydim):
    zex=[]
    zey=[]
    for i in range(xdim):
        zex.append(0)
        zey.append(0)
    zxdata.append(zex)
    zydata.append(zey)

## plot
mplot = plot([zxdata, zydata],["Grid X","Grid Y"])
mplot0 = mplot[0]
mplot1 = mplot[1]

#mplot0.getAxis(mplot0.AxisId.X).setRange(-xdrange-10,xdrange+10)
#mplot0.getAxis(mplot0.AxisId.Y).setRange(-ydrange,ydrange)
#mplot1.getAxis(mplot1.AxisId.X).setRange(-xdrange,xdrange)
#mplot1.getAxis(mplot1.AxisId.Y).setRange(-ydrange,ydrange)

zxplot = mplot[0].getSeries(0)
zyplot = mplot[1].getSeries(0)

for i in range(xdim):
    for j in range(ydim):
        zxdata[j][i]=i+j
        zydata[j][i]=-(i+j)
        zxplot.setData(zxdata, xdata, ydata)
        zyplot.setData(zydata, xdata, ydata)

        
