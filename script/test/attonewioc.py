## test of connection to attocube over night

Epics.destroy()
Epics.create()

## epics channels
posrbvpv = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
posrbvpv.setMonitored(True)
pospv = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
pospv.setMonitored(True)
cntpv = create_channel_device("ANC:CNT", type='d')
errpv = create_channel_device("ANC:ERR", type='d')
outpv = create_channel_device("ANC:POS", type='d')


print("Connecting...")
sleep(2)

cnt=0
err=0
lastpos = posrbvpv.read()
sleep(0.5)
print("Main loop")
print("start: {}".format(time.asctime()))
while True:
    pos=posrbvpv.read()
    if pos==lastpos:
        err+err+1
    laspos=pos    
    cnt=cnt+1
    outpv.write(pos)
    cntpv.write(cnt)
    errpv.write(err)
    sleep(1)

