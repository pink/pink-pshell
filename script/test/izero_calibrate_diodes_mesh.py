# record mesh arrays for diode bpm
def main():
    print("### Diode mesh test ###")
    print(time.asctime())
    # Range
    xdrange = 300.0
    xstep = 50.0
    ydrange = 100.0
    ystep = 10.0

   
    # exposure (sec)
    exposure = 1.0

    ## channels
    print("Creating channels...")
    xmotor = create_channel_device("PINK:SMA01:m10a.VAL", type='d')
    xmotor_rbv = create_channel_device("PINK:SMA01:m10a.RBV", type='d')
    xmotor_rbv.setMonitored(True)
    xmotor_dmov = create_channel_device("PINK:SMA01:m10a.DMOV", type='d')
    xmotor_dmov.setMonitored(True)
    ymotor = create_channel_device("PINK:SMA01:m9a.VAL", type='d')
    ymotor_rbv = create_channel_device("PINK:SMA01:m9a.RBV", type='d')
    ymotor_rbv.setMonitored(True)
    ymotor_dmov = create_channel_device("PINK:SMA01:m9a.DMOV", type='d')
    ymotor_dmov.setMonitored(True)
    xbeam = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV", type='d')
    xbeam.setMonitored(True)
    ybeam = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV", type='d')
    ybeam.setMonitored(True)
    cae2_acq = create_channel_device("PINK:CAE2:Acquire", type='i')
    cae2_acq.setMonitored(True)
    diode_d1_pv = create_channel_device("PINK:CAE2:Current1:MeanValue_RBV", type='d')
    diode_d1_pv.setMonitored(True)
    diode_d2_pv = create_channel_device("PINK:CAE2:Current2:MeanValue_RBV", type='d')
    diode_d2_pv.setMonitored(True)
    diode_d3_pv = create_channel_device("PINK:CAE2:Current3:MeanValue_RBV", type='d')
    diode_d3_pv.setMonitored(True)
    diode_d4_pv = create_channel_device("PINK:CAE2:Current4:MeanValue_RBV", type='d')
    diode_d4_pv.setMonitored(True)
    sleep(2)

    ## cae2 setup
    print("Setup CAE2...")
    caputq("PINK:CAE2:Acquire", 0)
    sleep(2)
    caput("PINK:CAE2:AcquireMode", 2)
    caput("PINK:CAE2:ValuesPerRead", 1000*exposure)
    caput("PINK:CAE2:AveragingTime", exposure)
    sleep(1)
    
    ## positions
    xpos = linspace(-xdrange, xdrange, xstep)
    ypos = linspace(-ydrange, ydrange, ystep)
    xdim = len(xpos)
    ydim = len(ypos)
    N = xdim*ydim
    loop_id=0
    set_status("{:d}/{:d}".format(loop_id, N))

    ## initial positions
    xini = xmotor_rbv.read()
    yini = ymotor_rbv.read()

    ## Plot
    xdata = [range(xdim)]*ydim
    ydata = [[j]*xdim for j in range(ydim)]
    zxdata=[]
    zydata=[]
    for j in range(ydim):
        zex=[]
        zey=[]
        for i in range(xdim):
            zex.append(0)
            zey.append(0)
        zxdata.append(zex)
        zydata.append(zey)

    mplot = plot([zxdata, zydata],["Grid X","Grid Y"])
    zxplot = mplot[0].getSeries(0)
    zyplot = mplot[1].getSeries(0) 
            
    ## arrays
    minpx = []
    minpy = []
    motorposx = []
    motorposy = []
    diode_d1 = []
    diode_d2 = []
    diode_d3 = []
    diode_d4 = []

    print("running...")    
    ## main loop
    for i in range(xdim):
        posx = xpos[i]
        xmotor.write(posx)
        xmotor_rbv.waitValueInRange(posx, 1.0, 10000)
        xmotor_dmov.waitValueInRange(1, 0.5, 10000)
        for j in range(ydim):
            posy = ypos[j]       
            ymotor.write(posy)
            ymotor_rbv.waitValueInRange(posy, 1.0, 10000)
            ymotor_dmov.waitValueInRange(1, 0.5, 10000)
            while(cae2_acq.read()>0): sleep(0.2)
            #cae2_acq.waitValueInRange(0, 0.5, 10000) 
            sleep(0.3)
            cae2_acq.write(1)
            xbeam.waitCacheChange(1000*int(exposure+20))
            motorposx.append(xmotor_rbv.read())
            motorposy.append(ymotor_rbv.read())
            minpx.append(xbeam.read())
            minpy.append(ybeam.read())
            diode_d1.append(diode_d1_pv.value)
            diode_d2.append(diode_d2_pv.value)
            diode_d3.append(diode_d3_pv.value)
            diode_d4.append(diode_d4_pv.value)
            loop_id += 1
            zxdata[j][i]=xbeam.value
            zydata[j][i]=ybeam.value
            zxplot.setData(zxdata, xdata, ydata)
            zyplot.setData(zydata, xdata, ydata)            
            set_status("{:d}/{:d}".format(loop_id, N))

    return

    ## end of loop
    print("Saving data...")
    save_dataset("data/xpos", xpos)
    save_dataset("data/ypos", ypos)
    save_dataset("data/minpx", minpx)
    save_dataset("data/minpy", minpy)
    save_dataset("data/motorX", motorposx)
    save_dataset("data/motorY", motorposy)
    save_dataset("data/diode_d1", diode_d1)
    save_dataset("data/diode_d2", diode_d2)
    save_dataset("data/diode_d3", diode_d3)
    save_dataset("data/diode_d4", diode_d4)
    
    ## push results
    print("Pushing data to epics...")
    caput("PINK:QBPM:xdata", array('d', minpx))
    caput("PINK:QBPM:ydata", array('d', minpy))
    caput("PINK:QBPM:zxdata", array('d', motorposx))
    caput("PINK:QBPM:zydata", array('d', motorposy))
    caput("PINK:QBPM:xdim", float(xdim))
    caput("PINK:QBPM:ydim", float(ydim))
    caput("PINK:QBPM:calibrate.PROC", 1)
    
    ## moving back to initial position
    xmotor.write(xini)
    ymotor.write(yini)

main()
print("Done\nOK")
    

    