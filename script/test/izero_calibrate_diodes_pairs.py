# Find linear fit coefs to diode displacement 
def main():
    # distance from middle (um)
    drange = 300.0
    # step size (um)
    step = 10.0
    # exposure (sec)
    exposure = 2.0
    
    ###########################
    ### calculate positions ###
    xm = caget("PINK:SMA01:m10a.RBV")
    ym = caget("PINK:SMA01:m9a.RBV")
    xpos = linspace(xm-drange, xm+drange, step)
    ypos = linspace(ym-drange, ym+drange, step)
    print("Middle positions are:")
    print("X: {:.3f}".format(xm))
    print("Y: {:.3f}".format(ym))

    ## channels
    print("Creating channels...")
    xmotor = create_channel_device("PINK:SMA01:m10a.VAL", type='d')
    xmotor_rbv = create_channel_device("PINK:SMA01:m10a.RBV", type='d')
    xmotor_rbv.setMonitored(True)
    xmotor_dmov = create_channel_device("PINK:SMA01:m10a.DMOV", type='d')
    xmotor_dmov.setMonitored(True)
    ymotor = create_channel_device("PINK:SMA01:m9a.VAL", type='d')
    ymotor_rbv = create_channel_device("PINK:SMA01:m9a.RBV", type='d')
    ymotor_rbv.setMonitored(True)
    ymotor_dmov = create_channel_device("PINK:SMA01:m9a.DMOV", type='d')
    ymotor_dmov.setMonitored(True)
    xbeam = create_channel_device("PINK:CAE2:PosX:MeanValue_RBV", type='d')
    #xbeam = create_channel_device("PINK:BP:posX", type='d')
    xbeam.setMonitored(True)
    ybeam = create_channel_device("PINK:CAE2:PosY:MeanValue_RBV", type='d')
    #ybeam = create_channel_device("PINK:BP:posY", type='d')
    ybeam.setMonitored(True)
    cae2_acq = create_channel_device("PINK:CAE2:Acquire", type='i')
    
    ## arrays
    alabel = ["X", "Y"]
    amid = [xm,ym]
    apos = [xpos, ypos]
    amotor = [xmotor, ymotor]
    amotor_rbv = [xmotor_rbv, ymotor_rbv]
    amotor_dmov = [xmotor_dmov, ymotor_dmov]
    abeampos = [xbeam, ybeam]
    adata = []
    
    ## cae2 setup
    print("Setup CAE2...")
    caputq("PINK:CAE2:Acquire", 0)
    sleep(2)
    caput("PINK:CAE2:AcquireMode", 2)
    caput("PINK:CAE2:ValuesPerRead", 1000*exposure)
    caput("PINK:CAE2:AveragingTime", exposure)
    sleep(1)

    ## plot setup
    plottitle="X diode bpm"
    plottitle2="Y diode bpm"            
    [p1, p2]=plot([None, None], [plottitle, plottitle2], title="Izero cal")
    p1.getAxis(p1.AxisId.X).setRange(-drange,drange)
    p2.getAxis(p2.AxisId.X).setRange(-drange,drange)
    p1.getAxis(p1.AxisId.X).setLabel("Position")
    p2.getAxis(p2.AxisId.X).setLabel("Position")
    p1.addSeries(LinePlotSeries("Fit"))
    p2.addSeries(LinePlotSeries("Fit"))
    aplot = [p1.getSeries(0), p2.getSeries(0)]
    aplotfit = [p1.getSeries(1), p2.getSeries(1)]
    #p1.getSeries(0).setData(xdata, ydata)
    #p2.getSeries(0).setData(xdata, ydata)
    
    ## calibration
    for i in range(2):
        label = alabel[i]
        mid = amid[i]
        mpos = apos[i]
        motor = amotor[i]
        motor_rbv = amotor_rbv[i]
        motor_dmov = amotor_dmov[i]
        beampos = abeampos[i]
        plotdata = aplot[i]

        print("Calibrating {} diodes...".format(label))
        xdata = []
        ydata = []
        xabs = []
        for pos in mpos:
            #xdata.append(pos)
            motor.write(pos)
            motor_rbv.waitValueInRange(pos, 1.0, 10000)
            motor_dmov.waitValueInRange(1, 0.5, 10000)
            xtemp = motor_rbv.read()
            xdata.append(mid-xtemp)
            xabs.append(xtemp)
            cae2_acq.write(1)
            beampos.waitCacheChange(1000*int(exposure+20))
            ydata.append(beampos.read())
            plotdata.setData(xdata, ydata)
            #sleep(1)
        adata.append([xdata,ydata, xabs])
        motor.write(mid)
        sleep(5)

    ## Plot, fit and save
    print("Done")
    print("Fit, save and plot...")
    save_dataset("x/xdata", adata[0][0])
    save_dataset("x/ydata", adata[0][1])
    save_dataset("x/xabs", adata[0][2])
    save_dataset("y/xdata", adata[1][0])
    save_dataset("y/ydata", adata[1][1])
    save_dataset("y/xabs", adata[1][2])
    
    ## fit data
    xfit = fit_polynomial(adata[0][1], adata[0][0], 1)
    xifit = fit_polynomial(adata[0][0], adata[0][1], 1)
    yfit = fit_polynomial(adata[1][1], adata[1][0], 1)
    yifit = fit_polynomial(adata[1][0], adata[1][1], 1)

    save_dataset("x/A", xfit[1])
    save_dataset("x/B", xfit[0])
    save_dataset("x/A_epics", xifit[1])
    save_dataset("x/B_epics", xifit[0])

    save_dataset("y/A", yfit[1])
    save_dataset("y/B", yfit[0])
    save_dataset("y/A_epics", yifit[1])
    save_dataset("y/B_epics", yifit[0])

    ## plot fit
    xdata = adata[0][0]
    ydata = [i*xfit[1]+xfit[0] for i in xdata]
    plotfit = aplotfit[0]
    plotfit.setData(xdata,ydata)
    save_dataset("x/yfit", ydata)

    xdata = adata[1][0]
    ydata = [i*yfit[1]+yfit[0] for i in xdata]
    plotfit = aplotfit[1]
    plotfit.setData(xdata,ydata)
    save_dataset("y/yfit", ydata)

    # Final print
    print("EPICS settings:")
    print("X axis A: {:.6f}".format(xifit[1]))
    print("X axis B: {:.6f}".format(xifit[0]))
    print("--------")
    print("Y axis A: {:.6f}".format(yifit[1]))
    print("Y axis B: {:.6f}".format(yifit[0]))
            
main()
print("OK")