#t8

title="Attocube"
xlabel="point"
ylabel="speed (um/s)"

p1h=plot(None, "Speed", title=title)
p1 = p1h.get(0)
p1.setTitle(title)
p1.getAxis(p1.AxisId.X).setLabel(xlabel)
p1.getAxis(p1.AxisId.Y).setLabel(ylabel)

#1
p1.addSeries(LinePlotSeries("avg"))
#2
p1.addSeries(LinePlotSeries("smooth"))
p1.setLegendVisible(True)

# channels
Epics.destroy()
Epics.create()
spd = create_channel_device("PINK:ANC01:ACT0:calcspeed")
spd.setMonitored(True)
avg = create_channel_device("PINK:ANC01:ACT0:speedavg")
avg.setMonitored(True)

X = []
Y0 = []
Y1 = []
Y2 = []
i=1

smooth = [avg.read()]
N = 4

##loop
log_enable.write(1.0)
while(int(log_enable.read())):
    avg.waitCacheChange(3000)

    smooth.append(avg.value)
    smooth = smooth[-N:]
    sm = mean(smooth)
    
    X.append(i)
    Y0.append(spd.value)
    #Y1.append(0.0)
    Y1.append(avg.read())
    Y2.append(sm)
    p1.getSeries(0).setData(X,Y0)
    p1.getSeries(1).setData(X,Y1)
    p1.getSeries(2).setData(X,Y2)
    i+=1    


#p1.getSeries(0).setData(points,s1)