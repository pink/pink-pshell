load_pvs=True

if load_pvs:
    atto_position = create_channel_device("PINK:ANC01:ACT0:POSITION", type='d')
    atto_position.setMonitored(True)
    target = create_channel_device("PINK:ANC01:ACT0:CMD:TARGET", type='d')
    target.setMonitored(True)
    freq = create_channel_device("PINK:ANC01:ACT0:CMD:FREQ", type='d')
    freq.setMonitored(True)
    freq_RBV = create_channel_device("PINK:ANC01:ACT0:CLC_FREQ", type='d')
    freq_RBV.setMonitored(True)
    speed_avg_RBV = create_channel_device("PINK:ANC01:ACT0:speedavg", type='d')
    speed_avg_RBV.setMonitored(True)
    atto_DMOV = create_channel_device("PINK:ANC01:ACT0:IN_TARGET", type='i')
    atto_DMOV.setMonitored(True)
    atto_FWD = create_channel_device("PINK:ANC01:ACT0:CMD:CONT_FWD", type='i')
    atto_BCK = create_channel_device("PINK:ANC01:ACT0:CMD:CONT_BCK", type='i')

