## functions
def mbusy():
    err=abs(target.take()-position.take())
    if err>10.0:
        return True
    else:
        return False

def waitmotor():
    while(mbusy()):
        sleep(1)   

## define speed
motor_speed = 1000.0
exp_range = 10000.0

## origin
pos_orig = position.take()
exptime0 = time.clock()

# move to initial point
freq.write(500.0)
target.write(10000.0+exp_range/2)
waitmotor()
sleep(2)

## measure average speed up
pos0=position.take()
time0=time.clock()
freq.write(motor_speed)
target.write(10000.0-exp_range/2)
waitmotor()
pos1=position.take()
time1=time.clock()
avg_speed = abs(pos1-pos0)/(time1-time0)
print("Target: {:.1f} um/s\t Avg: {:.1f} um/s".format(motor_speed, avg_speed))

sleep(3)

## measure average speed down
pos0=position.take()
time0=time.clock()
freq.write(motor_speed)
target.write(10000.0+exp_range/2)
waitmotor()
pos1=position.take()
time1=time.clock()
avg_speed = abs(pos1-pos0)/(time1-time0)
print("Target: {:.1f} um/s\t Avg: {:.1f} um/s".format(motor_speed, avg_speed))

# move to origin
freq.write(500.0)
target.write(pos_orig)
waitmotor()

exptime1 = time.clock()
exptime = exptime1-exptime0
print("Experiment done in {:.3f} seconds".format(exptime))

print("Done")
    
