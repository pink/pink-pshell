d = []
y = []
x = [i for i in range(10)]
fac=1
for i in range(50):
    dt = [100+(fac*i) for i in range(10)]
    fac=fac+2
    d.append(dt)
    y.append(fac)
    h = plot(d, xdata=x,ydata=y)[0]
    h.setColormap(Colormap.Flame)
    sleep(1)