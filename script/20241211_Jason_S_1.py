
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=11200, X1=13600, dX=600, Y0=4500, Y1=16500, passes=3, sample='s#1 FeS', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=23700, X1=26500, dX=600, Y0=4500, Y1=16500, passes=3, sample='s#2 CoS', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=36200, X1=38600, dX=600, Y0=4500, Y1=16500, passes=3, sample='s#3 NiS', linedelay=0)

#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=59600, X1=62100, dX=600, Y0=4500, Y1=12000, passes=4, sample='s#5 Na2SO4', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=71500, X1=73200, dX=500, Y0=4500, Y1=16500, passes=3, sample='s#6 CuS', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=83500, X1=86100, dX=600, Y0=4500, Y1=14500, passes=3, sample='s#7 ZnS', linedelay=0)


#scan.spot(detector.greateyes(), X=60000, Y=14500, exposure=3, images=60, sample='Ni foil, Ka12, Ap30')
#scan.spot(detector.greateyes(), X=60000, Y=14500, exposure=1, images=60, sample='Ni foil, Ka12')

#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveOPEN(14)


#pink.roi(detector.greateyes(), y=86, size=70)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=24000, X1=26200, dX=550, Y0=5000, Y1=14000, passes=8, sample='#2 Co SM liquid', linedelay=0)
#pink.roi(detector.greateyes(), y=80, size=70)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=36000, X1=38000, dX=500, Y0=5000, Y1=14000, passes=8, sample='#3 Co SM liquid', linedelay=0)
#ROI=80,70
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=47200, X1=49800, dX=600, Y0=5000, Y1=11000, passes=2, sample='#4 bottom Na2SO4', linedelay=0)
#pink.roi(detector.greateyes(), y=60, size=80)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=72100, X1=73900, dX=600, Y0=5000, Y1=16000, passes=8, sample='#6 Co+O2 liquid', linedelay=0)
#pink.roi(detector.greateyes(), y=74, size=70)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=84600, X1=86400, dX=600, Y0=5000, Y1=16500, passes=8, sample='#7 Co+O2 liquid', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.4, X0=23800, X1=26200, dX=550, Y0=5000, Y1=14000, passes=10, sample='#2 Co SM liquid CoKb13', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.4, X0=70500, X1=72000, dX=500, Y0=5000, Y1=14000, passes=10, sample='#6 Co + O2 liquid CoKb13', linedelay=0)
