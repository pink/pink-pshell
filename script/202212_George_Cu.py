
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=11300, X1=13000, dX=700, Y0=4050, Y1=16700, passes=20, sample='s#1 bpy2Cu2(OH)2', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=22600, X1=24200, dX=700, Y0=4050, Y1=16700, passes=20, sample='s#2 bpy2Cu2(OH)2 dup2', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=34200, X1=36300, dX=700, Y0=3550, Y1=15950, passes=12, sample='s#3 empty', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=46500, X1=48200, dX=700, Y0=4100, Y1=16000, passes=5, sample='s#4 CuCl', linedelay=0)

scan.spot(detector.eiger(), X=59000, Y=15000, exposure=3, images=60, sample='s#5 Zn foil Ka12')
scan.spot(detector.eiger(), X=59000, Y=13000, exposure=3, images=200, sample='s#5 Cu foil Kb')
scan.spot(detector.eiger(), X=59000, Y=7600, exposure=3, images=100, sample='s#5 Ho foil La')


scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=70700, X1=71400, dX=700, Y0=4100, Y1=16600, passes=5, sample='s#6 water', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=81700, X1=83200, dX=700, Y0=4100, Y1=16600, passes=20, sample='s#7 bpyCu(OH)2 dup2', linedelay=0)


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveCLOSE(12)