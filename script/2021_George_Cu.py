scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=58000, X1=60000, dX=780.0, Y0=3200, Y1=16900, passes=25, sample='S#5 TMGtren-Cu-superoxo', linedelay=0)

caput("PINK:ANC01:ACT0:POSITION", 10000.0)
sleep(5)

scan.spot(detector.eiger(), X=23500, Y=14500, exposure=5, images=60, sample='s#2 Cu foil Kb')
scan.spot(detector.eiger(), X=23500, Y=7400, exposure=10, images=60, sample='s#2 Zn foil Kb')

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=10500, X1=13000, dX=780.0, Y0=3200, Y1=16900, passes=8, sample='S#1 CuCl', linedelay=0)


