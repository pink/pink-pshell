
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=10200, X1=12200, dX=600, Y0=4500, Y1=16000, passes=6, sample='s#1_W1_Co(I)-N2-Co(I)-sq', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=21000, X1=22800, dX=600, Y0=4500, Y1=16000, passes=3, sample='s#2_H1_Co(II)-Cl-NacNac-granded', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=34000, X1=36000, dX=600, Y0=4500, Y1=16000, passes=6, sample='s#3_H2_Co(0)-N2-Co(0)-NacNac-2nd-batch', linedelay=0)
#scan.spot(detector.eiger(), X=46500, Y=12500, exposure=2, images=150, sample='s4 Co foil, Kb')
#scan.spot(detector.eiger(), X=46500, Y=10000, exposure=5, images=60, sample='s4 Ni Ka')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=57200, X1=59500, dX=700, Y0=4500, Y1=16000, passes=6, sample='s#5_K1_Co(I)-reference', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=67800, X1=70400, dX=700, Y0=4500, Y1=16000, passes=6, sample='s#6_Co(I)-N2-Co(I)-linear dilute', linedelay=0)




#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=69800, X1=72200, dX=700, Y0=4500, Y1=16000, passes=3, sample='s#6_Co(OH)2', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=58200, X1=60500, dX=700, Y0=4500, Y1=16000, passes=3, sample='s#5_CoF2', linedelay=0)
scan.spot(detector.eiger(), X=47000, Y=14000, exposure=2, images=150, sample='s4 Co foil, Kb')
scan.spot(detector.eiger(), X=47000, Y=12600, exposure=5, images=60, sample='s4 Ni Ka')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=35100, X1=37000, dX=600, Y0=4500, Y1=16000, passes=3, sample='s#3_H1_NacNacCoCl', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=22800, X1=25000, dX=600, Y0=4500, Y1=16000, passes=10, sample='s#2_Co(I)-N2-T', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=22800, X1=25000, dX=600, Y0=4500, Y1=16000, passes=10, sample='s#2_Co(I)-N2-T', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=22800, X1=25000, dX=600, Y0=4500, Y1=16000, passes=16, sample='s#2_Co(I)-N2-T', linedelay=0)