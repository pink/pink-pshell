#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=83300, X1=86100, dX=700, Y0=3400, Y1=16500, passes=3, sample='s#7 60; NiPdNiCl2(dpa)4 (diluted in wax)', linedelay=0)

#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=72200, X1=75200, dX=750, Y0=3400, Y1=16500, passes=3, sample='s#6 72; Pd(NHC)2(O2)2', linedelay=0)


#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=60200, X1=63300, dX=750, Y0=3500, Y1=16500, passes=3, sample='s#5 71; Pd(NHC)2(O2)', linedelay=0)

#scan.spot(detector.greateyes(), X=49800, Y=14600, exposure=2, images=150, sample='s#4 Pd foil')
#scan.spot(detector.greateyes(), X=49800, Y=13000, exposure=2, images=100, sample='s#4 Fe foil, Ka2')
#scan.zigzag_absolute(detector.greateyes(), exposure=3, X0=49200, dX=700, Xpoints=3, Y0=4500, dY=100, Ypoints=22, passes=1, sample='s#4 Fe2O3 Ka2', linedelay=0, moveback=0)

#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=36400, X1=39000, dX=650, Y0=3500, Y1=16500, passes=3, sample='s#3 58; CoPdCoCl2(dpa)4 ', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=25000, X1=27900, dX=700, Y0=3500, Y1=16500, passes=3, sample='s#2 56; FePdFeCl2(dpa)4', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=13400, X1=16200, dX=700, Y0=3500, Y1=16500, passes=3, sample='s#1 PdCl', linedelay=0)

scan.spot(detector.greateyes(), X=38000, Y=3300, exposure=3, images=100, sample='s#3 58; CoPdCoCl2(dpa)4')
scan.spot(detector.greateyes(), X=62000, Y=3300, exposure=3, images=100, sample='s#5 71; Pd(NHC)2(O2')
scan.spot(detector.greateyes(), X=74000, Y=3300, exposure=3, images=100, sample='s#6 72; Pd(NHC)2(O2)2')
scan.spot(detector.greateyes(), X=85000, Y=3300, exposure=3, images=100, sample='s#7 60; NiPdNiCl2(dpa)4 (diluted in wax')


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveOPEN(14)
#caput("PINK:GEYES:cam1:Temperature", 20)
#pink.gap(9.0)

