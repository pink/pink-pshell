scan.spot(detector.eiger(), X=38000, Y=-500, exposure=10, images=60, sample='s#1up Ta foil La12')
scan.spot(detector.eiger(), X=38000, Y=2400, exposure=2, images=120, sample='s#1mid Ni foil Kb')
scan.spot(detector.eiger(), X=38000, Y=7200, exposure=2, images=120, sample='s#1down Tb foil La12')
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=21100, X1=23400, dX=700, Y0=-6800, Y1=8100, passes=2, sample='s#2 NiPPh3Cl', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=4000, X1=6100, dX=700, Y0=-6800, Y1=8100, passes=2, sample='s#3 NiPPh3Br', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=-13700, X1=-11600, dX=700, Y0=-6800, Y1=8100, passes=4, sample='s#4 NiPPh2ethCl', linedelay=0)
pink.shutter_hard_CLOSE()
#caput("PINK:PLCVAC:V19close", 1)
#caput("PINK:PLCVAC:V26close", 1)
pink.valveCLOSE(19)
pink.valveCLOSE(26)


alert.message("End of the script")
pink.scanAlarm()

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveCLOSE(12)