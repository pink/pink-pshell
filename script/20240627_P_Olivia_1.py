#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.3, X0=94600, X1=95600, dX=500., Y0=4250, Y1=15950, passes=1, sample='#8 NaH2PO4 old Zac 2023', linedelay=15)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=83100, X1=85200, dX=500., Y0=6650, Y1=13450, passes=1, sample='#7 CoPSCO', linedelay=15)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=47600, X1=49600, dX=500., Y0=5050, Y1=15950, passes=1, sample='#4  NaH2PO4', linedelay=15)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=35700, X1=38300, dX=550., Y0=5050, Y1=16450, passes=1, sample='#3 CoPSH', linedelay=15)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=23800, X1=26200, dX=550., Y0=4850, Y1=16450, passes=1, sample='#2 KCoPS', linedelay=15)


#sim.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=83100, X1=85200, dX=500., Y0=6500, Y1=13500, passes=1, sample='#7 CoPSCO', linedelay=10)

#sim.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=71200, X1=73500, dX=500., Y0=6500, Y1=16300, passes=1, sample='#6  KCotrpbdt', linedelay=10)

#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=59200, X1=62000, dX=500., Y0=5000, Y1=16400, passes=1, sample='#5  Cotrpbdt', linedelay=10)

#scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=35700, X1=38300, dX=550., Y0=5000, Y1=16500, passes=1, sample='#3 CoPSH', linedelay=15)

#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=23800, X1=26200, dX=550., Y0=4800, Y1=16500, passes=1, sample='#2 KCoPS', linedelay=15)

scan.spot(detector.eiger(), X=12000, Y=14000, exposure=2, images=300, sample='Co foil, Kb13 ')
scan.spot(detector.eiger(), X=12000, Y=11000, exposure=5, images=60, sample='Ni foil, Ka12 ')

#sim.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=24500, X1=26200, dX=550., Y0=4150, Y1=16600, passes=8, sample='#2 FePS', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=35500, X1=37800, dX=550., Y0=4500, Y1=16600, passes=10, sample='#3 1 CotrpbdtBF4', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=59200, X1=61700, dX=550., Y0=5500, Y1=16400, passes=16, sample='#5 Co(bdt)2H', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=71200, X1=73400, dX=550., Y0=4500, Y1=16600, passes=10, sample='#6 2 CoPS', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=83700, X1=86400, dX=550., Y0=4500, Y1=16600, passes=10, sample='#7 CoPSCO', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=83700, X1=86400, dX=550., Y0=4500, Y1=16600, passes=5, sample='#7 CoPSCO', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=71200, X1=73400, dX=550., Y0=4500, Y1=16600, passes=5, sample='#6 2 CoPS', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=59200, X1=61700, dX=550., Y0=5500, Y1=16400, passes=10, sample='#5 Co(bdt)2H', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=35500, X1=37800, dX=550., Y0=4500, Y1=16600, passes=5, sample='#3 1 CotrpbdtBF4', linedelay=0)

scan.spot(detector.eiger(), X=12000, Y=14000, exposure=2, images=300, sample='Co foil, Kb13 ')
scan.spot(detector.eiger(), X=12000, Y=11000, exposure=5, images=60, sample='Ni foil, Ka12 ')

caput("PINK:ANC01:ACT0:CMD:TARGET",10000)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)