scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=11300, X1=13000, dX=750, Y0=8000, Y1=16000, passes=12, sample='s01, Ru1 (Ru(II) monomer in TFE, 30 mM', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=23800, X1=25800, dX=780, Y0=5000, Y1=13000, passes=12, sample='s02, Ru2 (Ru(II) dimer in TFE, 15 mM', linedelay=0)

#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=47000, X1=49000, dX=750, Y0=5000, Y1=16900, passes=10, sample='s04, Ru15 (Ru(II) oligomer in TFE, 10 mM', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=59000, X1=61000, dX=750, Y0=5000, Y1=16000, passes=8, sample='s05, Ru15Ce15 (Ru(III) oligomer in TFE, 10 mM', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=70500, X1=72100, dX=750, Y0=5000, Y1=16900, passes=8, sample='s06, Ru15Ce30 (Ru(IV) oligomer in TFE, 10 mM', linedelay=0)

#alert.message("measurement done")
print("########## DONE ##########")
#pink.shutter_hard_CLOSE()