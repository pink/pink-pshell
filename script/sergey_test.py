#scan.spot(detector.eiger(), X=24000, Y=15000, exposure=5, images=100, sample='Fe foil Kb, E=7130eV')
scan.spot(detector.eiger(), X=24000, Y=6000, exposure=5, images=100, sample='Fe2O3 Kb, E=7130eV')
scan.spot(detector.eiger(), X=24000, Y=15000, exposure=5, images=100, sample='Fe foil Kb, E=7130eV')
scan.spot(detector.eiger(), X=24000, Y=6000, exposure=5, images=100, sample='Fe2O3 Kb, E=7130eV')
scan.spot(detector.eiger(), X=36000, Y=15000, exposure=5, images=100, sample='Ferricyanidel Kb, E=7130eV')

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)
