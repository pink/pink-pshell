#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=83800, X1=86500, dX=750, Y0=3500, Y1=16500, passes=3, sample='s#7 62; CuPdCuCl2(dpa)4', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=74800, dX=700, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=74800, dX=700, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=74800, dX=700, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)

scan.spot(detector.greateyes(), X=50000, Y=15200, exposure=2, images=150, sample='s#4 Pd foil')
scan.spot(detector.greateyes(), X=50000, Y=13800, exposure=2, images=100, sample='s#4 Fe foil, Ka2')
scan.zigzag_absolute(detector.greateyes(), exposure=3, X0=49000, dX=700, Xpoints=3, Y0=4700, dY=100, Ypoints=22, passes=1, sample='s#4 Fe2O3 Ka2', linedelay=0, moveback=0)

scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=75000, dX=600, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=75000, dX=600, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=75000, dX=600, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=72500, X1=74800, dX=700, Y0=4500, Y1=16500, passes=4, sample='s#6 70; Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs', linedelay=0)

#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=60700, X1=63300, dX=750, Y0=3500, Y1=16500, passes=3, sample='s#5 69;  Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=36500, X1=38500, dX=650, Y0=3500, Y1=16500, passes=3, sample='s#3 68; Reduced 0.5% Pd / 0.5% Au NPs', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=24600, X1=27000, dX=750, Y0=3500, Y1=16500, passes=3, sample='s#2 67; Reduced 1% Pd NPs', linedelay=0)
#scan.spot(detector.greateyes(), X=26000, Y=3500, exposure=5, images=200, sample='s#2 damage 67; Reduced 1% Pd NPl')
#scan.spot(detector.greateyes(), X=26000, Y=16550, exposure=5, images=200, sample='s#2 damage 67; Reduced 1% Pd NPl')

#scan.spot(detector.greateyes(), X=37000, Y=3500, exposure=5, images=200, sample='s#3 68; damage Reduced 0.5% Pd / 0.5% Au NPs')
#scan.spot(detector.greateyes(), X=37000, Y=16550, exposure=5, images=200, sample='s#3 68; damage Reduced 0.5% Pd / 0.5% Au NPs')

#scan.spot(detector.greateyes(), X=62000, Y=3500, exposure=5, images=200, sample='s#5 69; damage Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs')
#scan.spot(detector.greateyes(), X=62000, Y=16550, exposure=5, images=200, sample='s#5 69; damage Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs')

#scan.spot(detector.greateyes(), X=73500, Y=3500, exposure=5, images=200, sample='s#5 70; damage Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs')
#scan.spot(detector.greateyes(), X=73500, Y=16550, exposure=5, images=200, sample='s#5 70; damage Reduced 0.33% Pd / 0.33% Au / 0.33% Pt NPs')

#scan.spot(detector.greateyes(), X=85000, Y=3500, exposure=5, images=200, sample='s#5 62; damage CuPdCuCl2(dpa)4')
#scan.spot(detector.greateyes(), X=85000, Y=16550, exposure=5, images=200, sample='s#5 62; damage CuPdCuCl2(dpa)4')

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)