#T=80K
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=25200, X1=25700, dX=650, Y0=4600, Y1=16700, passes=1, sample='s#2 FF-X1, T=130K', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=36400, X1=37000, dX=650, Y0=4600, Y1=16700, passes=1, sample='s#3 FF-X2, T=130K', linedelay=0)
scan.spot(detector.eiger(), X=47000, Y=13800, exposure=1, images=120, sample='s4 top Co foil Ka12')
scan.spot(detector.eiger(), X=47000, Y=15800, exposure=2, images=100, sample='s4 top Fe foil Kb')
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=60000, X1=60400, dX=650, Y0=4600, Y1=16700, passes=1, sample='s#5 FF-X3, T=130K', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=71000, X1=72300, dX=700, Y0=4400, Y1=16700, passes=1, sample='s#6 FF-X4, T=130K', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=82600, X1=83000, dX=650, Y0=4400, Y1=16700, passes=1, sample='s#7 aminoferrocene, T=130K', linedelay=0)