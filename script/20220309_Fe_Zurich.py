
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=21500, X1=24000, dX=700, Y0=-5000, Y1=8100, passes=2, sample='s#2 Zac sample', linedelay=0)
scan.spot(detector.eiger(), X=40500, Y=-4200, exposure=5, images=120, sample='s#1up Fe foil Kb')
scan.spot(detector.eiger(), X=40500, Y=1500, exposure=5, images=20, sample='s#1mid Co foil Ka')
#scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=4000, X1=6200, dX=700, Y0=-6800, Y1=8100, passes=3, sample='s#3 FeSO4', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=-14100, X1=-11500, dX=700, Y0=-6800, Y1=8100, passes=3, sample='s#4 Fe2TiO5', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=1, X0=-14500, X1=-13000, dX=700, Y0=-6800, Y1=8100, passes=1, sample='s#4 Fe2TiO5', linedelay=0)
pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveCLOSE(12)