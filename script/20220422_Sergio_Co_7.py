#scan.spot(detector.eiger(), X=93000, Y=12500, exposure=1, images=20, sample='Ni foil, Ka')
scan.spot(detector.eiger(), X=94000, Y=13000, exposure=2, images=120, sample='Co foil, Kb')
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=34700, X1=37500, dX=750, Y0=13500, Y1=16700, passes=3, sample='s#3top Co(OH)2', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=82400, X1=85000, dX=750, Y0=4100, Y1=16900, passes=4, sample='s#7 Fe(IV)CCPh CS05132', linedelay=0)