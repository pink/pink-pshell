#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=36400, X1=39000, dX=600., Y0=3800, Y1=16700, passes=2, sample='s#3 Li14Cr2N8O spent', linedelay=0)

#scan.spot(detector.eiger(), X=48000, Y=9000, exposure=1, images=60, sample='Mn foil, Ka12')
#scan.spot(detector.eiger(), X=48000, Y=12000, exposure=2, images=150, sample='Cr foil, Kb13')
#scan.spot(detector.eiger(), X=48000, Y=14000, exposure=1, images=60, sample='Gd foil, La12')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.4, X0=60400, X1=63000, dX=600., Y0=3800, Y1=16700, passes=1, sample='s#5 CrO3', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.4, X0=72400, X1=75100, dX=600., Y0=3800, Y1=16700, passes=1, sample='s#6 K2CrO7', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.4, X0=84400, X1=87000, dX=600., Y0=3800, Y1=16700, passes=1, sample='s#7 KCr(SO4)2 12H2O', linedelay=0)


scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=24700, X1=27200, dX=600., Y0=3850, Y1=16650, passes=1, sample='s#2 Li14Cr2N8O fresh', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=10, sample='Darkm image')

scan.continuous(detector.greateyes(), det_exposure=4, sample_exposure=0.2, X0=24700, X1=27200, dX=600., Y0=3850, Y1=16650, passes=1, sample='s#2 Li14Cr2N8O fresh', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=4, images=10, sample='Darkm image')

scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=24700, X1=27200, dX=600., Y0=3850, Y1=16650, passes=1, sample='s#2 Li14Cr2N8O fresh', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=10, sample='Darkm image')