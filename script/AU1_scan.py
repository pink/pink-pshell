from mathutils import *

n=35
diode=[0.0]*n
beam_prof=[0.0]*n
slit_y=[0.0]*n
print(diode)
y1=-1.4
for i in range(n):
    slit_y[i]=y1+i*0.1 #AU1
    caput("PINK:AU1:centerY", slit_y[i]) #AU3 slit, left write
    sleep(2)
    #caget("PINK:PHY:AxisF.RBV")  #JJ slit, left read
    #caget("u171pgm1:PH_4_GET")  #AU2 slit, left read
    #caget("AUY01U112L:rdPosM3")  #AU3 slit, left read
    print(slit_y[i])
    #diode[i]=caget("PINK:CAE1:Current3:MeanValue_RBV") #diode BPM3
    # diode[i]=caget("PINK:CAE1:Current1:MeanValue_RBV") #diode diag chamber
    diode[i]=caget("EMILEL:Keithley09:rdCur") #I0
    
    print(diode[i])
    
    plot(diode, title = "slit scan", xdata = slit_y)
sleep(2)
beam_prof=deriv(diode)
#plot(beam_prof, title = "slit scan", xdata = slit_x)
#[p1, ]=plot([None, None], ["diode", "derivative"], title="Blade scan")  
#p1.getSeries(0).setData(slit_y, diode)
