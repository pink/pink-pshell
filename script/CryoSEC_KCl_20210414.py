
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=71800, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#6 cell', linedelay=0)

scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=60200, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#5 8um kapton', linedelay=0)
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=59500, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#5 8um kapton', linedelay=0)

scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=47300, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#4 KCl10mM 8um kapton', linedelay=0)
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=49000, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#4 KCl10mM 8um kapton', linedelay=0)

scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=35800, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#3 KCl1mM, 8um kapton', linedelay=0)
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=36800, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#3 KCl1mM, 8um kapton', linedelay=0)

scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=24400, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#2 H20, 8um kapton', linedelay=0)
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=25300, dX=700, Xpoints=1, Y0=4500, dY=50, Ypoints=220, passes=1, sample='s#2 H20, 8um kapton', linedelay=0)

print("########## DONE ##########")
