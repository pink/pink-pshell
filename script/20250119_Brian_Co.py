#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=83100, X1=85700, dX=650, Y0=3500, Y1=13000, passes=6, sample='s#7 CoMg', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=71400, X1=73500, dX=650, Y0=3500, Y1=16750, passes=6, sample='s#6 CoFe', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=59200, X1=61800, dX=650, Y0=3500, Y1=16000, passes=6, sample='s#5 CoCo', linedelay=0)
#scan.spot(detector.eiger(), X=48000, Y=12000, exposure=2, images=200, sample='s#4 Co foil, Kb')
#scan.spot(detector.eiger(), X=48000, Y=10800, exposure=3, images=100, sample='s#4 Ni foil, Ka')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=35400, X1=38200, dX=650, Y0=3500, Y1=16750, passes=6, sample='s#3 CoNi', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=23800, X1=26600, dX=650, Y0=3500, Y1=16750, passes=6, sample='s#2 CoZn', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=23800, X1=26600, dX=650, Y0=4000, Y1=16600, passes=2, sample='s#2 CoZn', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=35400, X1=38200, dX=650, Y0=4000, Y1=16600, passes=3, sample='s#3 CoNi', linedelay=0)
scan.spot(detector.eiger(), X=48000, Y=12000, exposure=2, images=100, sample='s#4 Co foil, Kb')
scan.spot(detector.eiger(), X=48000, Y=10800, exposure=3, images=60, sample='s#4 Ni foil, Ka')
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=59200, X1=61800, dX=650, Y0=4000, Y1=16000, passes=3, sample='s#5 CoCo', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=71400, X1=73500, dX=650, Y0=4000, Y1=16600, passes=3, sample='s#6 CoFe', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.3, X0=83100, X1=85700, dX=650, Y0=4000, Y1=13000, passes=3, sample='s#7 CoMg', linedelay=0)