#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=34700, X1=37500, dX=750, Y0=13000, Y1=16900, passes=4, sample='s#3top Co(OH)2', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=34700, X1=37500, dX=750, Y0=4000, Y1=8700, passes=4, sample='s#3down CoCl4', linedelay=0)
#scan.spot(detector.eiger(), X=36000, Y=3900, exposure=5, images=60, sample='s#3down damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=11200, X1=13500, dX=700, Y0=5000, Y1=16900, passes=6, sample='s#1 3-Mes', linedelay=0)
#scan.spot(detector.eiger(), X=12000, Y=3900, exposure=5, images=120, sample='s#1 damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=23200, X1=25500, dX=750, Y0=4000, Y1=16900, passes=2, sample='s#2 4-Mes', linedelay=0)
#scan.spot(detector.eiger(), X=24000, Y=3900, exposure=5, images=60, sample='s#2 damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=46900, X1=49500, dX=750, Y0=13000, Y1=16900, passes=25, sample='s#4top CoI4', linedelay=0)
#scan.spot(detector.eiger(), X=48000, Y=3900, exposure=5, images=60, sample='s#4top damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=46900, X1=49500, dX=750, Y0=4000, Y1=8700, passes=4, sample='s#4bottom CoBr4', linedelay=0)
#scan.spot(detector.eiger(), X=48000, Y=3900, exposure=5, images=60, sample='s#4bottom damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=58700, X1=61000, dX=750, Y0=4000, Y1=16900, passes=2, sample='s#5 CoPc', linedelay=0)
#scan.spot(detector.eiger(), X=60000, Y=3900, exposure=5, images=60, sample='s#5 damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=71000, X1=73700, dX=750, Y0=13000, Y1=16900, passes=6, sample='s#6top C_Co(H2L)', linedelay=0)
#scan.spot(detector.eiger(), X=72000, Y=3900, exposure=5, images=60, sample='s#6top damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=71000, X1=73700, dX=750, Y0=4000, Y1=8700, passes=6, sample='s#6bottom D_Co(L)', linedelay=0)
#scan.spot(detector.eiger(), X=72000, Y=3900, exposure=5, images=60, sample='s#6bottom damage, ')

scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=81900, X1=84100, dX=700, Y0=4000, Y1=16900, passes=6, sample='s#7 E_Co(H3L)_CoCl4', linedelay=0)
scan.spot(detector.eiger(), X=83000, Y=3900, exposure=5, images=120, sample='s#7 damage, ')

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=71000, X1=73700, dX=750, Y0=4000, Y1=8700, passes=5, sample='s#6bottom D_Co(L)', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=46900, X1=49500, dX=750, Y0=13000, Y1=16900, passes=25, sample='s#4top CoI4', linedelay=0)


#scan.spot(detector.eiger(), X=93000, Y=12500, exposure=1, images=20, sample='Ni foil, Ka')
#scan.spot(detector.eiger(), X=93000, Y=14000, exposure=2, images=150, sample='Co foil, Kb')

alert.message("End of the script")
pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14) 