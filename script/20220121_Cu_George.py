#Energy calibration
scan.spot(detector.eiger(), X=47600, Y=13500, exposure=5, images=300, sample='s#4 Cu foil Kb')
#scan.spot(detector.eiger(), X=47600, Y=15500, exposure=10, images=30, sample='s#4 Zn foil Kb')
#scan.spot(detector.eiger(), X=47600, Y=8000, exposure=5, images=60, sample='s#4 Ho foil La')
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=2, X0=70700, X1=73300, dX=800, Y0=4750, Y1=16350, passes=1, sample='s#6 CuCl', linedelay=0)
#Measurement
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=2, X0=58500, X1=61500, dX=750, Y0=4800, Y1=15950, passes=2, sample='s#5 KCuF3 ', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=2, X0=34500, X1=37500, dX=750, Y0=4800, Y1=16450, passes=3, sample='s#3 Cs2CuF6', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=22650, X1=25000, dX=750, Y0=4800, Y1=16450, passes=14, sample='s#2(TMG3tren)CuO2 10 mM)', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=11550, X1=13700, dX=710, Y0=4800, Y1=16450, passes=14, sample='s#1 (TMG3tren)CuO2 10 mM', linedelay=0)

print("########## DONE ##########")
alert.message('Measurement done')
pink.shutter_hard_CLOSE()
