
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=10800, X1=13800, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#1_H3poat_B', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=22800, X1=25300, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#2_PdPPh2ethCl2_B', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=34500, X1=37000, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#3_PPh2eth_B', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=46600, X1=48700, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#4_NaH2PO4_B', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=58300, X1=60700, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#5_PPh2eth_C', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=71100, X1=72600, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#6_PPh2pro_B', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=82300, X1=84300, dX=700, Y0=4000, Y1=16600, passes=4, sample='s#7_PPh2pro_C', linedelay=0)

#pink.shutter_hard_CLOSE()