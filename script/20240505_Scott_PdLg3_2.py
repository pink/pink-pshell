scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=25200, X1=28000, dX=600., Y0=4200, Y1=16200, passes=6, sample='s#2 67; Reduced 1% Pd NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=36500, X1=39000, dX=600, Y0=4200, Y1=16200, passes=6, sample='s#3 68; Reduced 0.5% Pd / 0.5% Au NPs', linedelay=0)

scan.spot(detector.greateyes(), X=50000, Y=16200, exposure=10, images=300, sample='Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=14800, exposure=2, images=200, sample='Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=50000, Y=13500, exposure=5, images=200, sample='Pd foil with kapton, Lg3')
scan.zigzag_absolute(detector.greateyes(), exposure=10, X0=48900, dX=500, Xpoints=5, Y0=4500, dY=60, Ypoints=40, passes=1, sample='Fe2O3 Kb13', linedelay=10, moveback=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=60400, X1=63000, dX=650, Y0=4200, Y1=16200, passes=6, sample='s#5 69;  Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=83800, X1=86500, dX=650, Y0=4200, Y1=16200, passes=6, sample='s#7 62; CuPdCuCl2(dpa)4', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=60400, X1=63000, dX=650, Y0=4200, Y1=16200, passes=4, sample='s#5 69;  Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs', linedelay=0)

scan.spot(detector.greateyes(), X=50000, Y=16200, exposure=10, images=300, sample='Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=13500, exposure=5, images=100, sample='Pd foil with kapton, Lg3')

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=25200, X1=28000, dX=600., Y0=4200, Y1=16200, passes=4, sample='s#2 67; Reduced 1% Pd NPs', linedelay=0)

pink.shutter_hard_CLOSE()
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=6, sample='dark noise, 5s')
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)