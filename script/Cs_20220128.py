scan.spot(detector.greateyes(), X=40800, Y=3000, exposure=1, images=120, sample='Pd La12, 7300eV')

scan.spot(detector.greateyes(), X=-12000, Y=2000, exposure=5, images=120, sample='s#4 CsF, Yaw=-1800, Roll=500, L=116000, 7300eV')
scan.spot(detector.greateyes(), X=-12000, Y=4000, exposure=5, images=120, sample='s#4 CsF, Yaw=-1800, Roll=500, L=116000, 7300eV')
scan.spot(detector.greateyes(), X=-12000, Y=6000, exposure=5, images=120, sample='s#4 CsF, Yaw=-1800, Roll=500, L=116000, 7300eV')

scan.spot(detector.greateyes(), X=4500, Y=2000, exposure=5, images=120, sample='s#3 CsOH, Yaw=-1800, Roll=500, L=116000, 7300eV')
scan.spot(detector.greateyes(), X=4500, Y=4000, exposure=5, images=120, sample='s#3 CsOH, Yaw=-1800, Roll=500, L=116000, 7300eV')
scan.spot(detector.greateyes(), X=4500, Y=6000, exposure=5, images=120, sample='s#3 CsOH, Yaw=-1800, Roll=500, L=116000, 7300eV')
