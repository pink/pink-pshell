scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=-13500, X1=-11000, dX=750, Y0=-4400, Y1=8250, passes=1, sample='s#4 Coen3 std', linedelay=0)
scan.spot(detector.eiger(), X=5000, Y=1000, exposure=2, images=100, sample='s#3 Co foil Kb')
scan.spot(detector.eiger(), X=5000, Y=5600, exposure=2, images=100, sample='s#3 Ni foil Ka')
scan.continuous(detector.eiger(), det_exposure=1, sample_exposure=1, X0=38700, X1=41000, dX=750, Y0=2500, Y1=6000, passes=10, sample='s#1 Co(OH)2 2hr', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=1, sample_exposure=0.2, X0=21700, X1=24200, dX=750, Y0=-4400, Y1=8250, passes=40, sample='s#2 Co7', linedelay=0)

pink.valveCLOSE(11)
pink.valveCLOSE(19)
pink.valveCLOSE(26)