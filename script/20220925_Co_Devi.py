scan.spot(detector.eiger(), X=22000, Y=6000, exposure=1, images=20, sample='s#3 Ni foil Ka12')
scan.spot(detector.eiger(), X=22000, Y=1500, exposure=3, images=200, sample='s#3 Co foil Kb')

scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=-13800, X1=-11500, dX=750, Y0=-6600, Y1=8100, passes=5, sample='s#1 341 CoNCN/CNT', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=3800, X1=6100, dX=700, Y0=-6000, Y1=8100, passes=5, sample='s#2 336 CoNCN/CNT', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=39000, X1=41200, dX=700, Y0=-6600, Y1=8100, passes=5, sample='s#4 317 CoNCN/CNT', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=-13800, X1=-11500, dX=750, Y0=-6600, Y1=8100, passes=6, sample='s#1 341 CoNCN/CNT', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=3800, X1=6100, dX=700, Y0=-6000, Y1=8100, passes=6, sample='s#2 336 CoNCN/CNT', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=39000, X1=41200, dX=700, Y0=-6600, Y1=8100, passes=6, sample='s#4 317 CoNCN/CNT', linedelay=0)

pink.shutter_hard_CLOSE()