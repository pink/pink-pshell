scan.spot(detector.greateyes(), X=22500, Y=13600, exposure=1, images=10, sample='s#2 Au foil, Mb')
scan.spot(detector.greateyes(), X=22500, Y=15000, exposure=3, images=200, sample='s#2 Pt foil Mb')
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=1, X0=34500, X1=35500, dX=500, Y0=16000, Y1=5000, passes=1, sample='s#3 Pt 200mM', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=1, X0=46000, X1=47000, dX=500, Y0=16000, Y1=5000, passes=5, sample='s#4 Pt 10mM', linedelay=0)