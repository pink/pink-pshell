#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=66100, X1=68900, dX=700, Y0=3050, Y1=16750, passes=2, sample='s#6 FeCl2', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=54300, X1=57000, dX=650, Y0=3050, Y1=16750, passes=2, sample='s#5 Fc+', linedelay=0)

#scan.spot(detector.eiger(), X=43000, Y=15500, exposure=2, images=300, sample='s4 top Fe foil Kb')
#scan.spot(detector.eiger(), X=43000, Y=13200, exposure=1, images=120, sample='s4 top Co foil Ka12')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=42400, X1=45500, dX=700, Y0=3100, Y1=8800, passes=2, sample='s#4 bottom Fe2O3', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=30700, X1=33500, dX=700, Y0=3100, Y1=16700, passes=2, sample='s#3 Fc', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=18900, X1=21700, dX=700, Y0=3100, Y1=16700, passes=2, sample='s#2 Fp2', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=7200, X1=10000, dX=700, Y0=3100, Y1=16700, passes=2, sample='s#1 NaFp', linedelay=0)

#Second run
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=30700, X1=33400, dX=650, Y0=4300, Y1=16700, passes=2, sample='s#3 ZnBr', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=18900, X1=21700, dX=650, Y0=4300, Y1=16700, passes=2, sample='s#2 ZnClO4', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=7200, X1=9900, dX=650, Y0=4300, Y1=16700, passes=2, sample='s#1 ZnIm4', linedelay=0)

#scan.spot(detector.eiger(), X=43000, Y=16000, exposure=2, images=200, sample='s4 top Fe foil Kb')
#scan.spot(detector.eiger(), X=43000, Y=14200, exposure=1, images=120, sample='s4 top Co foil Ka12')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=42500, X1=45100, dX=650, Y0=3100, Y1=7600, passes=2, sample='s#4 bottom Fe2O3', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=54100, X1=56900, dX=650, Y0=4200, Y1=16700, passes=2, sample='s#5 ZnFe decomp', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=66100, X1=68900, dX=600, Y0=4200, Y1=16700, passes=2, sample='s#6 ZnH2Fe decomp', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=78000, X1=80700, dX=650, Y0=4200, Y1=14000, passes=2, sample='s#7 LZnH decomp', linedelay=0)


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveOPEN(14)
