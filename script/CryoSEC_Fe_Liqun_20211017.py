# Fe measurement
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=83300, X1=85800, dX=750, Y0=4000, Y1=16000, passes=5, sample='s7 K-MgFe2O4(R)', linedelay=0)
scan.spot(detector.eiger(), X=12500, Y=12300, exposure=1, images=60, sample='Co foil Ka12')
scan.spot(detector.eiger(), X=12500, Y=15000, exposure=2, images=300, sample='Fe foil Kb')
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=12000, dX=750, Xpoints=3, Y0=4000, dY=50, Ypoints=60, passes=3, sample='s1 bottom Fe2O3 VtC', linedelay=0, moveback=0)

#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=23500, dX=750, Xpoints=3, Y0=4000, dY=50, Ypoints=60, passes=3, sample='s2 Fe3O4 VtC', linedelay=0, moveback=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=46900, X1=49100, dX=700, Y0=4000, Y1=16000, passes=5, sample='s4 MgFe2O4(C)', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=58800, X1=61300, dX=750, Y0=4000, Y1=16000, passes=5, sample='s5 K-MgFe2O4(C)', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=70800, X1=73300, dX=750, Y0=4000, Y1=16000, passes=5, sample='s6 MgFe2O4(R)', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=83300, X1=85800, dX=750, Y0=4000, Y1=16000, passes=5, sample='s7 K-MgFe2O4(R)', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=35500, X1=37500, dX=700, Y0=4000, Y1=16000, passes=5, sample='s3 Fe-MOF', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=83200, X1=86000, dX=750, Y0=4000, Y1=17000, passes=5, sample='s7 K-MgFeCoO4(R)', linedelay=0)
#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=35000, dX=750, Xpoints=3, Y0=12100, dY=50, Ypoints=40, passes=2, sample='s3 top CoO VtC', linedelay=0, moveback=1)
#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=35000, dX=750, Xpoints=4, Y0=4000, dY=50, Ypoints=80, passes=1, sample='s3 bottom Co(OH)2 VtC', linedelay=0, moveback=1)
#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=23200, dX=750, Xpoints=3, Y0=12100, dY=50, Ypoints=40, passes=2, sample='s2 top CoAl2O4 VtC', linedelay=0, moveback=1)
#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=23200, dX=750, Xpoints=3, Y0=5000, dY=50, Ypoints=40, passes=2, sample='s2 bottom Co3O4 VtC', linedelay=0, moveback=1)


#pink.photon_shutter_auto_open_disable()
pink.shutter_hard_CLOSE()