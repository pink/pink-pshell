###################################################################################################
#  Deployment specific global definitions - executed after startup.py
###################################################################################################
from plib.logger_class import MasterLogger

run("plib/bpm_class.py")
run("plib/pink_extra.py")
run("plib/pink_class.py")
run("plib/blade_func.py")
run("plib/scan_func_elec.py")
run("plib/scan_func_cryo.py")
run("plib/sim_functions.py")
run("plib/cryo_functions.py")
run("plib/alert_class.py")
#run("plib/atto_class.py")
run("plib/dcm_func.py")
run("plib/headers.py")
run("plib/m2_class.py")
run("plib/devices/devlist.py")
run("plib/fluid_func.py")

pinkdevice = PINKDEV()

bpm = BPM()
pink = PINKCLASS()
blade = BLADEFUNC()
if (get_setting("chamber")=="cryo"):
    scan = SCANFUNCRYO()
else:
    scan = SCANFUNC()
detector = DETEC()
slit = SLITS()
filters = FILTERS()
source = SOURCES()
axis = SAMPLEAXIS()
sim = SIMFUNC()
cryo = CRYOFUN()
alert = ALERT()
#atto = ATTO()
piezo = PIEZOS()
dcm = DCMFUNC()
headers = HEADERS()
m2 = M2()
fluid = FLUID()

elab = ELAB()
sleep(1)
mlogger = MasterLogger(elab)

if(get_setting("chamber")=="cryo"):
    print("Scripts using Cryogenic SEC")
else:
    print("Scripts using Electro SEC")

def on_command_started(info):
    mlogger.onstart(info)

def on_command_finished(info):
    mlogger.onend(info)

pink.Help()

