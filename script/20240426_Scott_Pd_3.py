#20240426_Scott_Pd_3
#New aluminum holder: Al#4

scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=79000, X1=82000, dX=700, Y0=3400, Y1=16700, passes=2, sample='s#7 74; [TpPd(u-OH)]2 (diluted in wax)', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=67000, X1=70000, dX=750, Y0=3400, Y1=16700, passes=2, sample='s#6 73; TpPd(py)(OH)', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=55300, X1=58300, dX=700, Y0=3400, Y1=16700, passes=8, sample='s#5 43; 0.33% Pd/ 0.33% Au/ 0.33% Pt NPs', linedelay=0)

scan.spot(detector.greateyes(), X=45000, Y=14000, exposure=2, images=150, sample='s#4 Pd foil')
scan.spot(detector.greateyes(), X=45000, Y=12500, exposure=2, images=100, sample='s#4 Fe foil, Ka2')
scan.zigzag_absolute(detector.greateyes(), exposure=3, X0=44000, dX=700, Xpoints=3, Y0=4500, dY=100, Ypoints=22, passes=1, sample='s#4 Fe2O3 Ka2', linedelay=0, moveback=0)

scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=31000, X1=34000, dX=700, Y0=3400, Y1=16700, passes=8, sample='s#3 42; 0.475% Pd/ 0.475% Au/ 0.05% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=19200, X1=22000, dX=700., Y0=3500, Y1=16500, passes=8, sample='s#2 41; 0.5% Pd/ 0.5% Au NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=7200, X1=10000, dX=700, Y0=3500, Y1=16500, passes=8, sample='s#1 40; 1% Pd NPs', linedelay=0)

scan.spot(detector.greateyes(), X=8500, Y=3350, exposure=3, images=200, sample='s#1 40; 1% Pd NPs')
scan.spot(detector.greateyes(), X=20500, Y=3350, exposure=3, images=200, sample='s#2 41; 0.5% Pd/ 0.5% Au NPs')
scan.spot(detector.greateyes(), X=32500, Y=3350, exposure=3, images=200, sample='s#3 42; 0.475% Pd/ 0.475% Au/ 0.05% Pt NPs')
scan.spot(detector.greateyes(), X=57000, Y=3350, exposure=3, images=200, sample='s#5 43; 0.33% Pd/ 0.33% Au/ 0.33% Pt NPs')
scan.spot(detector.greateyes(), X=68500, Y=3350, exposure=3, images=200, sample='s#6 73; TpPd(py)(OH)')
scan.spot(detector.greateyes(), X=80500, Y=3350, exposure=3, images=200, sample='s#7 74; [TpPd(u-OH)]2 (diluted in wax)')

scan.spot(detector.greateyes(), X=45000, Y=14000, exposure=2, images=150, sample='s#4 Pd foil')
scan.spot(detector.greateyes(), X=45000, Y=12500, exposure=2, images=100, sample='s#4 Fe foil, Ka2')
scan.zigzag_absolute(detector.greateyes(), exposure=3, X0=44000, dX=700, Xpoints=3, Y0=4500, dY=100, Ypoints=22, passes=1, sample='s#4 Fe2O3 Ka2', linedelay=0, moveback=0)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)