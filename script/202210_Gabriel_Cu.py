scan.spot(detector.eiger(), X=4000, Y=500, exposure=5, images=200, sample='s#2 Cu foil Kb')
scan.spot(detector.eiger(), X=4000, Y=3000, exposure=3, images=120, sample='s#2 Zn foil Ka12')
scan.spot(detector.eiger(), X=4000, Y=5500, exposure=5, images=200, sample='s#2 Ho foil La')

scan.zigzag(detector.eiger(), exposure=10, X0=22500, dX=100, Xpoints=1, Y0=2000, dY=2, Ypoints=100, passes=10, sample='s#3 IG0281 Zr6 Cu0.7 O31 C79 1.7% damage scan', linedelay=0)
scan.zigzag(detector.eiger(), exposure=10, X0=-13000, dX=100, Xpoints=1, Y0=2000, dY=2, Ypoints=100, passes=9, sample='s#1 IGO282 Zr6 Cu0.7 O31 C79 2.0% damage scan', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=-13550, X1=-11450, dX=700, Y0=-2500, Y1=8100, passes=7, sample='s#1 IGO279 Zr6 Cu0.35 O31 C79 0.81%', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=21450, X1=23550, dX=700, Y0=-6600, Y1=8100, passes=5, sample='s#3 IG0280 Zr6 Cu0.35 O31 C79 0.83%', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=38200, X1=40300, dX=700, Y0=-6600, Y1=8100, passes=5, sample='s#4 IGO282 Zr6 Cu0.7 O31 C79 2.0%', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=-13550, X1=-11450, dX=700, Y0=-2500, Y1=8100, passes=10, sample='s#1 IGO279 Zr6 Cu0.35 O31 C79 0.81%', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=21450, X1=23550, dX=700, Y0=-6600, Y1=8100, passes=10, sample='s#3 IG0280 Zr6 Cu0.35 O31 C79 0.83%', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=38200, X1=40300, dX=700, Y0=-6600, Y1=8100, passes=7, sample='s#4 IGO282 Zr6 Cu0.7 O31 C79 2.0%', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=-13550, X1=-11450, dX=700, Y0=-2500, Y1=8100, passes=12, sample='s#1 IGO279 Zr6 Cu0.35 O31 C79 0.81%', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1.0, X0=21450, X1=23550, dX=700, Y0=-6600, Y1=8100, passes=8, sample='s#3 IG0280 Zr6 Cu0.35 O31 C79 0.83%', linedelay=0)


pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveCLOSE(12)