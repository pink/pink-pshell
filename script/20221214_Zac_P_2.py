
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.4, X0=12900, X1=13550, dX=650, Y0=4050, Y1=16750, passes=10, sample='s#1_Fe2NH3poat_ZM', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.3, X0=22700, X1=23400, dX=700, Y0=4350, Y1=15500, passes=4, sample='s#2_H3poat_ZN', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.3, X0=36800, X1=37450, dX=650, Y0=4050, Y1=16700, passes=4, sample='s#3_Fe2OH3poat_ZP', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.5, X0=47800, X1=49400, dX=700, Y0=4100, Y1=16500, passes=1, sample='s#4_NaH2PO4_new', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.3, X0=72000, X1=72650, dX=650, Y0=4050, Y1=16700, passes=4, sample='s#6_Fe2OHpoat_D', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.3, X0=84200, X1=84900, dX=700, Y0=4100, Y1=16200, passes=4, sample='s#7_Fe3NH3poat_F', linedelay=0)




## after returning beam

scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.1, X0=47200, X1=49000, dX=700, Y0=5500, Y1=14000, passes=10, sample='s#4_NaH2PO4_new Si111 bent', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.1, X0=72000, X1=72700, dX=700, Y0=3800, Y1=16400, passes=10, sample='s#6_Fe2OHpoat_D', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.1, X0=84200, X1=84900, dX=700, Y0=3800, Y1=16500, passes=10, sample='s#7_Fe3NH3poat_F', linedelay=0)

