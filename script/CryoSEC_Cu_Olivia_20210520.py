scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=9200, X1=11000, dX=750, Y0=3100, Y1=16700, passes=10, sample='s#1 Kb', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=21000, X1=23000, dX=750, Y0=3100, Y1=16700, passes=10, sample='s#2 Kb', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=33500, X1=35000, dX=750, Y0=3100, Y1=16700, passes=10, sample='s#3 Kb', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=44800, X1=47000, dX=750, Y0=3100, Y1=16700, passes=10, sample='CuCl2 Kb', linedelay=0)


scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=33600, X1=35100, dX=750, Y0=3100, Y1=16700, passes=5, sample='s#3 Kb', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=21100, X1=23000, dX=750, Y0=3100, Y1=16700, passes=15, sample='s#2 Kb', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=9300, X1=11000, dX=750, Y0=3100, Y1=16700, passes=5, sample='s#1 Kb', linedelay=0)

alert.message("measurement done")
print("########## DONE ##########")

pink.shutter_hard_CLOSE()