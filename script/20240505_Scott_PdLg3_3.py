scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=31900, X1=34300, dX=600., Y0=3200, Y1=16200, passes=6, sample='s#3 67; Reduced 1% Pd NPs', linedelay=0)

scan.spot(detector.greateyes(), X=45000, Y=15400, exposure=10, images=200, sample='Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=45000, Y=13800, exposure=2, images=100, sample='Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=45000, Y=12300, exposure=5, images=200, sample='Pd foil with kapton, Lg3')
scan.zigzag_absolute(detector.greateyes(), exposure=10, X0=43800, dX=650, Xpoints=5, Y0=4500, dY=60, Ypoints=40, passes=1, sample='Fe2O3 Kb13', linedelay=0, moveback=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=55300, X1=57900, dX=650, Y0=3200, Y1=16200, passes=6, sample='s#5 69;  Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=55500, X1=57800, dX=650, Y0=3200, Y1=16200, passes=6, sample='s#5 69;  Reduced 0.475% Pd / 0.475% Au / 0.05% Pt NPs', linedelay=0)

scan.spot(detector.greateyes(), X=45000, Y=15400, exposure=10, images=200, sample='Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=45000, Y=12300, exposure=5, images=200, sample='Pd foil with kapton, Lg3')

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=32100, X1=34300, dX=700., Y0=3200, Y1=16200, passes=6, sample='s#3 67; Reduced 1% Pd NPs', linedelay=0)

pink.shutter_hard_CLOSE()
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=6, sample='dark noise, 5s')

pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)