
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=10400, X1=13300, dX=650, Y0=3100, Y1=14000, passes=10, sample='s#1 59: Ni', linedelay=0)

scan.spot(detector.eiger(), X=22500, Y=7800, exposure=2, images=150, sample='s#2 Tb foil, La12')
scan.spot(detector.eiger(), X=22500, Y=12500, exposure=2, images=300, sample='s#2 Ni foil, Kb')
scan.spot(detector.eiger(), X=22500, Y=15000, exposure=10, images=50, sample='s#2 Ta foil, La12')

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=10400, X1=13300, dX=650, Y0=3100, Y1=14000, passes=10, sample='s#1 59: Ni', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=10400, X1=13300, dX=650, Y0=3100, Y1=14000, passes=10, sample='s#1 59: Ni', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33700, X1=36700, dX=650, Y0=3100, Y1=16750, passes=10, sample='s#3 60: NiPdNi', linedelay=0)

scan.spot(detector.eiger(), X=22500, Y=7800, exposure=2, images=150, sample='s#2 Tb foil, La12')
scan.spot(detector.eiger(), X=22500, Y=12500, exposure=2, images=300, sample='s#2 Ni foil, Kb')
scan.spot(detector.eiger(), X=22500, Y=15000, exposure=10, images=50, sample='s#2 Ta foil, La12')

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)