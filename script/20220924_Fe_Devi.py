scan.spot(detector.eiger(), X=22000, Y=-4200, exposure=3, images=200, sample='s#3 Fe foil Kb')
scan.spot(detector.eiger(), X=22000, Y=1500, exposure=4, images=25, sample='s#3 Co foil Ka12')

scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=-13600, X1=-11000, dX=750, Y0=-6600, Y1=8100, passes=5, sample='s#1 SrFeO3', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=1, X0=4600, X1=6300, dX=700, Y0=-6600, Y1=6000, passes=1, sample='s#2 Fe2O3', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=39000, X1=41400, dX=750, Y0=-6600, Y1=8100, passes=5, sample='s#4 Fe(acetylacetone)', linedelay=0)

#pink.shutter_hard_CLOSE()