
scan.spot(detector.eiger(), X=47000, Y=12500, exposure=2, images=60, sample='s#4 Co foil Ka12')
scan.spot(detector.eiger(), X=47000, Y=15500, exposure=2, images=200, sample='s#4 Fe foil Kb')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=2, X0=46500, X1=49500, dX=700, Y0=4000, Y1=7000, passes=2, sample='s#4_Fe2O3 Kb', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=34900, X1=37100, dX=700, Y0=4000, Y1=16500, passes=6, sample='s#3_Fe3', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=58200, X1=60500, dX=700, Y0=4000, Y1=16500, passes=10, sample='s#5 Fe4', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=70500, X1=72900, dX=700, Y0=10100, Y1=16600, passes=18, sample='s#6 Fe4 1/2 of sample', linedelay=0)

scan.spot(detector.eiger(), X=36000, Y=16200, exposure=2, images=150, sample='s#3 F3')
scan.spot(detector.eiger(), X=36000, Y=16100, exposure=2, images=150, sample='s#3 F3')
scan.spot(detector.eiger(), X=36000, Y=4500, exposure=2, images=150, sample='s#3 F3')
scan.spot(detector.eiger(), X=36000, Y=4400, exposure=2, images=150, sample='s#3 F3')

scan.spot(detector.eiger(), X=59000, Y=16200, exposure=5, images=60, sample='s#5 F4')
scan.spot(detector.eiger(), X=59000, Y=16100, exposure=5, images=60, sample='s#5 F4')
scan.spot(detector.eiger(), X=59000, Y=4500, exposure=5, images=60, sample='s#5 F4')
scan.spot(detector.eiger(), X=59000, Y=4400, exposure=5, images=60, sample='s#5 F4')

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=58200, X1=60500, dX=700, Y0=4000, Y1=16500, passes=10, sample='s#5 Fe4', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=1, X0=70500, X1=72900, dX=700, Y0=10100, Y1=16600, passes=10, sample='s#6 Fe4 1/2 of sample', linedelay=0)


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveCLOSE(12)
