scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=25200, X1=27300, dX=600., Y0=4100, Y1=13000, passes=4, sample='s#2 sFeII_FeQPY', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=37200, X1=39300, dX=600., Y0=4100, Y1=13400, passes=4, sample='s#3 FeI_FeQPY + 1eq CoCp2*', linedelay=0)

scan.spot(detector.eiger(), X=49000, Y=12200, exposure=5, images=240, sample='s#4 Fe foil, Kb13')
scan.spot(detector.eiger(), X=61000, Y=12000, exposure=1, images=100, sample='s#5 Co foil, Ka12')
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=47800, X1=49800, dX=500., Y0=4100, Y1=8000, passes=3, sample='s#4 Fe2O3 Kb13', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=71800, X1=74000, dX=600., Y0=4100, Y1=14000, passes=4, sample='s#6 Fe0_FeQPY + 2 equivalents CoCp2* ', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=0.5, X0=83300, X1=86100, dX=650., Y0=4100, Y1=16400, passes=4, sample='s#7 56: FePdFeCl2(dpa)4 ', linedelay=0)


scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=71800, X1=74000, dX=600., Y0=4100, Y1=14000, passes=8, sample='s#6 Fe0_FeQPY + 2 equivalents CoCp2* ', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=37200, X1=39300, dX=600., Y0=4100, Y1=13400, passes=8, sample='s#3 FeI_FeQPY + 1eq CoCp2*', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=25200, X1=27300, dX=600., Y0=4100, Y1=13000, passes=4, sample='s#2 sFeII_FeQPY', linedelay=0)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)
