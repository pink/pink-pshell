### Energy group ####
energy      = 8000
## Undulator gap
gap         = 6.475 #6.475 #5.66 
## foil filter: 0,5,10,20
foil        = 10
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.4
## AU2
au2centery  = 0.6
au2centerx  = 0.4
au2gapy     = 2.6 #3.5
au2gapx     = 2.8 #3.5
## AU3
au3centery  = -20.0
au3centerx  = -1.8
au3gapy     = 1.4
au3gapx     = 1.8
## M2 positions
# group options: 1,2 or 3
m2group     = 3
m2poix      = 18500
m2poiy      = 1250
m2tx        = 18500
m2ty        = 1250
m2tz        = 0
m2rx        = 835
m2ry        = -2000
m2rz        = -3860 #-3680
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 583
cross1y     = 687
## BPM2 cross
cross2x     = 659
cross2y     = 447
## BPM3 cross
cross3x     = 653
cross3y     = 471
## sample cam cross
#crossSx     = 570#760
#crossSy     = 866#730


## BPM1 camera
bpm1_gain     = 1.0
bpm1_exposure = 0.03
## BPM2 camera
bpm2_gain     = 1.0
bpm2_exposure = 0.04
## BPM3 camera
bpm3_gain     = 1.0
bpm3_exposure = 0.025
## BPM4 camera
#bpm4_gain     = 1.0
#bpm4_exposure = 0.03



# EOF
