### Energy group ####
energy      = 9500
## Undulator gap
gap         = 6.04 #6.04, 6.39, 6.85, 7.34
## foil filter: 0,5,10,20
foil        = 10 #20
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.4
## AU2
au2centery  = 0.6
au2centerx  = 0.4
au2gapy     = 2.6 #3.5
au2gapx     = 2.8 #3.5
## AU3
au3centery  = -20.0 #-20.2
au3centerx  = -1.8
au3gapy     = 1.2 #1.4
au3gapx     = 1.8
## M2 positions
# group options: 1,2 or 3
m2group     = 3
m2poix      = 24500
m2poiy      = 1500
m2tx        = 24500
m2ty        = 1500
m2tz        = 0
m2rx        = 675
m2ry        = -2000
m2rz        = 2760 #3210 #3110
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 580
cross1y     = 686
bpm1_gain     = 1.0
bpm1_exposure = 0.04
## BPM2 cross
cross2x     = 655
cross2y     = 498
bpm2_gain     = 1.0
bpm2_exposure = 0.06
## BPM3 cross
cross3x     = 651 #633
cross3y     = 474 #451 #453
bpm3_gain     = 1.0
bpm3_exposure = 0.06
## sample cam cross
#crossSx     = 760
#crossSy     = 730

# EOF
