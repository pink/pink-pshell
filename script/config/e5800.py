### Energy group ####
energy      = 5800
## Undulator gap
gap         = 6.06
## foil filter: 0,5,10,20
foil        = 10
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.2
## AU2
au2centery  = 0.75
au2centerx  = 0.1
au2gapy     = 4.0
au2gapx     = 4.0
## AU3
au3centery  = -20.0
au3centerx  = -1.6
au3gapy     = 1.2
au3gapx     = 1.8
## M2 positions
# group options: 1,2 or 3
m2group     = 2
m2poix      = 18300
m2poiy      = 600
m2tx        = 18300
m2ty        = 600
m2tz        = 0
m2rx        = 158
m2ry        = 100
m2rz        = -6350
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 634
cross1y     = 685
bpm1_gain     = 1.0
bpm1_exposure = 0.015
## BPM2 cross
cross2x     = 668
cross2y     = 501
bpm2_gain     = 1.0
bpm2_exposure = 0.01
## BPM3 cross
cross3x     = 570
cross3y     = 462
bpm3_gain     = 1.0
bpm3_exposure = 0.007
## sample cam cross
#crossSx     = 538
#crossSy     = 866
