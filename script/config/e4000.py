### Energy ####
energy      = 4000
## Undulator gap
gap         = 6.43 #7.73 #6.43 #5.65 #5.66 #6.47
## foil filter: 0,5,10,20
foil        = 10
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.4  #1.4
## AU2
au2centery  = 0.6
au2centerx  = 0.4
au2gapy     = 5
au2gapx     = 5
## AU3
au3centery  = -20.0
au3centerx  = -1.7
au3gapy     = 1.2
au3gapx     = 1.6
## M2 positions
# group options: 1,2 or 3
m2group     = 1
m2poix      = 24500
m2poiy      = 1500
m2tx        = 24500
m2ty        = 1500
m2tz        = 0
m2rx        = -1805 #-1800
m2ry        = 0
m2rz        = 2670 #2740 #3120
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 642
cross1y     = 685
bpm1_exposure = 0.0025
## BPM2
cross2x     = 669
cross2y     = 501
bpm2_exposure = 0.0025
## BPM3 cross
cross3x     = 557
cross3y     = 459
bpm3_exposure = 0.004
## sample cam cross
#crossSx     = 760
#crossSy     = 730

## Camera parameters
bpm1_gain     = 1.0
bpm1_exposure = 0.003
bpm2_gain     = 1.0
bpm2_exposure = 0.003

bpm3_gain     = 1.0
bpm3_exposure = 0.005
# EOF
