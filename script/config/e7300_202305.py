### Energy group ####
energy      = 7300
## Undulator gap
gap         = 5.66 #6.57
## foil filter: 0,5,10,20
foil        = 20
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.4  #1.4
## AU2
au2centery  = 0.6
au2centerx  = 0.4
au2gapy     = 2.6
au2gapx     = 2.8
## AU3
au3centery  = -20.0
au3centerx  = -1.5
au3gapy     = 1.2
au3gapx     = 1.6
## M2 positions
# group options: 1,2 or 3
m2group     = 3
m2poix      = 12500
m2poiy      = 1000
m2tx        = 12500
m2ty        = 1000
m2tz        = 0
m2rx        = 910
m2ry        = -1000
m2rz        = -10160 #-10100
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 583
cross1y     = 689 #686
## BPM2 cross
cross2x     = 658 #660
cross2y     = 491 #493
## BPM3 cross
cross3x     = 635
cross3y     = 459
## sample cam cross
#crossSx     = 760
#crossSy     = 730

## BPM1 camera
bpm1_gain     = 1.0
bpm1_exposure = 0.025
## BPM2 camera
bpm2_gain     = 1.0
bpm2_exposure = 0.03
## BPM3 camera
bpm3_gain     = 1.0
bpm3_exposure = 0.012
## BPM4 camera
#bpm4_gain     = 1.0
#bpm4_exposure = 0.03

