## M1 positions: May 2021
m1tx        = 570
m1ty        = 500
m1rx        = -1390
#m1ry        = -10650
m1ry        = -10480
m1rz        = 10500
#Old values: 2021 winter-spring
#m1tx        = 640
#m1ty        = 500
#m1rx        = -1400
#m1ry        = -10500
#m1rz        = 7500
## M1 delta values
m1deltatx   = 10
m1deltaty   = 2
m1deltarx   = 0.5
m1deltary   = 10
m1deltarz   = 10

