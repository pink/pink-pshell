### Energy group ####
energy      = 3000
## Undulator gap
gap         = 7.7  #6.25
## foil filter: 0,5,10,20
foil        = 10
## AU1
au1centery  = 0.6
au1centerx  = -0.2
au1gapy     = 0.8
au1gapx     = 1.4
## AU2
au2centery  = 0.75
au2centerx  = 0.5
au2gapy     = 4.0
au2gapx     = 4.0
## AU3
au3centery  = -20.0
au3centerx  = -1.8
au3gapy     = 1.2
au3gapx     = 1.8
## M2 positions
# group options: 1,2 or 3
m2group     = 1
m2poix      = 18400
m2poiy      = 2000
m2tx        = 18400
m2ty        = 2000
m2tz        = 0
m2rx        = -1710
m2ry        = 0
m2rz        = -4170 #-3910 #-3760
## M2 delta values
m2deltatx   = 10
m2deltaty   = 5
m2deltatz   = 10
m2deltarx   = 10
m2deltary   = 10
m2deltarz   = 10
## BPM1 cross
cross1x     = 625
cross1y     = 685
bpm1_exposure = 0.002
## BPM2 cross
cross2x     = 665
cross2y     = 498
bpm2_exposure = 0.003
## BPM3 cross
cross3x     = 578
cross3y     = 471
bpm3_exposure = 0.016
## sample cam cross
#crossSx     = 760
#crossSy     = 730
