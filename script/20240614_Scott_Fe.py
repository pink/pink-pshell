scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=50500, X1=53500, dX=600, Y0=3500, Y1=13000, passes=12, sample='s#4 Marcos FeII-S2', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=27300, X1=30000, dX=600., Y0=4500, Y1=16000, passes=12, sample='s#2 57: FeCl2(dmap)4', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=15400, X1=18000, dX=600., Y0=4500, Y1=16000, passes=12, sample='s#1 56: FePdFeCl2(dpa)4', linedelay=0)

scan.spot(detector.eiger(), X=39500, Y=16400, exposure=5, images=300, sample='s#3 Fe foil, Kb13')
scan.spot(detector.eiger(), X=39500, Y=14200, exposure=1, images=60, sample='s#3 Co foil, Ka12')
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=2, X0=38500, X1=41500, dX=500., Y0=4000, Y1=8000, passes=2, sample='s#3 Fe2O3 Kb13', linedelay=0)


scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=50500, X1=53500, dX=600, Y0=3500, Y1=13000, passes=12, sample='s#4 Marcos FeII-S2', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=27300, X1=30000, dX=600., Y0=4500, Y1=16000, passes=12, sample='s#2 57: FeCl2(dmap)4', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=15400, X1=18000, dX=600., Y0=4500, Y1=16000, passes=12, sample='s#1 56: FePdFeCl2(dpa)4', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=15400, X1=18000, dX=600., Y0=4500, Y1=16000, passes=6, sample='s#1 56: FePdFeCl2(dpa)4', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=27300, X1=30000, dX=600., Y0=4500, Y1=16000, passes=6, sample='s#2 57: FeCl2(dmap)4', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=50500, X1=53500, dX=600, Y0=3500, Y1=13000, passes=6, sample='s#4 Marcos FeII-S2', linedelay=0)


pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)