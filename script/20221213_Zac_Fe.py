
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=11600, X1=12250, dX=650, Y0=4050, Y1=16750, passes=20, sample='s#1_Fe2OHpoat_B', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=23400, X1=24050, dX=650, Y0=4050, Y1=16700, passes=5, sample='s#2_Fe2poat_A', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=35500, X1=36150, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#3_Fe2OH2poat_C', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.2, X0=46500, X1=48200, dX=700, Y0=4100, Y1=16000, passes=5, sample='s#4', linedelay=0)

#scan.spot(detector.eiger(), X=60000, Y=15000, exposure=2, images=60, sample='s#5 Co foil Ka12')
#scan.spot(detector.eiger(), X=60000, Y=12500, exposure=2, images=200, sample='s#5 Fe foil Kb')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=2, X0=59500, X1=61000, dX=700, Y0=5000, Y1=7000, passes=5, sample='s#5_Fe2O3 Kb', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=70700, X1=71350, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#6_Fe3O3poat_I', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=82800, X1=83500, dX=700, Y0=4100, Y1=16200, passes=10, sample='s#7Fe3O3Hpoat_H', linedelay=0)


## second passes

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=82800, X1=83500, dX=700, Y0=4200, Y1=16200, passes=10, sample='s#7Fe3O3Hpoat_H', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=70700, X1=71350, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#6_Fe3O3poat_I', linedelay=0)

#scan.spot(detector.eiger(), X=60000, Y=15000, exposure=2, images=60, sample='s#5 Co foil Ka12')
#scan.spot(detector.eiger(), X=60000, Y=12500, exposure=2, images=200, sample='s#5 Fe foil Kb')
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=2, X0=58500, X1=59500, dX=700, Y0=5000, Y1=7000, passes=5, sample='s#5_Fe2O3 Kb', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=35500, X1=36150, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#3_Fe2OH2poat_C', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=23400, X1=24050, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#2_Fe2poat_A', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=11600, X1=12250, dX=650, Y0=4050, Y1=16750, passes=10, sample='s#1_Fe2OHpoat_B', linedelay=0)

## third passes

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=11600, X1=12250, dX=650, Y0=4050, Y1=16750, passes=10, sample='s#1_Fe2OHpoat_B', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=23400, X1=24050, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#2_Fe2poat_A', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=35500, X1=36150, dX=650, Y0=4050, Y1=16700, passes=10, sample='s#3_Fe2OH2poat_C', linedelay=0)




###
### second sample carrier, mapped for phosphorus logbook
###

scan.spot(detector.eiger(), X=60000, Y=12500, exposure=2, images=30, sample='s#5 Co foil Ka12')
scan.spot(detector.eiger(), X=60000, Y=15500, exposure=2, images=100, sample='s#5 Fe foil Kb')

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=12400, X1=13100, dX=700, Y0=4300, Y1=15500, passes=16, sample='s#1_Fe2NH3poat_ZM', linedelay=0)
#sim.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=22700, X1=23400, dX=700, Y0=4350, Y1=15500, passes=4, sample='s#2_H3poat_ZN', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=36400, X1=37100, dX=700, Y0=4300, Y1=16500, passes=16, sample='s#3_Fe2OH3poat_ZP', linedelay=0)


#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=72000, X1=72700, dX=700, Y0=4050, Y1=16700, passes=20, sample='s#6_Fe2OHpoat_D', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=83800, X1=84500, dX=700, Y0=4100, Y1=16200, passes=700, sample='s#7_Fe3NH3poat_F', linedelay=0)


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveCLOSE(12)