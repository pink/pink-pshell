
#
#sim.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=48500, X1=49300, dX=700, Y0=6300, Y1=16000, passes=12, sample='TFE solvent, background', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=36500, X1=37500, dX=750, Y0=6300, Y1=17000, passes=18, sample='Ru IV', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=23700, X1=25000, dX=750, Y0=6300, Y1=17000, passes=18, sample='Ru III', linedelay=0)
#sim.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=11800, X1=13000, dX=750, Y0=6300, Y1=17000, passes=18, sample='Ru II', linedelay=0)

#measurement 2
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=11800, X1=13300, dX=700, Y0=6300, Y1=17000, passes=8, sample='s1 Ru II', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=23200, X1=24800, dX=700, Y0=6300, Y1=17000, passes=8, sample='Ru III', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=36000, X1=37800, dX=750, Y0=6300, Y1=17000, passes=16, sample='Ru IV', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=48400, X1=49900, dX=600, Y0=6300, Y1=16000, passes=12, sample='s4 TFE solvent, background', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=59000, X1=61500, dX=700, Y0=6100, Y1=14000, passes=10, sample='s5 Ru tda electrode', linedelay=0)
scan.spot(detector.greateyes(), X=71500, Y=7000, exposure=1, images=20, sample='Pd La12')
scan.spot(detector.greateyes(), X=71500, Y=14000, exposure=2, images=100, sample='KCl Kb13')
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=2, X0=82500, X1=85500, dX=700, Y0=3600, Y1=6000, passes=8, sample='s7 bottom RuP NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=4, X0=82500, X1=85300, dX=2800, Y0=13000, Y1=17000, passes=20, sample='s7 top RuP NPs @ SILP', linedelay=0)

