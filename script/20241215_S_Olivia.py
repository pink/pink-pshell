
pink.roi(detector.greateyes(), y=80, size=90)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=22800, X1=25500, dX=500, Y0=4000, Y1=16500, passes=5, sample='#2 (4) Cotrpbdt2H', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=35200, X1=37600, dX=600, Y0=4000, Y1=15000, passes=5, sample='#3 (2) CoPSH', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=46500, X1=49100, dX=500, Y0=13500, Y1=16600, passes=2, sample='#4 top Na2SO4', linedelay=0)
pink.roi(detector.greateyes(), y=70, size=90)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=58800, X1=61800, dX=600, Y0=4000, Y1=16500, passes=4, sample='#5 (6) Cotrpbdt', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=71800, X1=74800, dX=600, Y0=4000, Y1=16500, passes=4, sample='#6 (7) KCotrpbdt', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=83600, X1=86600, dX=600, Y0=4000, Y1=16500, passes=4, sample='#7 (8) KCoPS', linedelay=0)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)
