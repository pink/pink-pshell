
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=47100, X1=49500, dX=750, Y0=4000, Y1=17000, passes=5, sample='s4 MgFeCoO4(C)', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=59100, X1=62000, dX=750, Y0=4000, Y1=17000, passes=5, sample='s5 K-MgFeCoO4(C)', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=71200, X1=74000, dX=750, Y0=4000, Y1=17000, passes=5, sample='s6 MgFeCoO4(R)', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=83200, X1=86000, dX=750, Y0=4000, Y1=17000, passes=5, sample='s7 K-MgFeCoO4(R)', linedelay=0)
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=35000, dX=750, Xpoints=3, Y0=12100, dY=50, Ypoints=40, passes=2, sample='s3 top CoO VtC', linedelay=0, moveback=1)
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=35000, dX=750, Xpoints=4, Y0=4000, dY=50, Ypoints=80, passes=1, sample='s3 bottom Co(OH)2 VtC', linedelay=0, moveback=1)
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=23200, dX=750, Xpoints=3, Y0=12100, dY=50, Ypoints=40, passes=2, sample='s2 top CoAl2O4 VtC', linedelay=0, moveback=1)
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=23200, dX=750, Xpoints=3, Y0=5000, dY=50, Ypoints=40, passes=2, sample='s2 bottom Co3O4 VtC', linedelay=0, moveback=1)