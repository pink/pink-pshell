scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=34000, X1=35700, dX=760.0, Y0=3100, Y1=16900, passes=10, sample='s#3 (nacnac)Cu(I)(NCCH3)', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=46800, X1=48500, dX=770.0, Y0=3100, Y1=16900, passes=40, sample='s#4 (TMG3tren)Cu(I)', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=10400, X1=13000, dX=780.0, Y0=3200, Y1=16900, passes=4, sample='s#1 CuCl', linedelay=0)
scan.spot(detector.eiger(), X=23500, Y=14500, exposure=5, images=60, sample='s#2 Cu foil Kb')
scan.spot(detector.eiger(), X=23500, Y=7400, exposure=10, images=60, sample='s#2 Zn foil Kb')

#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=0.5, X0=57600, X1=59200, dX=770.0, Y0=3100, Y1=16900, passes=5, sample='s#5 TMGtren-Cu-hydroperoxo, irradiated s8', linedelay=0)


print("########## DONE ##########")
pink.shutter_hard_CLOSE()


