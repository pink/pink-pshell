#Energy calibration
#scan.spot(detector.eiger(), X=40000, Y=1000, exposure=2, images=60, sample='s#1 Co foil Ka12')
#scan.spot(detector.eiger(), X=40000, Y=-5000, exposure=2, images=100, sample='s#1 Fe foil Kb')


#Measurement
#scan.spot(detector.eiger(), X=23500, Y=300, exposure=2, images=300, sample='s#2 (4) Co25Fe05O4 Kb')
#scan.spot(detector.eiger(), X=23500, Y=400, exposure=2, images=300, sample='s#2 (4) Co25Fe05O4 Kb')
#scan.spot(detector.eiger(), X=23500, Y=500, exposure=2, images=300, sample='s#2 (4) Co25Fe05O4 Kb')
#scan.spot(detector.eiger(), X=23500, Y=600, exposure=2, images=300, sample='s#2 (4) Co25Fe05O4 Kb')
#scan.spot(detector.eiger(), X=23500, Y=700, exposure=2, images=300, sample='s#2 (4) Co25Fe05O4 Kb')



#scan.spot(detector.eiger(), X=5800, Y=700, exposure=2, images=300, sample='s#3 (5) Co0.5Fe2.5O4 Kb')
#scan.spot(detector.eiger(), X=5800, Y=800, exposure=2, images=300, sample='s#3 (5) Co0.5Fe2.5O4 Kb')


scan.spot(detector.eiger(), X=-11500, Y=1300, exposure=2, images=300, sample='s#4 (2) Co2FeO4 Kb')
scan.spot(detector.eiger(), X=-11500, Y=1400, exposure=2, images=300, sample='s#4 (2) Co2FeO4 Kb')



