from mathutils import *

n=21
diode=[0.0]*n
beam_prof=[0.0]*n
slit_x=[0.0]*n
print(diode)
for i in range(n):
    #slit_x[i]=1500-i*50 # JJ
    #slit_x[i]=-2.6+i*0.3 #AU2
    slit_x[i]=-3.8+i*0.2 #AU3
    
    #caput("PINK:PHY:AxisF", slit_x[i]) #JJ slit, left write
    #caput("u171pgm1:PH_4_SET", slit_x[i]) #AU2 slit, left write
    caput("AUY01U112L:AbsM3", slit_x[i]) #AU3 slit, left write
    sleep(2)
    #caget("PINK:PHY:AxisF.RBV")  #JJ slit, left read
    #caget("u171pgm1:PH_4_GET")  #AU2 slit, left read
    #caget("AUY01U112L:rdPosM3")  #AU3 slit, left read
    print(slit_x[i])
    #diode[i]=caget("PINK:CAE1:Current3:MeanValue_RBV") #diode BPM3
    # diode[i]=caget("PINK:CAE1:Current1:MeanValue_RBV") #diode diag chamber
    diode[i]=caget("PINK:CAE2:SumAll:MeanValue_RBV") #I0
    
    print(diode[i])
    
    plot(diode, title = "slit scan", xdata = slit_x)
sleep(2)
beam_prof=deriv(diode)
#plot(beam_prof, title = "slit scan", xdata = slit_x)
[p1, p2]=plot([None, None], ["diode", "derivative"], title="Blade scan")  
p1.getSeries(0).setData(slit_x, diode)
p2.getSeries(0).setData(slit_x, beam_prof)
ss_h = LinePlotSeries("Diode")
ss_v = LinePlotSeries("Derivative")
p1.addSeries(ss_h)
p2.addSeries(ss_v)


    