#Sample carrier 1
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=83000, X1=86000, dX=550, Y0=4000, Y1=16800, passes=1, sample='s7 (NH4)2HPO4', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=70600, X1=73500, dX=550, Y0=4000, Y1=16800, passes=1, sample='s6 NH4MgPO4', linedelay=0)




#scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=10600, X1=13600, dX=550, Y0=4000, Y1=16800, passes=1, sample='s1 45 KH2PO4', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=23500, X1=26500, dX=600, Y0=4000, Y1=16800, passes=1, sample='s2 Creatine phosphate', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=35500, X1=38500, dX=600, Y0=4000, Y1=16600, passes=1, sample='s3 Fe3(PO4)2', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=2, sample_exposure=0.2, X0=47100, X1=50000, dX=550, Y0=4000, Y1=16800, passes=1, sample='s4 EC NaH2PO4 ', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.5, X0=59100, X1=62000, dX=550, Y0=4000, Y1=16800, passes=2, sample='s5 D1S1-A', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=1, sample_exposure=0.2, X0=23700, X1=26500, dX=650, Y0=13000, Y1=16800, passes=2, sample='my test', linedelay=0) #Fortest
scan.spot(detector.greateyes(), X=None, Y=None, exposure=1, images=1, sample='test')