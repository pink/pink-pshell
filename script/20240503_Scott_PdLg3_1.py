#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=79100, X1=82000, dX=700, Y0=4000, Y1=16200, passes=8, sample='s#7 74; [TpPd(u-OH)]2 (diluted in wax)', linedelay=0)
#scan.spot(detector.greateyes(), X=80500, Y=3400, exposure=10, images=200, sample='s#7 damage 74; [TpPd(u-OH)]2 (diluted in wax)')

#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.5, X0=67300, X1=70100, dX=700, Y0=4000, Y1=16200, passes=8, sample='s#6 73; TpPd(py)(OH)', linedelay=0)
#scan.spot(detector.greateyes(), X=68500, Y=3400, exposure=10, images=200, sample='s#6 damage 73; TpPd(py)(OH)')


#scan.spot(detector.greateyes(), X=44500, Y=14900, exposure=10, images=300, sample='Fe foil with kapton, Kb13')
#scan.spot(detector.greateyes(), X=44500, Y=13900, exposure=2, images=200, sample='Ag foil with kapton, Lg1')
#scan.spot(detector.greateyes(), X=44500, Y=12100, exposure=10, images=100, sample='Pd foil with kapton, Lg3')
#scan.zigzag_absolute(detector.greateyes(), exposure=10, X0=43400, dX=500, Xpoints=5, Y0=4500, dY=60, Ypoints=40, passes=1, sample='Fe2O3 Kb13', linedelay=10, moveback=0)


#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=8500, X1=11300, dX=600, Y0=4000, Y1=16200, passes=6, sample='s#1 40; 1% Pd NPs', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=20000, X1=22800, dX=700., Y0=4000, Y1=16200, passes=8, sample='s#2 41; 0.5% Pd/ 0.5% Au NPs', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=31400, X1=34200, dX=700, Y0=4000, Y1=16200, passes=8, sample='s#3 42; 0.475% Pd/ 0.475% Au/ 0.05% Pt NPs', linedelay=0)


#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=55500, X1=58300, dX=650, Y0=4000, Y1=16200, passes=8, sample='s#5 43; 0.33% Pd/ 0.33% Au/ 0.33% Pt NPs', linedelay=0)
#
#
#2024.05.04
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=8600, X1=11400, dX=650., Y0=3800, Y1=12200, passes=8, sample='s#1 40; 1% Pd NPs', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=20000, X1=22900, dX=650., Y0=3800, Y1=11200, passes=10, sample='s#2 41; 0.5% Pd/ 0.5% Au NPs', linedelay=0)
scan.spot(detector.greateyes(), X=44500, Y=16500, exposure=10, images=6, sample='dark noise')
scan.spot(detector.greateyes(), X=44500, Y=14900, exposure=10, images=300, sample='Fe foil with kapton, Kb13')
scan.spot(detector.greateyes(), X=44500, Y=13900, exposure=2, images=200, sample='Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=44500, Y=12100, exposure=10, images=100, sample='Pd foil with kapton, Lg3')
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=55600, X1=58300, dX=600, Y0=3800, Y1=12200, passes=10, sample='s#5 43; 0.33% Pd/ 0.33% Au/ 0.33% Pt NPs', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1.4, X0=55600, X1=58300, dX=600, Y0=3800, Y1=12200, passes=6, sample='s#5 43; 0.33% Pd/ 0.33% Au/ 0.33% Pt NPs', linedelay=0)
scan.spot(detector.greateyes(), X=44500, Y=14900, exposure=10, images=300, sample='Fe foil with kapton, Kb13')

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1.4, X0=20000, X1=22900, dX=650., Y0=3800, Y1=11200, passes=6, sample='s#2 41; 0.5% Pd/ 0.5% Au NPs', linedelay=0)

scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1.4, X0=8600, X1=11400, dX=650., Y0=3800, Y1=12200, passes=4, sample='s#1 40; 1% Pd NPs', linedelay=0)
scan.spot(detector.greateyes(), X=44500, Y=14900, exposure=10, images=300, sample='Fe foil with kapton, Kb13')


pink.shutter_hard_CLOSE()
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise')
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)
