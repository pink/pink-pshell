###
### second sample carrier, after 3am
###

scan.spot(detector.eiger(), X=60000, Y=12500, exposure=2, images=60, sample='s#5 Co foil Ka12')
scan.spot(detector.eiger(), X=60000, Y=15500, exposure=2, images=200, sample='s#5 Fe foil Kb')
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=2, X0=59500, X1=61000, dX=700, Y0=5000, Y1=7000, passes=10, sample='s#5_Fe2O3 Kb', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=12400, X1=13100, dX=700, Y0=4300, Y1=15500, passes=16, sample='s#1_Fe2NH3poat_ZM', linedelay=0)
#sim.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=22700, X1=23400, dX=700, Y0=4350, Y1=15500, passes=4, sample='s#2_H3poat_ZN', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=36400, X1=37100, dX=700, Y0=4300, Y1=16500, passes=16, sample='s#3_Fe2OH3poat_ZP', linedelay=0)


scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=72000, X1=72700, dX=700, Y0=4050, Y1=16700, passes=16, sample='s#6_Fe2OHpoat_D', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=83800, X1=84500, dX=700, Y0=4100, Y1=16200, passes=16, sample='s#7_Fe3NH3poat_F', linedelay=0)

### repeats

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=83800, X1=84500, dX=700, Y0=4100, Y1=16200, passes=16, sample='s#7_Fe3NH3poat_F', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=72000, X1=72700, dX=700, Y0=4050, Y1=16700, passes=16, sample='s#6_Fe2OHpoat_D', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=36400, X1=37100, dX=700, Y0=4300, Y1=16500, passes=16, sample='s#3_Fe2OH3poat_ZP', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=12400, X1=13100, dX=700, Y0=4300, Y1=15500, passes=16, sample='s#1_Fe2NH3poat_ZM', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=12400, X1=13100, dX=700, Y0=4300, Y1=15500, passes=16, sample='s#1_Fe2NH3poat_ZM', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=36400, X1=37100, dX=700, Y0=4300, Y1=16500, passes=16, sample='s#3_Fe2OH3poat_ZP', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=72000, X1=72700, dX=700, Y0=4050, Y1=16700, passes=16, sample='s#6_Fe2OHpoat_D', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.4, X0=83800, X1=84500, dX=700, Y0=4100, Y1=16200, passes=16, sample='s#7_Fe3NH3poat_F', linedelay=0)


#pink.shutter_hard_CLOSE()
#pink.valveCLOSE(11)
#pink.valveCLOSE(12)


