#scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=1, X0=25300, X1=28100, dX=650., Y0=4000, Y1=8000, passes=4, sample='s#2 S1 Zr(IV) reference', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=1, X0=37000, X1=39800, dX=650., Y0=4000, Y1=8000, passes=4, sample='s#3 S2 Zr(III) reference', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=1, X0=48500, X1=51300, dX=650., Y0=4000, Y1=12000, passes=3, sample='s#4 S3 MOP (Material)', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=1, X0=60700, X1=63500, dX=650., Y0=4300, Y1=16000, passes=1, sample='s#5 S5 ZrO2', linedelay=0)   #reference
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=72000, X1=76000, dX=650., Y0=4200, Y1=16500, passes=16, sample='s#6 S4 Reduced MOP', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=72700, X1=75300, dX=600., Y0=4300, Y1=16300, passes=30, sample='s#6 S4 Reduced MOP', linedelay=0)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)

scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=10, sample='dark noise, 5s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=10, sample='dark noise, 5s')
caput("PINK:GEYES:cam1:Temperature", 20)