#scan.spot(detector.greateyes(), X=48500, Y=13200, exposure=3, images=100, sample='Pd foil, Lb2')
#scan.spot(detector.greateyes(), X=48500, Y=15000, exposure=3, images=100, sample='Fe foil, Ka2')

#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=59100, X1=62000, dX=650., Y0=4000, Y1=16700, passes=3, sample='s#5 SDB-AS-082: Pd(IPr)2', linedelay=5)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=70300, X1=73700, dX=650., Y0=4000, Y1=16700, passes=3, sample='s#6 SDB-AS-80: Pd(IMes)2(OAc)2', linedelay=5)
#scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=83500, X1=86000, dX=600., Y0=4000, Y1=16700, passes=3, sample='s#7 SDB-AS-84: Pd(IMes)2(OAc)(OOH)', linedelay=5)

scan.spot(detector.eiger(), X=48500, Y=15000, exposure=3, images=200, sample='Fe foil, Kb13')
scan.spot(detector.eiger(), X=48500, Y=16500, exposure=2, images=60, sample='Co foil Ka12')
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=48200, X1=51000, dX=650., Y0=3800, Y1=9000, passes=3, sample='s#4 down Fe2O3 Kb13', linedelay=0)

#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.2, X0=36700, X1=39500, dX=650., Y0=3800, Y1=12500, passes=20, sample='s#3 SDB-AS-083: [FePdFeCl2(dpa)4]1+', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.2, X0=24600, X1=27500, dX=650., Y0=3800, Y1=16700, passes=10, sample='s#2 SDB-AS-079: [FePdFeCl2(dpa)4]2+', linedelay=0)
