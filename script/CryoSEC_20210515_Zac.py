scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=9200, X1=11500, dX=740, Y0=3150, Y1=15900, passes=10, sample='s#1 TL33 Fe2N', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=20400, X1=23000, dX=740, Y0=4000, Y1=16900, passes=10, sample='s#2 TL24 PhFe2N2', linedelay=0)
#X motion
caput("PINK:PHY:AxisJ.VAL", 22000.)
#Y motion
caput("PINK:ANC01:ACT0:CMD:TARGET", 10000.)
scan.spot(detector.eiger(), exposure=5, images=60, sample='s#2 TL24 PhFe2N2 Damage scan')
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.2, X0=33100, X1=35500, dX=740, Y0=3100, Y1=16900, passes=10, sample='s#3 TL23 PhFe2N2', linedelay=0)
#######Energy calibration
#X motion
caput("PINK:PHY:AxisJ.VAL", 69000.)
scan.continuous(detector.eiger(), det_exposure=2, sample_exposure=0.5, X0=69500, X1=71500, dX=750, Y0=7500, Y1=3100, passes=10, sample='Fe2O3 Kb', linedelay=0)

caput("PINK:ANC01:ACT0:CMD:TARGET", 12500.) #Y motion
caput("PINK:PHY:AxisJ.VAL", 705000.) #X motion
scan.spot(detector.eiger(), exposure=1, images=20, sample='Co Ka1,2')
caput("PINK:ANC01:ACT0:CMD:TARGET", 15500.) #Y motion
scan.spot(detector.eiger(), exposure=5, images=60, sample='Fe Kb')

alert.message("measurement done")
print("########## DONE ##########")