#scan.spot(detector.eiger(), X=11400, Y=13200, exposure=5, images=120, sample='s1 Cu foil Kb')
#scan.spot(detector.eiger(), X=11500, Y=15300, exposure=10, images=60, sample='s1 Zn foil Ka')
#scan.spot(detector.eiger(), X=11400, Y=7000, exposure=5, images=60, sample='s1 Ho La12')
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=58500, X1=60000, dX=700, Y0=5000, Y1=16000, passes=20, sample='s5 dicopper mu-oxo, mu-nitrosyl Kb VtC', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=47300, X1=49200, dX=700, Y0=5000, Y1=15500, passes=5, sample='s4 dicopper bis-mu-nitrosyl Kb VtC', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=35000, X1=37000, dX=700, Y0=5200, Y1=15500, passes=22, sample='s3 dicopper(I,I) precursor Kb VtC', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=5, sample_exposure=1, X0=47300, X1=49200, dX=700, Y0=5100, Y1=15000, passes=10, sample='s4 dicopper bis-mu-nitrosyl Kb VtC', linedelay=0)

pink.shutter_hard_CLOSE()