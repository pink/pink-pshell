scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=10, sample='bkg')
scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=12900, X1=15600, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#1 Cr2O3', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=24700, X1=27200, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#2 Li14Cr2N8O fresh', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.2, X0=36400, X1=39000, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#3 Li14Cr2N8O spent', linedelay=10)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.spot(detector.greateyes(), X=49500, Y=11500, exposure=3, images=200, sample='Cr foil, Kb13')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=60500, X1=63100, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#5 CrO3', linedelay=0)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=72500, X1=75200, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#6 K2CrO7', linedelay=0)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')

scan.continuous(detector.greateyes(), det_exposure=3, sample_exposure=0.4, X0=84500, X1=87100, dX=600., Y0=3850, Y1=16650, passes=2, sample='s#7 KCr(SO4)2 12H2O', linedelay=0)
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=3, images=1, sample='bkg')
