
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=48000, dX=500, Xpoints=2, Y0=5000, dY=50, Ypoints=230, passes=2, sample='s#4 KCl 1mM', linedelay=0)
scan.zigzag_absolute(detector.greateyes(), exposure=5, X0=24000, dX=500, Xpoints=2, Y0=5000, dY=50, Ypoints=230, passes=2, sample='s#2 KCl 1mM', linedelay=0)

alert.message("measurement done")

print("########## DONE ##########")
