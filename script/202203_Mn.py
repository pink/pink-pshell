scan.spot(detector.eiger(), X=40500, Y=-5200, exposure=1, images=30, sample='s#1 Fe foil Ka12')
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=2, X0=21300, X1=23700, dX=700, Y0=-6000, Y1=8000, passes=4, sample='s#2 258-01 SDB-FO', linedelay=0)
#scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=2, X0=3900, X1=6100, dX=700, Y0=-6200, Y1=8100, passes=1, sample='s#3 242-01 SDB-FO', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=4, sample_exposure=2, X0=-13900, X1=-11500, dX=750, Y0=-6200, Y1=8100, passes=1, sample='s#4 243-01 SDB-FO', linedelay=0)