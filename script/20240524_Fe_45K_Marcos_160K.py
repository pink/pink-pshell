a3=caget("PINK:LKS:KRDG3")
a2=caget("PINK:LKS:KRDG2")
a1=caget("PINK:LKS:KRDG1")
a0=caget("PINK:LKS:KRDG0")
print(a3,a2,a1,a0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=24500, X1=26500, dX=600., Y0=4100, Y1=13000, passes=4, sample='s#2 sFeII_FeQPY T=160K', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=36600, X1=38500, dX=600., Y0=4100, Y1=13400, passes=4, sample='s#3 FeI_FeQPY + 1eq CoCp2* T=160K', linedelay=0)


scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=71700, X1=73800, dX=600., Y0=4100, Y1=14000, passes=4, sample='s#6 Fe0_FeQPY + 2 equivalents CoCp2* T=160K ', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=0.5, X0=83000, X1=85800, dX=650., Y0=4100, Y1=16400, passes=4, sample='s#7 56: FePdFeCl2(dpa)4 T=160K ', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=71700, X1=73800, dX=600., Y0=4100, Y1=14000, passes=16, sample='s#6 Fe0_FeQPY + 2 equivalents CoCp2* T=160K ', linedelay=0)

scan.spot(detector.eiger(), X=49000, Y=12200, exposure=5, images=240, sample='s#4 Fe foil, Kb13')
scan.spot(detector.eiger(), X=61000, Y=12000, exposure=1, images=100, sample='s#5 Co foil, Ka12')

scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=36600, X1=38500, dX=600., Y0=4100, Y1=13400, passes=8, sample='s#3 FeI_FeQPY + 1eq CoCp2* T=160K', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=10, sample_exposure=1, X0=24500, X1=26500, dX=600., Y0=4100, Y1=13000, passes=4, sample='s#2 sFeII_FeQPY T=160K', linedelay=0)
a3=caget("PINK:LKS:KRDG3")
a2=caget("PINK:LKS:KRDG2")
a1=caget("PINK:LKS:KRDG1")
a0=caget("PINK:LKS:KRDG0")
print(a3,a2,a1,a0)

pink.shutter_hard_CLOSE()
pink.valveCLOSE(11)
pink.valveOPEN(14)

caput("PINK:LKS:RANGE_S2",0)
caput("PINK:LKS:RANGE_S1",0)