scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=84100, X1=86800, dX=650., Y0=3500, Y1=16400, passes=8, sample='s#7 70; Reduced 0.33% Pd/0.33% Au/0.33% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=72200, X1=74900, dX=650., Y0=3500, Y1=16400, passes=8, sample='s#6 69; Reduced 0.475% Pd/0.475% Au/0.05% Pt NPss', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=60500, X1=63100, dX=650., Y0=3500, Y1=16400, passes=8, sample='s#5 42; 0.475% Pd/0.475% Au/0.05% Pt NPs', linedelay=0)

scan.spot(detector.greateyes(), X=50000, Y=16000, exposure=10, images=200, sample='s#4 Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=14700, exposure=1, images=200, sample='s#4 Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=50000, Y=13300, exposure=10, images=100, sample='s#4 Pd foil with kapton, Lg3')
scan.zigzag_absolute(detector.greateyes(), exposure=10, X0=48500, dX=500, Xpoints=5, Y0=4500, dY=60, Ypoints=40, passes=1, sample='s#4 Fe2O3 Kb13', linedelay=10, moveback=0)

scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=36500, X1=39200, dX=650., Y0=3500, Y1=16400, passes=8, sample='s#3 68; Reduced 0.5% Pd/0.5% Au NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=5, sample_exposure=0.2, X0=24600, X1=27400, dX=650., Y0=3500, Y1=16400, passes=12, sample='s#2 56; FePdFeCl2(dpa)4', linedelay=0)


scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.4, X0=36500, X1=39200, dX=650., Y0=3500, Y1=16400, passes=6, sample='s#3 68; Reduced 0.5% Pd/0.5% Au NPs', linedelay=0)
scan.spot(detector.greateyes(), X=50000, Y=16000, exposure=10, images=200, sample='s#4 Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=14700, exposure=1, images=200, sample='s#4 Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=50000, Y=13300, exposure=10, images=100, sample='s#4 Pd foil with kapton, Lg3')
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.4, X0=60500, X1=63100, dX=650., Y0=3500, Y1=16400, passes=6, sample='s#5 42; 0.475% Pd/0.475% Au/0.05% Pt NPs', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.4, X0=72200, X1=74900, dX=650., Y0=3500, Y1=16400, passes=6, sample='s#6 69; Reduced 0.475% Pd/0.475% Au/0.05% Pt NPss', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=0.4, X0=84100, X1=86800, dX=650., Y0=3500, Y1=16400, passes=6, sample='s#7 70; Reduced 0.33% Pd/0.33% Au/0.33% Pt NPs', linedelay=0)

pink.shutter_hard_CLOSE()
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=10, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=10, sample='dark noise, 5s')
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)