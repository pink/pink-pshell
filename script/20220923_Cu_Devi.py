scan.spot(detector.eiger(), X=4000, Y=0, exposure=3, images=200, sample='s#2 Cu foil Kb')
scan.spot(detector.eiger(), X=4000, Y=3000, exposure=3, images=100, sample='s#2 Zn foil Ka12')
scan.spot(detector.eiger(), X=4000, Y=5500, exposure=5, images=100, sample='s#2 Ho foil La')
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=-13700, X1=-11400, dX=750, Y0=-6600, Y1=8100, passes=5, sample='s#1 Cu(bpy)3', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=21500, X1=23800, dX=700, Y0=-6600, Y1=8100, passes=5, sample='s#3 Cu(III) HMPA', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=39100, X1=41400, dX=750, Y0=-6600, Y1=8100, passes=5, sample='s#4 Cu(OH)2', linedelay=0)

scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=-13700, X1=-11400, dX=750, Y0=-6600, Y1=8100, passes=7, sample='s#1 Cu(bpy)3', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=21500, X1=23800, dX=700, Y0=-6600, Y1=8100, passes=4, sample='s#3 Cu(III) HMPA', linedelay=0)
scan.continuous(detector.eiger(), det_exposure=3, sample_exposure=0.5, X0=39100, X1=41400, dX=750, Y0=-6600, Y1=8100, passes=4, sample='s#4 Cu(OH)2', linedelay=0)

scan.spot(detector.eiger(), X=4000, Y=0, exposure=3, images=200, sample='s#2 Cu foil Kb')
pink.shutter_hard_CLOSE()