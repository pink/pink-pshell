#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=12800, X1=16000, dX=700., Y0=3300, Y1=16500, passes=2, sample='s#1 PdCl', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=25000, X1=27900, dX=700, Y0=3300, Y1=16500, passes=4, sample='s#2 56; FePdFeCl2(dpa)4', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=36500, X1=39100, dX=650., Y0=3300, Y1=16500, passes=4, sample='s#3 58; CoPdCoCl2(dpa)4 ', linedelay=0)

#scan.spot(detector.greateyes(), X=50000, Y=15900, exposure=10, images=200, sample='s#4 Fe foil no kapton, Kb13')
#scan.spot(detector.greateyes(), X=50000, Y=14400, exposure=2, images=200, sample='s#4 Ag foil with kapton, Lg1')
#scan.spot(detector.greateyes(), X=50000, Y=12900, exposure=10, images=100, sample='s#4 Pd foil with kapton, Lg3')


scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=60300, X1=63800, dX=750., Y0=4000, Y1=16500, passes=4, sample='s#5 71; Pd(NHC)2(O2)', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=72000, X1=75000, dX=700., Y0=4000, Y1=16500, passes=4, sample='s#6 72; Pd(NHC)2(O2)2', linedelay=0)
continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=83500, X1=86300, dX=700., Y0=34000, Y1=16500, passes=6, sample='s#7 60; NiPdNiCl2(dpa)4 (diluted in wax)', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=72000, X1=75000, dX=700., Y0=4000, Y1=16500, passes=2, sample='s#6 72; Pd(NHC)2(O2)2', linedelay=0)
scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=60300, X1=63800, dX=750., Y0=34000, Y1=16500, passes=2, sample='s#5 71; Pd(NHC)2(O2)', linedelay=0)

scan.spot(detector.greateyes(), X=50000, Y=15900, exposure=10, images=200, sample='s#4 Fe foil no kapton, Kb13')
scan.spot(detector.greateyes(), X=50000, Y=14400, exposure=2, images=200, sample='s#4 Ag foil with kapton, Lg1')
scan.spot(detector.greateyes(), X=50000, Y=12900, exposure=10, images=100, sample='s#4 Pd foil with kapton, Lg3')
scan.zigzag_absolute(detector.greateyes(), exposure=10, X0=48900, dX=500, Xpoints=5, Y0=4500, dY=60, Ypoints=40, passes=1, sample='s#4 Fe2O3 Kb13', linedelay=10, moveback=0)

#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=36500, X1=39100, dX=650., Y0=4000, Y1=16500, passes=4, sample='s#3 58; CoPdCoCl2(dpa)4 ', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=25000, X1=27900, dX=700, Y0=4000, Y1=16500, passes=4, sample='s#2 56; FePdFeCl2(dpa)4', linedelay=0)
#scan.continuous(detector.greateyes(), det_exposure=10, sample_exposure=1, X0=12800, X1=16000, dX=700., Y0=4000, Y1=16500, passes=2, sample='s#1 PdCl', linedelay=0)

#pink.shutter_hard_CLOSE()
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=10, images=6, sample='dark noise, 10s')
scan.spot(detector.greateyes(), X=None, Y=None, exposure=5, images=6, sample='dark noise, 5s')
pink.valveCLOSE(11)
pink.valveOPEN(14)
caput("PINK:GEYES:cam1:Temperature", 20)
pink.gap(8.0)