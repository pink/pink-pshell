#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=70000, dX=750, Xpoints=3, Y0=4500, dY=50, Ypoints=200, passes=1, sample='s#6 FeCl3 100mM', linedelay=0)

#scan.zigzag_absolute(detector.eiger(), exposure=5, X0=82000, dX=750, Xpoints=3, Y0=4500, dY=50, Ypoints=200, passes=1, sample='s#7 H2O', linedelay=0)
scan.zigzag_absolute(detector.eiger(), exposure=5, X0=10000, dX=750, Xpoints=3, Y0=4500, dY=50, Ypoints=200, passes=1, sample='s#1 FeCl3 3mM', linedelay=0)

scan.zigzag_absolute(detector.eiger(), exposure=5, X0=10000, dX=750, Xpoints=3, Y0=5000, dY=50, Ypoints=200, passes=1, sample='s#1 FeCl3 3mM', linedelay=0)

scan.zigzag_absolute(detector.eiger(), exposure=5, X0=21700, dX=750, Xpoints=3, Y0=4500, dY=50, Ypoints=200, passes=1, sample='s#2 FeCl3 10mM', linedelay=0)

scan.zigzag_absolute(detector.eiger(), exposure=5, X0=70000, dX=750, Xpoints=3, Y0=4500, dY=50, Ypoints=200, passes=1, sample='s#6 FeCl3 100mM', linedelay=0)

alert.message("measurement done")
print("########## DONE ##########")
pink.shutter_hard_CLOSE()